#!bash

npm install gulp@3.9.1 \
&& npm install --save-dev gulp@3.9.1 \
&& npm install node-sass@4.9.0 \
&& npm install gulp-sass@4.0.1 --save-dev \
&& npm install gulp-clean-css@3.9.4 --save-dev \
&& npm install --save-dev gulp-autoprefixer@5.0.0 \
&& npm install --save-dev gulp-wrap@0.13.0 \
&& npm install --save-dev gulp-tap@1.0.1 \
&& npm install --save-dev gulp-file-include@2.0.1 \
&& npm install gulp-concat@2.6.1 gulp-rename@1.2.3 gulp-uglify@3.0.0 --save-dev \
&& npm install --save-dev gulp-util@3.0.8 \
&& npm install --save-dev gulp-debug@4.0.0