#!/bin/bash

echo ""
echo "HIVE | installer\n"

# get local stuff
echo "do local stuff"
cd /app/web/

if [ ! -f app.php ]; then
    echo "touch app.php"
    touch ./app.php
    echo "<?php phpinfo(); ?>" >> ./app.php
fi

if [ ! -f adminer.php ]; then
    echo "get adminer"
    wget https://www.adminer.org/latest.php -q -O ./adminer.php
fi


if [ ! -f .htaccess ]; then
    echo "ln -s .htaccess_local .htaccess"
    ln -s .htaccess_local .htaccess
fi

echo "\ncomposer install"

cd /
#wget https://getcomposer.org/composer.phar
chmod 777 ./composer.phar && ./composer.phar install

echo "cd /app/web/typo3conf/ext/\n"

cd /app/web/typo3conf/ext/

echo "ln -s ../../../vendor/helhum/typo3-console typo3_console\n"

ln -s ../../../vendor/helhum/typo3-console typo3_console

echo "cd /app/\n"

cd /app/

echo "install typo3"

./web/typo3conf/ext/typo3_console/typo3cms install:setup \
    --no-interaction \
    --use-existing-database \
    --database-user-name="dev" \
    --database-host-name="mysql" \
    --database-port="3306" \
    --database-name="dev" \
    --admin-user-name="sudo" \
    --admin-password="Jai_Fee4Eewohci?um7a" \
    --site-name="Basic Install"

echo ""
echo "Activate all extensions"
echo ""

./web/typo3conf/ext/typo3_console/typo3cms install:generatepackagestates \
    --activate-default

echo ""
echo "User: sudo"
echo "Password: Jai_Fee4Eewohci?um7a (please change this by importing dummy db)"

echo ""
echo "Done."