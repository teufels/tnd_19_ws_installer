-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: mysql    Database: dev
-- ------------------------------------------------------
-- Server version	5.6.43-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backend_layout`
--

DROP TABLE IF EXISTS `backend_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_layout` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `icon` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_layout`
--

LOCK TABLES `backend_layout` WRITE;
/*!40000 ALTER TABLE `backend_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_groups`
--

DROP TABLE IF EXISTS `be_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_groups` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `non_exclude_fields` text COLLATE utf8_unicode_ci,
  `explicit_allowdeny` text COLLATE utf8_unicode_ci,
  `allowed_languages` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_options` text COLLATE utf8_unicode_ci,
  `db_mountpoints` text COLLATE utf8_unicode_ci,
  `pagetypes_select` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tables_select` text COLLATE utf8_unicode_ci,
  `tables_modify` text COLLATE utf8_unicode_ci,
  `groupMods` text COLLATE utf8_unicode_ci,
  `file_mountpoints` text COLLATE utf8_unicode_ci,
  `file_permissions` text COLLATE utf8_unicode_ci,
  `lockToDomain` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TSconfig` text COLLATE utf8_unicode_ci,
  `subgroup` text COLLATE utf8_unicode_ci,
  `workspace_perms` smallint(6) NOT NULL DEFAULT '1',
  `category_perms` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_groups`
--

LOCK TABLES `be_groups` WRITE;
/*!40000 ALTER TABLE `be_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_sessions`
--

DROP TABLE IF EXISTS `be_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_sessions` (
  `ses_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ses_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `ses_data` longblob,
  `ses_backuserid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_sessions`
--

LOCK TABLES `be_sessions` WRITE;
/*!40000 ALTER TABLE `be_sessions` DISABLE KEYS */;
INSERT INTO `be_sessions` VALUES ('e33841cbca5ab7bc72526d6859252a70','172.25.0.1',1,1558684734,'a:4:{s:26:\"formProtectionSessionToken\";s:64:\"e03b6c1d7cd08af94cec838c89d92c15b83ba9309064004986c601630f65ebda\";s:27:\"core.template.flashMessages\";N;s:49:\"TYPO3\\CMS\\Backend\\Controller\\PageLayoutController\";a:1:{s:12:\"search_field\";N;}s:53:\"extbase.flashmessages.tx_beuser_system_beusertxbeuser\";N;}',0);
/*!40000 ALTER TABLE `be_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_users`
--

DROP TABLE IF EXISTS `be_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `disable` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `avatar` int(10) unsigned NOT NULL DEFAULT '0',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `admin` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lang` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `db_mountpoints` text COLLATE utf8_unicode_ci,
  `options` smallint(5) unsigned NOT NULL DEFAULT '0',
  `realName` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `userMods` text COLLATE utf8_unicode_ci,
  `allowed_languages` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uc` mediumblob,
  `file_mountpoints` text COLLATE utf8_unicode_ci,
  `file_permissions` text COLLATE utf8_unicode_ci,
  `workspace_perms` smallint(6) NOT NULL DEFAULT '1',
  `lockToDomain` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `disableIPlock` smallint(5) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text COLLATE utf8_unicode_ci,
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `createdByAction` int(11) NOT NULL DEFAULT '0',
  `usergroup_cached_list` text COLLATE utf8_unicode_ci,
  `workspace_id` int(11) NOT NULL DEFAULT '0',
  `category_perms` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `username` (`username`),
  KEY `parent` (`pid`,`deleted`,`disable`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_users`
--

LOCK TABLES `be_users` WRITE;
/*!40000 ALTER TABLE `be_users` DISABLE KEYS */;
INSERT INTO `be_users` VALUES (1,0,1558683445,1558683445,0,0,0,0,0,NULL,'sudo',0,'$argon2i$v=19$m=16384,t=16,p=2$bG5HMGZLVFM1WjZnVFlRUA$JNKvzF6Q0M5fwIVOPQgDBu65mTiCq78vJ+417pcgpgM',1,'','','',NULL,0,'',NULL,'','a:16:{s:14:\"interfaceSetup\";s:7:\"backend\";s:10:\"moduleData\";a:6:{s:10:\"web_layout\";a:2:{s:8:\"function\";s:1:\"1\";s:8:\"language\";s:1:\"0\";}s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";a:0:{}s:10:\"FormEngine\";a:2:{i:0;a:4:{s:32:\"696addfecc296b326ff6e9f04c7ff3e1\";a:4:{i:0;s:4:\"Home\";i:1;a:6:{s:4:\"edit\";a:1:{s:5:\"pages\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:9:\"workspace\";N;}i:2;s:28:\"&edit%5Bpages%5D%5B1%5D=edit\";i:3;a:5:{s:5:\"table\";s:5:\"pages\";s:3:\"uid\";i:1;s:3:\"pid\";i:0;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}s:32:\"86205c5935270b8ee413592ec1b62292\";a:4:{i:0;s:8:\"NEW SITE\";i:1;a:6:{s:4:\"edit\";a:1:{s:12:\"sys_template\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:9:\"workspace\";N;}i:2;s:35:\"&edit%5Bsys_template%5D%5B1%5D=edit\";i:3;a:5:{s:5:\"table\";s:12:\"sys_template\";s:3:\"uid\";i:1;s:3:\"pid\";i:2;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}s:32:\"29de12b3b960c7b9fac10cdbdef2bdfd\";a:4:{i:0;s:4:\"+ext\";i:1;a:6:{s:4:\"edit\";a:1:{s:12:\"sys_template\";a:1:{i:2;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";s:5:\"title\";s:6:\"noView\";N;s:9:\"workspace\";N;}i:2;s:53:\"&edit%5Bsys_template%5D%5B2%5D=edit&columnsOnly=title\";i:3;a:5:{s:5:\"table\";s:12:\"sys_template\";s:3:\"uid\";i:2;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}s:32:\"c72d4a5ffd106794d45e5a4a941814ba\";a:4:{i:0;s:8:\"+ext [1]\";i:1;a:6:{s:4:\"edit\";a:1:{s:12:\"sys_template\";a:1:{i:2;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;s:9:\"workspace\";N;}i:2;s:35:\"&edit%5Bsys_template%5D%5B2%5D=edit\";i:3;a:5:{s:5:\"table\";s:12:\"sys_template\";s:3:\"uid\";i:2;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}}}i:1;s:32:\"696addfecc296b326ff6e9f04c7ff3e1\";}s:6:\"web_ts\";a:2:{s:8:\"function\";s:85:\"TYPO3\\CMS\\Tstemplate\\Controller\\TypoScriptTemplateInformationModuleFunctionController\";s:19:\"constant_editor_cat\";s:0:\"\";}s:16:\"browse_links.php\";a:1:{s:10:\"expandPage\";s:1:\"2\";}s:9:\"tx_beuser\";s:530:\"O:40:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\ModuleData\":2:{s:9:\"\0*\0demand\";O:36:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\Demand\":12:{s:11:\"\0*\0userName\";s:0:\"\";s:11:\"\0*\0userType\";i:0;s:9:\"\0*\0status\";i:0;s:9:\"\0*\0logins\";i:0;s:19:\"\0*\0backendUserGroup\";N;s:6:\"\0*\0uid\";N;s:16:\"\0*\0_localizedUid\";N;s:15:\"\0*\0_languageUid\";N;s:16:\"\0*\0_versionedUid\";N;s:6:\"\0*\0pid\";N;s:61:\"\0TYPO3\\CMS\\Extbase\\DomainObject\\AbstractDomainObject\0_isClone\";b:0;s:69:\"\0TYPO3\\CMS\\Extbase\\DomainObject\\AbstractDomainObject\0_cleanProperties\";a:0:{}}s:18:\"\0*\0compareUserList\";a:0:{}}\";}s:19:\"thumbnailsByDefault\";i:1;s:14:\"emailMeAtLogin\";i:0;s:11:\"startModule\";s:15:\"help_AboutAbout\";s:8:\"titleLen\";i:50;s:8:\"edit_RTE\";s:1:\"1\";s:20:\"edit_docModuleUpload\";s:1:\"1\";s:15:\"resizeTextareas\";i:1;s:25:\"resizeTextareas_MaxHeight\";i:500;s:24:\"resizeTextareas_Flexible\";i:0;s:4:\"lang\";s:0:\"\";s:19:\"firstLoginTimeStamp\";i:1558684281;s:15:\"moduleSessionID\";a:6:{s:10:\"web_layout\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";s:10:\"FormEngine\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";s:6:\"web_ts\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";s:16:\"browse_links.php\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";s:9:\"tx_beuser\";s:32:\"e33841cbca5ab7bc72526d6859252a70\";}s:17:\"BackendComponents\";a:1:{s:6:\"States\";a:1:{s:8:\"Pagetree\";a:1:{s:9:\"stateHash\";a:1:{s:3:\"0_0\";s:1:\"1\";}}}}s:11:\"browseTrees\";a:1:{s:11:\"browsePages\";s:24:\"a:1:{i:0;a:1:{i:0;i:1;}}\";}}',NULL,NULL,1,'',0,NULL,1558684281,0,NULL,0,NULL),(2,0,1558683473,1558683473,0,0,0,0,0,NULL,'_cli_',0,'$argon2i$v=19$m=16384,t=16,p=2$a1FJeDludzlTY2pPTXBYTw$zoxpyJNeTlQhn4oCDz17Ml9DJUdw08vbG+Vlgop39CQ',1,'','','',NULL,0,'',NULL,'','a:13:{s:14:\"interfaceSetup\";s:0:\"\";s:10:\"moduleData\";a:0:{}s:19:\"thumbnailsByDefault\";i:1;s:14:\"emailMeAtLogin\";i:0;s:11:\"startModule\";s:15:\"help_AboutAbout\";s:8:\"titleLen\";i:50;s:8:\"edit_RTE\";s:1:\"1\";s:20:\"edit_docModuleUpload\";s:1:\"1\";s:15:\"resizeTextareas\";i:1;s:25:\"resizeTextareas_MaxHeight\";i:500;s:24:\"resizeTextareas_Flexible\";i:0;s:4:\"lang\";s:0:\"\";s:19:\"firstLoginTimeStamp\";i:1558683473;}',NULL,NULL,1,'',0,NULL,0,0,NULL,0,NULL);
/*!40000 ALTER TABLE `be_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_treelist`
--

DROP TABLE IF EXISTS `cache_treelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_treelist` (
  `md5hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT '0',
  `treelist` mediumtext COLLATE utf8_unicode_ci,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_treelist`
--

LOCK TABLES `cache_treelist` WRITE;
/*!40000 ALTER TABLE `cache_treelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_treelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_adminpanel_requestcache`
--

DROP TABLE IF EXISTS `cf_adminpanel_requestcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_adminpanel_requestcache` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_adminpanel_requestcache`
--

LOCK TABLES `cf_adminpanel_requestcache` WRITE;
/*!40000 ALTER TABLE `cf_adminpanel_requestcache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_adminpanel_requestcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_adminpanel_requestcache_tags`
--

DROP TABLE IF EXISTS `cf_adminpanel_requestcache_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_adminpanel_requestcache_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_adminpanel_requestcache_tags`
--

LOCK TABLES `cf_adminpanel_requestcache_tags` WRITE;
/*!40000 ALTER TABLE `cf_adminpanel_requestcache_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_adminpanel_requestcache_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_hash`
--

DROP TABLE IF EXISTS `cf_cache_hash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_hash` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_hash`
--

LOCK TABLES `cf_cache_hash` WRITE;
/*!40000 ALTER TABLE `cf_cache_hash` DISABLE KEYS */;
INSERT INTO `cf_cache_hash` VALUES (1,'89dd425378b0a106e79d917e41fb6c3e',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:3:{s:4:\"mod.\";a:5:{s:9:\"web_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:18:\"tableDisplayOrder.\";a:10:{s:9:\"be_users.\";a:1:{s:5:\"after\";s:9:\"be_groups\";}s:15:\"sys_filemounts.\";a:1:{s:5:\"after\";s:8:\"be_users\";}s:17:\"sys_file_storage.\";a:1:{s:5:\"after\";s:14:\"sys_filemounts\";}s:13:\"sys_language.\";a:1:{s:5:\"after\";s:16:\"sys_file_storage\";}s:9:\"fe_users.\";a:2:{s:5:\"after\";s:9:\"fe_groups\";s:6:\"before\";s:5:\"pages\";}s:13:\"sys_template.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:15:\"backend_layout.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:11:\"sys_domain.\";a:1:{s:5:\"after\";s:12:\"sys_template\";}s:11:\"tt_content.\";a:1:{s:5:\"after\";s:33:\"pages,backend_layout,sys_template\";}s:13:\"sys_category.\";a:1:{s:5:\"after\";s:10:\"tt_content\";}}s:12:\"searchLevel.\";a:1:{s:6:\"items.\";a:6:{i:-1;s:82:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.infinite\";i:0;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.0\";i:1;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.1\";i:2;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.2\";i:3;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.3\";i:4;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.4\";}}}s:8:\"wizards.\";a:2:{s:10:\"newRecord.\";a:1:{s:6:\"pages.\";a:1:{s:5:\"show.\";a:3:{s:10:\"pageInside\";s:1:\"1\";s:9:\"pageAfter\";s:1:\"1\";s:18:\"pageSelectPosition\";s:1:\"1\";}}}s:18:\"newContentElement.\";a:1:{s:12:\"wizardItems.\";a:6:{s:7:\"common.\";a:3:{s:9:\"elements.\";a:8:{s:7:\"header.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-header\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:6:\"header\";}}s:5:\"text.\";a:4:{s:14:\"iconIdentifier\";s:12:\"content-text\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"text\";}}s:8:\"textpic.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-textpic\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"textpic\";}}s:6:\"image.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-image\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"image\";}}s:10:\"textmedia.\";a:4:{s:14:\"iconIdentifier\";s:17:\"content-textmedia\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:9:\"textmedia\";}}s:8:\"bullets.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-bullets\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"bullets\";}}s:6:\"table.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-table\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"table\";}}s:8:\"uploads.\";a:4:{s:14:\"iconIdentifier\";s:23:\"content-special-uploads\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"uploads\";}}}s:4:\"show\";s:57:\"header,text,textpic,image,textmedia,bullets,table,uploads\";s:6:\"header\";s:81:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common\";}s:5:\"menu.\";a:3:{s:9:\"elements.\";a:11:{s:14:\"menu_abstract.\";a:4:{s:14:\"iconIdentifier\";s:21:\"content-menu-abstract\";s:5:\"title\";s:94:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_abstract.title\";s:11:\"description\";s:100:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_abstract.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:13:\"menu_abstract\";}}s:25:\"menu_categorized_content.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-menu-categorized\";s:5:\"title\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_content.title\";s:11:\"description\";s:111:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_content.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:24:\"menu_categorized_content\";}}s:23:\"menu_categorized_pages.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-menu-categorized\";s:5:\"title\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_pages.title\";s:11:\"description\";s:109:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:22:\"menu_categorized_pages\";}}s:11:\"menu_pages.\";a:4:{s:14:\"iconIdentifier\";s:18:\"content-menu-pages\";s:5:\"title\";s:91:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_pages.title\";s:11:\"description\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:10:\"menu_pages\";}}s:14:\"menu_subpages.\";a:4:{s:14:\"iconIdentifier\";s:18:\"content-menu-pages\";s:5:\"title\";s:94:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_subpages.title\";s:11:\"description\";s:100:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_subpages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:13:\"menu_subpages\";}}s:22:\"menu_recently_updated.\";a:4:{s:14:\"iconIdentifier\";s:29:\"content-menu-recently-updated\";s:5:\"title\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_recently_updated.title\";s:11:\"description\";s:108:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_recently_updated.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:21:\"menu_recently_updated\";}}s:19:\"menu_related_pages.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-related\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_related_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_related_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_related_pages\";}}s:13:\"menu_section.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-section\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section.title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:12:\"menu_section\";}}s:19:\"menu_section_pages.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-section\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_section_pages\";}}s:13:\"menu_sitemap.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-sitemap\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap.title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:12:\"menu_sitemap\";}}s:19:\"menu_sitemap_pages.\";a:4:{s:14:\"iconIdentifier\";s:26:\"content-menu-sitemap-pages\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_sitemap_pages\";}}}s:4:\"show\";s:191:\"menu_abstract,menu_categorized_content,menu_categorized_pages,menu_pages,menu_subpages,menu_recently_updated,menu_related_pages,menu_section,menu_section_pages,menu_sitemap,menu_sitemap_pages\";s:6:\"header\";s:79:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu\";}s:8:\"special.\";a:3:{s:9:\"elements.\";a:3:{s:5:\"html.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-special-html\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"html\";}}s:4:\"div.\";a:4:{s:14:\"iconIdentifier\";s:19:\"content-special-div\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:3:\"div\";}}s:9:\"shortcut.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-special-shortcut\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:8:\"shortcut\";}}}s:4:\"show\";s:17:\"html,div,shortcut\";s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special\";}s:6:\"forms.\";a:3:{s:4:\"show\";s:13:\"formframework\";s:9:\"elements.\";a:1:{s:14:\"formframework.\";a:4:{s:14:\"iconIdentifier\";s:12:\"content-form\";s:5:\"title\";s:75:\"LLL:EXT:form/Resources/Private/Language/locallang.xlf:form_new_wizard_title\";s:11:\"description\";s:77:\"LLL:EXT:form/Resources/Private/Language/locallang:form_new_wizard_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"form_formframework\";}}}s:6:\"header\";s:80:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms\";}s:8:\"plugins.\";a:3:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins\";s:9:\"elements.\";a:1:{s:8:\"general.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-plugin\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"list\";}}}s:4:\"show\";s:1:\"*\";}s:5:\"hive.\";a:4:{s:6:\"header\";s:4:\"Hive\";s:5:\"after\";s:33:\"common,special,menu,plugins,forms\";s:9:\"elements.\";a:2:{s:42:\"hivecptnavanchornavigationrendernavanchor.\";a:4:{s:14:\"iconIdentifier\";s:24:\"hive_cpt_brand_32x32_svg\";s:5:\"title\";s:17:\"Anchor Navigation\";s:11:\"description\";s:35:\"Anchor navigation for selected page\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:4:\"list\";s:9:\"list_type\";s:58:\"hivecptnavanchor_hivecptnavanchornavigationrendernavanchor\";}}s:17:\"hive_cpt_cnt_img.\";a:4:{s:14:\"iconIdentifier\";s:24:\"hive_cpt_brand_32x32_svg\";s:5:\"title\";s:9:\"FAL Image\";s:11:\"description\";s:27:\"Pure FAL Image without Flux\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:4:\"list\";s:9:\"list_type\";s:39:\"hivecptcntimg_hivecptcntimgshowfalimage\";}}}s:4:\"show\";s:58:\"hivecptnavanchornavigationrendernavanchor,hive_cpt_cnt_img\";}}}}s:9:\"web_view.\";a:1:{s:19:\"previewFrameWidths.\";a:12:{s:5:\"1920.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1920\";s:6:\"height\";s:4:\"1080\";}s:5:\"1366.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1366\";s:6:\"height\";s:3:\"768\";}s:5:\"1280.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1280\";s:6:\"height\";s:4:\"1024\";}s:5:\"1024.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:3:\"768\";}s:7:\"nexus7.\";a:4:{s:5:\"label\";s:7:\"Nexus 7\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:3:\"600\";s:6:\"height\";s:3:\"960\";}s:8:\"nexus6p.\";a:4:{s:5:\"label\";s:8:\"Nexus 6P\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"411\";s:6:\"height\";s:3:\"731\";}s:8:\"ipadpro.\";a:4:{s:5:\"label\";s:8:\"iPad Pro\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:4:\"1366\";}s:8:\"ipadair.\";a:4:{s:5:\"label\";s:8:\"iPad Air\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:3:\"768\";s:6:\"height\";s:4:\"1024\";}s:12:\"iphone7plus.\";a:4:{s:5:\"label\";s:13:\"iPhone 7 Plus\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"414\";s:6:\"height\";s:3:\"736\";}s:8:\"iphone6.\";a:4:{s:5:\"label\";s:8:\"iPhone 6\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"375\";s:6:\"height\";s:3:\"667\";}s:8:\"iphone5.\";a:4:{s:5:\"label\";s:8:\"iPhone 5\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"320\";s:6:\"height\";s:3:\"568\";}s:8:\"iphone4.\";a:4:{s:5:\"label\";s:8:\"iPhone 4\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"320\";s:6:\"height\";s:3:\"480\";}}}s:9:\"web_info.\";a:1:{s:17:\"fieldDefinitions.\";a:3:{s:2:\"0.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_0\";s:6:\"fields\";s:81:\"title,uid,slug,alias,starttime,endtime,fe_group,target,url,shortcut,shortcut_mode\";}s:2:\"1.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_1\";s:6:\"fields\";s:26:\"title,uid,###ALL_TABLES###\";}s:2:\"2.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_2\";s:6:\"fields\";s:93:\"title,uid,lastUpdated,newUntil,cache_timeout,php_tree_stop,TSconfig,is_siteroot,fe_login_mode\";}}}s:11:\"web_layout.\";a:1:{s:15:\"BackendLayouts.\";a:27:{s:17:\"SectionAnchor100.\";a:3:{s:5:\"title\";s:44:\"hive Backend Layout :: Section A/B/C :: 100%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:44:\"[0] SECTION A/B/C :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:79:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor.gif\";}s:27:\"SectionAnchor100WithOffset.\";a:3:{s:5:\"title\";s:55:\"hive Backend Layout :: SectionA/B/C with Offset :: 100%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:44:\"[0] SECTION A/B/C :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:89:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchorWithOffset.gif\";}s:24:\"SectionAnchor2020202020.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C :: 20% 20% 20% 20% 20%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:5:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"2\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"2\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"2\";}s:2:\"4.\";a:3:{s:4:\"name\";s:24:\"[3] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"2\";}s:2:\"5.\";a:3:{s:4:\"name\";s:24:\"[4] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"4\";s:7:\"colspan\";s:1:\"2\";}}}}}}s:4:\"icon\";s:89:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2020202020.gif\";}s:34:\"SectionAnchor2020202020WithOffset.\";a:3:{s:5:\"title\";s:71:\"hive Backend Layout :: Section A/B/C with Offset :: 20% 20% 20% 20% 20%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:5:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"2\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"2\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"2\";}s:2:\"4.\";a:3:{s:4:\"name\";s:36:\"[3] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"2\";}s:2:\"5.\";a:3:{s:4:\"name\";s:36:\"[4] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"4\";s:7:\"colspan\";s:1:\"2\";}}}}}}s:4:\"icon\";s:99:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2020202020WithOffset.gif\";}s:22:\"SectionAnchor25252525.\";a:3:{s:5:\"title\";s:55:\"hive Backend Layout :: Section A/B/C :: 25% 25% 25% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:4:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"3\";}s:2:\"4.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:87:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor25252525.gif\";}s:32:\"SectionAnchor25252525WithOffset.\";a:3:{s:5:\"title\";s:67:\"hive Backend Layout :: Section A/B/C with Offset :: 25% 25% 25% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:4:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"3\";}s:2:\"4.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:97:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor25252525WithOffset.gif\";}s:18:\"SectionAnchor2575.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 25% 75%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 75%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"9\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2575.gif\";}s:28:\"SectionAnchor2575WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 25% 75%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 75%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"9\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2575WithOffset.gif\";}s:20:\"SectionAnchor333333.\";a:3:{s:5:\"title\";s:51:\"hive Backend Layout :: Section A/B/C :: 33% 33% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:3:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:85:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor333333.gif\";}s:30:\"SectionAnchor333333WithOffset.\";a:3:{s:5:\"title\";s:63:\"hive Backend Layout :: Section A/B/C with Offset :: 33% 33% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:3:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:95:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor333333WithOffset.gif\";}s:18:\"SectionAnchor3366.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 33% 66%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 66%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"8\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor3366.gif\";}s:28:\"SectionAnchor3366WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 33% 66%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 66%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"8\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor3366WithOffset.gif\";}s:18:\"SectionAnchor4060.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 40% 60%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 40%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"5\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 60%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"7\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor4060.gif\";}s:28:\"SectionAnchor4060WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 40% 60%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 40%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"5\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 60%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"7\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor4060WithOffset.gif\";}s:18:\"SectionAnchor5050.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 50% 50%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 50%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"6\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 50%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"6\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor5050.gif\";}s:28:\"SectionAnchor5050WithOffset.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C with Offset :: 50% 50%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 50%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"6\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 50%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"6\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor5050WithOffset.gif\";}s:18:\"SectionAnchor6040.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 60% 40%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 60%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"7\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 40%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"5\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6040.gif\";}s:28:\"SectionAnchor6040WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 60% 40%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 60%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"7\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 40%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"5\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6040WithOffset.gif\";}s:18:\"SectionAnchor6633.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 66% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 66%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"8\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6633.gif\";}s:28:\"SectionAnchor6633WithOffset.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C with Offset :: 66% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 66%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"8\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6633WithOffset.gif\";}s:18:\"SectionAnchor7525.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 75% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 75%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"9\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor7525.gif\";}s:28:\"SectionAnchor7525WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 75% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 75%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"9\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor7525WithOffset.gif\";}s:13:\"CustomAnchor.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Custom :: HTML :: Anchor\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:48:\"[1] Anchor :: TopPage :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:21:\"CustomBasicOneColumn.\";a:3:{s:5:\"title\";s:64:\"hive Backend Layout :: Custom :: HTML :: Custom Basic One Column\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:28:\"[10] Custom Basic One Column\";s:6:\"colPos\";s:2:\"10\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:16:\"CustomPrototype.\";a:3:{s:5:\"title\";s:50:\"hive Backend Layout :: Custom :: HTML :: Prototype\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:35:\"[1] TEST :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:8:\"Default.\";a:3:{s:5:\"title\";s:48:\"hive Backend Layout :: Custom :: HTML :: Default\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:35:\"[1] TEST :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:6:\"Slide.\";a:3:{s:5:\"title\";s:46:\"hive Backend Layout :: Custom :: HTML :: Slide\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:4:\"9999\";s:5:\"rows.\";a:1:{s:3:\"10.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:18:\"[1] Slide :: Image\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}}}}s:8:\"TCEMAIN.\";a:2:{s:18:\"translateToMessage\";s:16:\"Translate to %s:\";s:12:\"linkHandler.\";a:5:{s:5:\"page.\";a:2:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\PageLinkHandler\";s:5:\"label\";s:77:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:page\";}s:5:\"file.\";a:4:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FileLinkHandler\";s:5:\"label\";s:77:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:file\";s:12:\"displayAfter\";s:4:\"page\";s:9:\"scanAfter\";s:4:\"page\";}s:7:\"folder.\";a:4:{s:7:\"handler\";s:50:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FolderLinkHandler\";s:5:\"label\";s:79:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:folder\";s:12:\"displayAfter\";s:4:\"file\";s:9:\"scanAfter\";s:4:\"file\";}s:4:\"url.\";a:4:{s:7:\"handler\";s:47:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\UrlLinkHandler\";s:5:\"label\";s:79:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:extUrl\";s:12:\"displayAfter\";s:6:\"folder\";s:9:\"scanAfter\";s:4:\"mail\";}s:5:\"mail.\";a:3:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\MailLinkHandler\";s:5:\"label\";s:78:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:email\";s:12:\"displayAfter\";s:3:\"url\";}}}s:8:\"TCEFORM.\";a:2:{s:11:\"tt_content.\";a:1:{s:12:\"imageorient.\";a:1:{s:6:\"types.\";a:1:{s:6:\"image.\";a:1:{s:11:\"removeItems\";s:18:\"8,9,10,17,18,25,26\";}}}}s:6:\"pages.\";a:13:{s:7:\"layout.\";a:2:{s:9:\"addItems.\";a:3:{i:4;s:1:\"A\";i:5;s:1:\"B\";i:6;s:1:\"C\";}s:10:\"altLabels.\";a:4:{i:0;s:7:\"Default\";i:1;s:7:\"Primary\";i:2;s:9:\"Secondary\";i:3;s:8:\"Tertiary\";}}s:38:\"tx_hivecptanchornav_bs4_class_section.\";a:1:{s:9:\"addItems.\";a:1:{s:26:\"additionalclass__fullwidth\";s:9:\"fullwidth\";}}s:34:\"tx_hivecptanchornav_bs4_align_row.\";a:1:{s:9:\"addItems.\";a:3:{s:17:\"align-items-start\";s:17:\"align-items-start\";s:18:\"align-items-center\";s:18:\"align-items-center\";s:15:\"align-items-end\";s:15:\"align-items-end\";}}s:39:\"tx_hivecptanchornav_bs4_no_gutters_row.\";a:1:{s:9:\"addItems.\";a:1:{s:10:\"no-gutters\";s:10:\"no-gutters\";}}s:35:\"tx_hivecptanchornav_bs4_align_col0.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col1.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col2.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col3.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col4.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:39:\"tx_hivecptanchornav_bs4_push_pull_col0.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col1.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col2.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col3.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}}}s:8:\"sections\";a:10:{s:32:\"186d8afa94e03e15bec43127c8542c0f\";s:110:\"[page|backend_layout = pagets__SectionAnchor100] || [page|backend_layout = pagets__SectionAnchor100WithOffset]\";s:32:\"6a50649d423563010cdb3622064e592b\";s:808:\"[page|backend_layout = pagets__SectionAnchor5050] || [page|backend_layout = pagets__SectionAnchor5050WithOffset] || [page|backend_layout = pagets__SectionAnchor2575] || [page|backend_layout = pagets__SectionAnchor2575WithOffset] || [page|backend_layout = pagets__SectionAnchor7525] || [page|backend_layout = pagets__SectionAnchor7525WithOffset] || [page|backend_layout = pagets__SectionAnchor3366] || [page|backend_layout = pagets__SectionAnchor3366WithOffset] || [page|backend_layout = pagets__SectionAnchor6633] || [page|backend_layout = pagets__SectionAnchor6633WithOffset] || [page|backend_layout = pagets__SectionAnchor4060] || [page|backend_layout = pagets__SectionAnchor4060WithOffset] || [page|backend_layout = pagets__SectionAnchor6040] || [page|backend_layout = pagets__SectionAnchor6040WithOffset]\";s:32:\"daaf67fbebeeba52b57d797900f22ba9\";s:116:\"[page|backend_layout = pagets__SectionAnchor333333] || [page|backend_layout = pagets__SectionAnchor333333WithOffset]\";s:32:\"6f8a4389477e2d21370146885ab065a1\";s:120:\"[page|backend_layout = pagets__SectionAnchor25252525] || [page|backend_layout = pagets__SectionAnchor25252525WithOffset]\";s:32:\"f509e84478712715f03fd83579de5901\";s:112:\"[page|backend_layout = pagets__SectionAnchor2575] || [page|backend_layout = pagets__SectionAnchor2575WithOffset]\";s:32:\"548cadce9b0815bfe96a402cfad10abe\";s:112:\"[page|backend_layout = pagets__SectionAnchor7525] || [page|backend_layout = pagets__SectionAnchor7525WithOffset]\";s:32:\"976c2e0633da0842a7c6d78ada6abb7a\";s:112:\"[page|backend_layout = pagets__SectionAnchor3366] || [page|backend_layout = pagets__SectionAnchor3366WithOffset]\";s:32:\"cc2a3b434c5362ff0f0d118a7aa4a2b6\";s:112:\"[page|backend_layout = pagets__SectionAnchor6633] || [page|backend_layout = pagets__SectionAnchor6633WithOffset]\";s:32:\"5f428ab939c84b4b1f14a48b191a6805\";s:112:\"[page|backend_layout = pagets__SectionAnchor4060] || [page|backend_layout = pagets__SectionAnchor4060WithOffset]\";s:32:\"9f2d51da85e2565d32f762b378d45fc9\";s:112:\"[page|backend_layout = pagets__SectionAnchor6040] || [page|backend_layout = pagets__SectionAnchor6040WithOffset]\";}s:5:\"match\";a:0:{}}i:1;s:32:\"5f8094b5df52e76d8a17bbc8004cef0c\";}'),(2,'eaa0ffa2db0b3f6820292e059110d999',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:2:{s:8:\"options.\";a:11:{s:15:\"enableBookmarks\";s:1:\"1\";s:10:\"file_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:10:\"selectable\";s:23:\"enableDisplayThumbnails\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:10:\"thumbnail.\";a:2:{s:5:\"width\";s:2:\"64\";s:6:\"height\";s:2:\"64\";}}s:9:\"pageTree.\";a:4:{s:31:\"doktypesToShowInNewPageDragArea\";s:37:\"1,6,4,7,3,254,255,199,116,117,118,119\";s:19:\"showPageIdWithTitle\";s:1:\"1\";s:16:\"backgroundColor.\";a:1:{i:0;s:7:\"#fafafa\";}s:23:\"showDomainNameWithTitle\";s:1:\"1\";}s:12:\"contextMenu.\";a:1:{s:6:\"table.\";a:3:{s:6:\"pages.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}s:9:\"sys_file.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}s:15:\"sys_filemounts.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}}}s:11:\"saveDocView\";s:1:\"1\";s:10:\"saveDocNew\";s:1:\"1\";s:11:\"saveDocNew.\";a:4:{s:5:\"pages\";s:1:\"1\";s:8:\"sys_file\";s:1:\"0\";s:17:\"sys_file_metadata\";s:1:\"0\";s:10:\"tt_content\";s:1:\"1\";}s:14:\"disableDelete.\";a:1:{s:8:\"sys_file\";s:1:\"1\";}s:11:\"clearCache.\";a:2:{s:5:\"pages\";s:1:\"1\";s:3:\"all\";s:1:\"1\";}s:13:\"saveClipboard\";s:1:\"1\";s:19:\"clipboardNumberPads\";s:1:\"1\";}s:9:\"admPanel.\";a:2:{s:7:\"enable.\";a:2:{s:4:\"edit\";s:1:\"0\";s:3:\"all\";s:1:\"1\";}s:9:\"override.\";a:1:{s:8:\"tsdebug.\";a:1:{s:20:\"forceTemplateParsing\";s:1:\"0\";}}}}s:8:\"sections\";a:0:{}s:5:\"match\";a:0:{}}i:1;s:32:\"1504ea0294e94c7f5bc0677276db8226\";}'),(3,'8ca70ca2359d16a666e14c14677f779e',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:3:{s:4:\"mod.\";a:5:{s:9:\"web_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:18:\"tableDisplayOrder.\";a:10:{s:9:\"be_users.\";a:1:{s:5:\"after\";s:9:\"be_groups\";}s:15:\"sys_filemounts.\";a:1:{s:5:\"after\";s:8:\"be_users\";}s:17:\"sys_file_storage.\";a:1:{s:5:\"after\";s:14:\"sys_filemounts\";}s:13:\"sys_language.\";a:1:{s:5:\"after\";s:16:\"sys_file_storage\";}s:9:\"fe_users.\";a:2:{s:5:\"after\";s:9:\"fe_groups\";s:6:\"before\";s:5:\"pages\";}s:13:\"sys_template.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:15:\"backend_layout.\";a:1:{s:5:\"after\";s:5:\"pages\";}s:11:\"sys_domain.\";a:1:{s:5:\"after\";s:12:\"sys_template\";}s:11:\"tt_content.\";a:1:{s:5:\"after\";s:33:\"pages,backend_layout,sys_template\";}s:13:\"sys_category.\";a:1:{s:5:\"after\";s:10:\"tt_content\";}}s:12:\"searchLevel.\";a:1:{s:6:\"items.\";a:6:{i:-1;s:82:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.infinite\";i:0;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.0\";i:1;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.1\";i:2;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.2\";i:3;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.3\";i:4;s:75:\"EXT:core/Resources/Private/Language/locallang_core.xlf:labels.searchLevel.4\";}}}s:8:\"wizards.\";a:2:{s:10:\"newRecord.\";a:1:{s:6:\"pages.\";a:1:{s:5:\"show.\";a:3:{s:10:\"pageInside\";s:1:\"1\";s:9:\"pageAfter\";s:1:\"1\";s:18:\"pageSelectPosition\";s:1:\"1\";}}}s:18:\"newContentElement.\";a:1:{s:12:\"wizardItems.\";a:6:{s:7:\"common.\";a:3:{s:9:\"elements.\";a:8:{s:7:\"header.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-header\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:6:\"header\";}}s:5:\"text.\";a:4:{s:14:\"iconIdentifier\";s:12:\"content-text\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"text\";}}s:8:\"textpic.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-textpic\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"textpic\";}}s:6:\"image.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-image\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"image\";}}s:10:\"textmedia.\";a:4:{s:14:\"iconIdentifier\";s:17:\"content-textmedia\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textMedia_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:9:\"textmedia\";}}s:8:\"bullets.\";a:4:{s:14:\"iconIdentifier\";s:15:\"content-bullets\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_bulletList_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"bullets\";}}s:6:\"table.\";a:4:{s:14:\"iconIdentifier\";s:13:\"content-table\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:5:\"table\";}}s:8:\"uploads.\";a:4:{s:14:\"iconIdentifier\";s:23:\"content-special-uploads\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:7:\"uploads\";}}}s:4:\"show\";s:57:\"header,text,textpic,image,textmedia,bullets,table,uploads\";s:6:\"header\";s:81:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common\";}s:5:\"menu.\";a:3:{s:9:\"elements.\";a:11:{s:14:\"menu_abstract.\";a:4:{s:14:\"iconIdentifier\";s:21:\"content-menu-abstract\";s:5:\"title\";s:94:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_abstract.title\";s:11:\"description\";s:100:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_abstract.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:13:\"menu_abstract\";}}s:25:\"menu_categorized_content.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-menu-categorized\";s:5:\"title\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_content.title\";s:11:\"description\";s:111:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_content.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:24:\"menu_categorized_content\";}}s:23:\"menu_categorized_pages.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-menu-categorized\";s:5:\"title\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_pages.title\";s:11:\"description\";s:109:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_categorized_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:22:\"menu_categorized_pages\";}}s:11:\"menu_pages.\";a:4:{s:14:\"iconIdentifier\";s:18:\"content-menu-pages\";s:5:\"title\";s:91:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_pages.title\";s:11:\"description\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:10:\"menu_pages\";}}s:14:\"menu_subpages.\";a:4:{s:14:\"iconIdentifier\";s:18:\"content-menu-pages\";s:5:\"title\";s:94:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_subpages.title\";s:11:\"description\";s:100:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_subpages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:13:\"menu_subpages\";}}s:22:\"menu_recently_updated.\";a:4:{s:14:\"iconIdentifier\";s:29:\"content-menu-recently-updated\";s:5:\"title\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_recently_updated.title\";s:11:\"description\";s:108:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_recently_updated.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:21:\"menu_recently_updated\";}}s:19:\"menu_related_pages.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-related\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_related_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_related_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_related_pages\";}}s:13:\"menu_section.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-section\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section.title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:12:\"menu_section\";}}s:19:\"menu_section_pages.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-section\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_section_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_section_pages\";}}s:13:\"menu_sitemap.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-menu-sitemap\";s:5:\"title\";s:93:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap.title\";s:11:\"description\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:12:\"menu_sitemap\";}}s:19:\"menu_sitemap_pages.\";a:4:{s:14:\"iconIdentifier\";s:26:\"content-menu-sitemap-pages\";s:5:\"title\";s:99:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap_pages.title\";s:11:\"description\";s:105:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu_sitemap_pages.description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"menu_sitemap_pages\";}}}s:4:\"show\";s:191:\"menu_abstract,menu_categorized_content,menu_categorized_pages,menu_pages,menu_subpages,menu_recently_updated,menu_related_pages,menu_section,menu_section_pages,menu_sitemap,menu_sitemap_pages\";s:6:\"header\";s:79:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:menu\";}s:8:\"special.\";a:3:{s:9:\"elements.\";a:3:{s:5:\"html.\";a:4:{s:14:\"iconIdentifier\";s:20:\"content-special-html\";s:5:\"title\";s:98:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_title\";s:11:\"description\";s:104:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_plainHTML_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"html\";}}s:4:\"div.\";a:4:{s:14:\"iconIdentifier\";s:19:\"content-special-div\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:3:\"div\";}}s:9:\"shortcut.\";a:4:{s:14:\"iconIdentifier\";s:24:\"content-special-shortcut\";s:5:\"title\";s:97:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_title\";s:11:\"description\";s:103:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_shortcut_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:8:\"shortcut\";}}}s:4:\"show\";s:17:\"html,div,shortcut\";s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special\";}s:6:\"forms.\";a:3:{s:4:\"show\";s:13:\"formframework\";s:9:\"elements.\";a:1:{s:14:\"formframework.\";a:4:{s:14:\"iconIdentifier\";s:12:\"content-form\";s:5:\"title\";s:75:\"LLL:EXT:form/Resources/Private/Language/locallang.xlf:form_new_wizard_title\";s:11:\"description\";s:77:\"LLL:EXT:form/Resources/Private/Language/locallang:form_new_wizard_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:18:\"form_formframework\";}}}s:6:\"header\";s:80:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:forms\";}s:8:\"plugins.\";a:3:{s:6:\"header\";s:82:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins\";s:9:\"elements.\";a:1:{s:8:\"general.\";a:4:{s:14:\"iconIdentifier\";s:14:\"content-plugin\";s:5:\"title\";s:96:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_title\";s:11:\"description\";s:102:\"LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:plugins_general_description\";s:21:\"tt_content_defValues.\";a:1:{s:5:\"CType\";s:4:\"list\";}}}s:4:\"show\";s:1:\"*\";}s:5:\"hive.\";a:4:{s:6:\"header\";s:4:\"Hive\";s:5:\"after\";s:33:\"common,special,menu,plugins,forms\";s:9:\"elements.\";a:2:{s:42:\"hivecptnavanchornavigationrendernavanchor.\";a:4:{s:14:\"iconIdentifier\";s:24:\"hive_cpt_brand_32x32_svg\";s:5:\"title\";s:17:\"Anchor Navigation\";s:11:\"description\";s:35:\"Anchor navigation for selected page\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:4:\"list\";s:9:\"list_type\";s:58:\"hivecptnavanchor_hivecptnavanchornavigationrendernavanchor\";}}s:17:\"hive_cpt_cnt_img.\";a:4:{s:14:\"iconIdentifier\";s:24:\"hive_cpt_brand_32x32_svg\";s:5:\"title\";s:9:\"FAL Image\";s:11:\"description\";s:27:\"Pure FAL Image without Flux\";s:21:\"tt_content_defValues.\";a:2:{s:5:\"CType\";s:4:\"list\";s:9:\"list_type\";s:39:\"hivecptcntimg_hivecptcntimgshowfalimage\";}}}s:4:\"show\";s:58:\"hivecptnavanchornavigationrendernavanchor,hive_cpt_cnt_img\";}}}}s:9:\"web_view.\";a:1:{s:19:\"previewFrameWidths.\";a:12:{s:5:\"1920.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1920\";s:6:\"height\";s:4:\"1080\";}s:5:\"1366.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1366\";s:6:\"height\";s:3:\"768\";}s:5:\"1280.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1280\";s:6:\"height\";s:4:\"1024\";}s:5:\"1024.\";a:4:{s:5:\"label\";s:66:\"LLL:EXT:viewpage/Resources/Private/Language/locallang.xlf:computer\";s:4:\"type\";s:7:\"desktop\";s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:3:\"768\";}s:7:\"nexus7.\";a:4:{s:5:\"label\";s:7:\"Nexus 7\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:3:\"600\";s:6:\"height\";s:3:\"960\";}s:8:\"nexus6p.\";a:4:{s:5:\"label\";s:8:\"Nexus 6P\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"411\";s:6:\"height\";s:3:\"731\";}s:8:\"ipadpro.\";a:4:{s:5:\"label\";s:8:\"iPad Pro\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:4:\"1024\";s:6:\"height\";s:4:\"1366\";}s:8:\"ipadair.\";a:4:{s:5:\"label\";s:8:\"iPad Air\";s:4:\"type\";s:6:\"tablet\";s:5:\"width\";s:3:\"768\";s:6:\"height\";s:4:\"1024\";}s:12:\"iphone7plus.\";a:4:{s:5:\"label\";s:13:\"iPhone 7 Plus\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"414\";s:6:\"height\";s:3:\"736\";}s:8:\"iphone6.\";a:4:{s:5:\"label\";s:8:\"iPhone 6\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"375\";s:6:\"height\";s:3:\"667\";}s:8:\"iphone5.\";a:4:{s:5:\"label\";s:8:\"iPhone 5\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"320\";s:6:\"height\";s:3:\"568\";}s:8:\"iphone4.\";a:4:{s:5:\"label\";s:8:\"iPhone 4\";s:4:\"type\";s:6:\"mobile\";s:5:\"width\";s:3:\"320\";s:6:\"height\";s:3:\"480\";}}}s:9:\"web_info.\";a:1:{s:17:\"fieldDefinitions.\";a:3:{s:2:\"0.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_0\";s:6:\"fields\";s:81:\"title,uid,slug,alias,starttime,endtime,fe_group,target,url,shortcut,shortcut_mode\";}s:2:\"1.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_1\";s:6:\"fields\";s:26:\"title,uid,###ALL_TABLES###\";}s:2:\"2.\";a:2:{s:5:\"label\";s:69:\"LLL:EXT:info/Resources/Private/Language/locallang_webinfo.xlf:pages_2\";s:6:\"fields\";s:93:\"title,uid,lastUpdated,newUntil,cache_timeout,php_tree_stop,TSconfig,is_siteroot,fe_login_mode\";}}}s:11:\"web_layout.\";a:1:{s:15:\"BackendLayouts.\";a:27:{s:17:\"SectionAnchor100.\";a:3:{s:5:\"title\";s:44:\"hive Backend Layout :: Section A/B/C :: 100%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:44:\"[0] SECTION A/B/C :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:79:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor.gif\";}s:27:\"SectionAnchor100WithOffset.\";a:3:{s:5:\"title\";s:55:\"hive Backend Layout :: SectionA/B/C with Offset :: 100%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:44:\"[0] SECTION A/B/C :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:89:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchorWithOffset.gif\";}s:24:\"SectionAnchor2020202020.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C :: 20% 20% 20% 20% 20%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:5:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"2\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"2\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"2\";}s:2:\"4.\";a:3:{s:4:\"name\";s:24:\"[3] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"2\";}s:2:\"5.\";a:3:{s:4:\"name\";s:24:\"[4] SECTION A/B/C :: 20%\";s:6:\"colPos\";s:1:\"4\";s:7:\"colspan\";s:1:\"2\";}}}}}}s:4:\"icon\";s:89:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2020202020.gif\";}s:34:\"SectionAnchor2020202020WithOffset.\";a:3:{s:5:\"title\";s:71:\"hive Backend Layout :: Section A/B/C with Offset :: 20% 20% 20% 20% 20%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:5:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"2\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"2\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"2\";}s:2:\"4.\";a:3:{s:4:\"name\";s:36:\"[3] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"2\";}s:2:\"5.\";a:3:{s:4:\"name\";s:36:\"[4] SECTION A/B/C with Offset :: 20%\";s:6:\"colPos\";s:1:\"4\";s:7:\"colspan\";s:1:\"2\";}}}}}}s:4:\"icon\";s:99:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2020202020WithOffset.gif\";}s:22:\"SectionAnchor25252525.\";a:3:{s:5:\"title\";s:55:\"hive Backend Layout :: Section A/B/C :: 25% 25% 25% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:4:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"3\";}s:2:\"4.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:87:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor25252525.gif\";}s:32:\"SectionAnchor25252525WithOffset.\";a:3:{s:5:\"title\";s:67:\"hive Backend Layout :: Section A/B/C with Offset :: 25% 25% 25% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:4:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"3\";}s:2:\"4.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"3\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:97:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor25252525WithOffset.gif\";}s:18:\"SectionAnchor2575.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 25% 75%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 75%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"9\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2575.gif\";}s:28:\"SectionAnchor2575WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 25% 75%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"3\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 75%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"9\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2575WithOffset.gif\";}s:20:\"SectionAnchor333333.\";a:3:{s:5:\"title\";s:51:\"hive Backend Layout :: Section A/B/C :: 33% 33% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:3:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}s:2:\"3.\";a:3:{s:4:\"name\";s:24:\"[2] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:85:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor333333.gif\";}s:30:\"SectionAnchor333333WithOffset.\";a:3:{s:5:\"title\";s:63:\"hive Backend Layout :: Section A/B/C with Offset :: 33% 33% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:3:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}s:2:\"3.\";a:3:{s:4:\"name\";s:36:\"[2] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"2\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:95:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor333333WithOffset.gif\";}s:18:\"SectionAnchor3366.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 33% 66%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 66%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"8\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor3366.gif\";}s:28:\"SectionAnchor3366WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 33% 66%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"4\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 66%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"8\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor3366WithOffset.gif\";}s:18:\"SectionAnchor4060.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 40% 60%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 40%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"5\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 60%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"7\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor4060.gif\";}s:28:\"SectionAnchor4060WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 40% 60%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 40%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"5\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 60%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"7\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor4060WithOffset.gif\";}s:18:\"SectionAnchor5050.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 50% 50%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 50%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"6\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 50%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"6\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor5050.gif\";}s:28:\"SectionAnchor5050WithOffset.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C with Offset :: 50% 50%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 50%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"6\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 50%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"6\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor5050WithOffset.gif\";}s:18:\"SectionAnchor6040.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 60% 40%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 60%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"7\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 40%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"5\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6040.gif\";}s:28:\"SectionAnchor6040WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 60% 40%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 60%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"7\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 40%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"5\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6040WithOffset.gif\";}s:18:\"SectionAnchor6633.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 66% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 66%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"8\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6633.gif\";}s:28:\"SectionAnchor6633WithOffset.\";a:3:{s:5:\"title\";s:59:\"hive Backend Layout :: Section A/B/C with Offset :: 66% 33%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 66%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"8\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 33%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"4\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6633WithOffset.gif\";}s:18:\"SectionAnchor7525.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Section A/B/C :: 75% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:24:\"[0] SECTION A/B/C :: 75%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"9\";}s:2:\"2.\";a:3:{s:4:\"name\";s:24:\"[1] SECTION A/B/C :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:83:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor7525.gif\";}s:28:\"SectionAnchor7525WithOffset.\";a:3:{s:5:\"title\";s:58:\"hive Backend Layout :: Section A/B/C with Offset:: 75% 25%\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:2:{s:2:\"1.\";a:3:{s:4:\"name\";s:36:\"[0] SECTION A/B/C with Offset :: 75%\";s:6:\"colPos\";s:1:\"0\";s:7:\"colspan\";s:1:\"9\";}s:2:\"2.\";a:3:{s:4:\"name\";s:36:\"[1] SECTION A/B/C with Offset :: 25%\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:1:\"3\";}}}}}}s:4:\"icon\";s:93:\"EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor7525WithOffset.gif\";}s:13:\"CustomAnchor.\";a:3:{s:5:\"title\";s:47:\"hive Backend Layout :: Custom :: HTML :: Anchor\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:48:\"[1] Anchor :: TopPage :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:21:\"CustomBasicOneColumn.\";a:3:{s:5:\"title\";s:64:\"hive Backend Layout :: Custom :: HTML :: Custom Basic One Column\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:28:\"[10] Custom Basic One Column\";s:6:\"colPos\";s:2:\"10\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:16:\"CustomPrototype.\";a:3:{s:5:\"title\";s:50:\"hive Backend Layout :: Custom :: HTML :: Prototype\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:35:\"[1] TEST :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:8:\"Default.\";a:3:{s:5:\"title\";s:48:\"hive Backend Layout :: Custom :: HTML :: Default\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:1:\"1\";s:5:\"rows.\";a:1:{s:2:\"1.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:35:\"[1] TEST :: NOT VISIBLE IN FRONTEND\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}s:6:\"Slide.\";a:3:{s:5:\"title\";s:46:\"hive Backend Layout :: Custom :: HTML :: Slide\";s:7:\"config.\";a:1:{s:15:\"backend_layout.\";a:3:{s:8:\"colCount\";s:2:\"12\";s:8:\"rowCount\";s:4:\"9999\";s:5:\"rows.\";a:1:{s:3:\"10.\";a:1:{s:8:\"columns.\";a:1:{s:2:\"1.\";a:3:{s:4:\"name\";s:18:\"[1] Slide :: Image\";s:6:\"colPos\";s:1:\"1\";s:7:\"colspan\";s:2:\"12\";}}}}}}s:4:\"icon\";s:70:\"EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif\";}}}}s:8:\"TCEMAIN.\";a:2:{s:18:\"translateToMessage\";s:16:\"Translate to %s:\";s:12:\"linkHandler.\";a:5:{s:5:\"page.\";a:2:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\PageLinkHandler\";s:5:\"label\";s:77:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:page\";}s:5:\"file.\";a:4:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FileLinkHandler\";s:5:\"label\";s:77:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:file\";s:12:\"displayAfter\";s:4:\"page\";s:9:\"scanAfter\";s:4:\"page\";}s:7:\"folder.\";a:4:{s:7:\"handler\";s:50:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\FolderLinkHandler\";s:5:\"label\";s:79:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:folder\";s:12:\"displayAfter\";s:4:\"file\";s:9:\"scanAfter\";s:4:\"file\";}s:4:\"url.\";a:4:{s:7:\"handler\";s:47:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\UrlLinkHandler\";s:5:\"label\";s:79:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:extUrl\";s:12:\"displayAfter\";s:6:\"folder\";s:9:\"scanAfter\";s:4:\"mail\";}s:5:\"mail.\";a:3:{s:7:\"handler\";s:48:\"TYPO3\\CMS\\Recordlist\\LinkHandler\\MailLinkHandler\";s:5:\"label\";s:78:\"LLL:EXT:recordlist/Resources/Private/Language/locallang_browse_links.xlf:email\";s:12:\"displayAfter\";s:3:\"url\";}}}s:8:\"TCEFORM.\";a:2:{s:11:\"tt_content.\";a:1:{s:12:\"imageorient.\";a:1:{s:6:\"types.\";a:1:{s:6:\"image.\";a:1:{s:11:\"removeItems\";s:18:\"8,9,10,17,18,25,26\";}}}}s:6:\"pages.\";a:13:{s:7:\"layout.\";a:2:{s:9:\"addItems.\";a:3:{i:4;s:1:\"A\";i:5;s:1:\"B\";i:6;s:1:\"C\";}s:10:\"altLabels.\";a:4:{i:0;s:7:\"Default\";i:1;s:7:\"Primary\";i:2;s:9:\"Secondary\";i:3;s:8:\"Tertiary\";}}s:38:\"tx_hivecptanchornav_bs4_class_section.\";a:1:{s:9:\"addItems.\";a:1:{s:26:\"additionalclass__fullwidth\";s:9:\"fullwidth\";}}s:34:\"tx_hivecptanchornav_bs4_align_row.\";a:1:{s:9:\"addItems.\";a:3:{s:17:\"align-items-start\";s:17:\"align-items-start\";s:18:\"align-items-center\";s:18:\"align-items-center\";s:15:\"align-items-end\";s:15:\"align-items-end\";}}s:39:\"tx_hivecptanchornav_bs4_no_gutters_row.\";a:1:{s:9:\"addItems.\";a:1:{s:10:\"no-gutters\";s:10:\"no-gutters\";}}s:35:\"tx_hivecptanchornav_bs4_align_col0.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col1.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col2.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col3.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:35:\"tx_hivecptanchornav_bs4_align_col4.\";a:1:{s:9:\"addItems.\";a:3:{s:16:\"align-self-start\";s:16:\"align-self-start\";s:17:\"align-self-center\";s:17:\"align-self-center\";s:14:\"align-self-end\";s:14:\"align-self-end\";}}s:39:\"tx_hivecptanchornav_bs4_push_pull_col0.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col1.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col2.\";a:1:{s:8:\"disabled\";s:1:\"1\";}s:39:\"tx_hivecptanchornav_bs4_push_pull_col3.\";a:1:{s:8:\"disabled\";s:1:\"1\";}}}}s:8:\"sections\";a:10:{s:32:\"186d8afa94e03e15bec43127c8542c0f\";s:110:\"[page|backend_layout = pagets__SectionAnchor100] || [page|backend_layout = pagets__SectionAnchor100WithOffset]\";s:32:\"6a50649d423563010cdb3622064e592b\";s:808:\"[page|backend_layout = pagets__SectionAnchor5050] || [page|backend_layout = pagets__SectionAnchor5050WithOffset] || [page|backend_layout = pagets__SectionAnchor2575] || [page|backend_layout = pagets__SectionAnchor2575WithOffset] || [page|backend_layout = pagets__SectionAnchor7525] || [page|backend_layout = pagets__SectionAnchor7525WithOffset] || [page|backend_layout = pagets__SectionAnchor3366] || [page|backend_layout = pagets__SectionAnchor3366WithOffset] || [page|backend_layout = pagets__SectionAnchor6633] || [page|backend_layout = pagets__SectionAnchor6633WithOffset] || [page|backend_layout = pagets__SectionAnchor4060] || [page|backend_layout = pagets__SectionAnchor4060WithOffset] || [page|backend_layout = pagets__SectionAnchor6040] || [page|backend_layout = pagets__SectionAnchor6040WithOffset]\";s:32:\"daaf67fbebeeba52b57d797900f22ba9\";s:116:\"[page|backend_layout = pagets__SectionAnchor333333] || [page|backend_layout = pagets__SectionAnchor333333WithOffset]\";s:32:\"6f8a4389477e2d21370146885ab065a1\";s:120:\"[page|backend_layout = pagets__SectionAnchor25252525] || [page|backend_layout = pagets__SectionAnchor25252525WithOffset]\";s:32:\"f509e84478712715f03fd83579de5901\";s:112:\"[page|backend_layout = pagets__SectionAnchor2575] || [page|backend_layout = pagets__SectionAnchor2575WithOffset]\";s:32:\"548cadce9b0815bfe96a402cfad10abe\";s:112:\"[page|backend_layout = pagets__SectionAnchor7525] || [page|backend_layout = pagets__SectionAnchor7525WithOffset]\";s:32:\"976c2e0633da0842a7c6d78ada6abb7a\";s:112:\"[page|backend_layout = pagets__SectionAnchor3366] || [page|backend_layout = pagets__SectionAnchor3366WithOffset]\";s:32:\"cc2a3b434c5362ff0f0d118a7aa4a2b6\";s:112:\"[page|backend_layout = pagets__SectionAnchor6633] || [page|backend_layout = pagets__SectionAnchor6633WithOffset]\";s:32:\"5f428ab939c84b4b1f14a48b191a6805\";s:112:\"[page|backend_layout = pagets__SectionAnchor4060] || [page|backend_layout = pagets__SectionAnchor4060WithOffset]\";s:32:\"9f2d51da85e2565d32f762b378d45fc9\";s:112:\"[page|backend_layout = pagets__SectionAnchor6040] || [page|backend_layout = pagets__SectionAnchor6040WithOffset]\";}s:5:\"match\";a:0:{}}i:1;s:32:\"5f8094b5df52e76d8a17bbc8004cef0c\";}'),(4,'c86790f9b1f60a6f5ef5e2a15eb88117',2145909600,'a:2:{i:0;a:3:{s:8:\"TSconfig\";a:2:{s:8:\"options.\";a:8:{s:15:\"enableBookmarks\";s:1:\"1\";s:10:\"file_list.\";a:4:{s:28:\"enableDisplayBigControlPanel\";s:10:\"selectable\";s:23:\"enableDisplayThumbnails\";s:10:\"selectable\";s:15:\"enableClipBoard\";s:10:\"selectable\";s:10:\"thumbnail.\";a:2:{s:5:\"width\";s:2:\"64\";s:6:\"height\";s:2:\"64\";}}s:9:\"pageTree.\";a:1:{s:31:\"doktypesToShowInNewPageDragArea\";s:21:\"1,6,4,7,3,254,255,199\";}s:12:\"contextMenu.\";a:1:{s:6:\"table.\";a:3:{s:6:\"pages.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}s:9:\"sys_file.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}s:15:\"sys_filemounts.\";a:2:{s:12:\"disableItems\";s:0:\"\";s:5:\"tree.\";a:1:{s:12:\"disableItems\";s:0:\"\";}}}}s:11:\"saveDocView\";s:1:\"1\";s:10:\"saveDocNew\";s:1:\"1\";s:11:\"saveDocNew.\";a:3:{s:5:\"pages\";s:1:\"0\";s:8:\"sys_file\";s:1:\"0\";s:17:\"sys_file_metadata\";s:1:\"0\";}s:14:\"disableDelete.\";a:1:{s:8:\"sys_file\";s:1:\"1\";}}s:9:\"admPanel.\";a:1:{s:7:\"enable.\";a:1:{s:3:\"all\";s:1:\"1\";}}}s:8:\"sections\";a:0:{}s:5:\"match\";a:0:{}}i:1;s:32:\"a5c661d673b61b952a5a7bb4d2f76ea8\";}'),(5,'efa955de67f970b5d0a3b231c66f125a',2145909600,'a:2:{s:9:\"constants\";a:3:{s:7:\"styles.\";a:2:{s:10:\"templates.\";a:3:{s:16:\"templateRootPath\";s:0:\"\";s:15:\"partialRootPath\";s:0:\"\";s:14:\"layoutRootPath\";s:0:\"\";}s:8:\"content.\";a:5:{s:17:\"defaultHeaderType\";s:1:\"2\";s:9:\"shortcut.\";a:1:{s:6:\"tables\";s:10:\"tt_content\";}s:9:\"allowTags\";s:392:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:10:\"textmedia.\";a:9:{s:4:\"maxW\";s:3:\"600\";s:10:\"maxWInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:10:\"rowSpacing\";s:2:\"10\";s:10:\"textMargin\";s:2:\"10\";s:11:\"borderColor\";s:7:\"#000000\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";s:9:\"linkWrap.\";a:6:{s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:9:\"newWindow\";s:1:\"0\";s:15:\"lightboxEnabled\";s:1:\"0\";s:16:\"lightboxCssClass\";s:8:\"lightbox\";s:20:\"lightboxRelAttribute\";s:21:\"lightbox[{field:uid}]\";}}s:6:\"links.\";a:2:{s:9:\"extTarget\";s:6:\"_blank\";s:4:\"keep\";s:4:\"path\";}}}s:7:\"plugin.\";a:10:{s:23:\"tx_hive_cfg_typoscript.\";a:1:{s:9:\"settings.\";a:6:{s:4:\"gulp\";s:1:\"1\";s:12:\"development.\";a:1:{s:7:\"config.\";a:5:{s:7:\"baseURL\";s:21:\"development.localhost\";s:10:\"compressJs\";s:1:\"9\";s:13:\"concatenateJs\";s:1:\"1\";s:11:\"compressCss\";s:1:\"9\";s:14:\"concatenateCss\";s:1:\"1\";}}s:8:\"staging.\";a:4:{s:7:\"config.\";a:5:{s:7:\"baseURL\";s:23:\"staging.musterdomain.de\";s:10:\"compressJs\";s:1:\"9\";s:13:\"concatenateJs\";s:1:\"1\";s:11:\"compressCss\";s:1:\"9\";s:14:\"concatenateCss\";s:1:\"1\";}s:5:\"meta.\";a:3:{s:8:\"viewport\";s:67:\"width=device-width,initial-scale=1, maximum-scale=1,minimum-scale=1\";s:8:\"siteName\";s:31:\"Bitte Name der Seiten einfuegen\";s:7:\"ogImage\";s:0:\"\";}s:12:\"includePath.\";a:1:{s:6:\"public\";s:41:\"EXT:hive_cfg_typoscript/Resources/Public/\";}s:8:\"classes.\";a:1:{s:4:\"body\";s:16:\"defaultBodyClass\";}}s:11:\"production.\";a:4:{s:7:\"config.\";a:5:{s:7:\"baseURL\";s:19:\"www.musterdomain.de\";s:10:\"compressJs\";s:1:\"9\";s:13:\"concatenateJs\";s:1:\"1\";s:11:\"compressCss\";s:1:\"9\";s:14:\"concatenateCss\";s:1:\"1\";}s:5:\"meta.\";a:3:{s:8:\"viewport\";s:67:\"width=device-width,initial-scale=1, maximum-scale=1,minimum-scale=1\";s:8:\"siteName\";s:31:\"Bitte Name der Seiten einfuegen\";s:7:\"ogImage\";s:0:\"\";}s:12:\"includePath.\";a:1:{s:6:\"public\";s:41:\"EXT:hive_cfg_typoscript/Resources/Public/\";}s:8:\"classes.\";a:1:{s:4:\"body\";s:16:\"defaultBodyClass\";}}s:5:\"auto.\";a:3:{s:8:\"protocol\";s:4:\"http\";s:7:\"bMobile\";s:1:\"0\";s:18:\"applicationContext\";s:37:\"applicationContext-Production/Staging\";}s:7:\"config.\";a:1:{s:8:\"dynamic.\";a:1:{s:8:\"baseURL.\";a:2:{s:6:\"prefix\";s:11:\"development\";s:4:\"host\";s:9:\"localhost\";}}}}}s:62:\"tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.\";a:2:{s:5:\"view.\";a:3:{s:16:\"templateRootPath\";s:52:\"EXT:hive_cpt_nav_anchor/Resources/Private/Templates/\";s:15:\"partialRootPath\";s:51:\"EXT:hive_cpt_nav_anchor/Resources/Private/Partials/\";s:14:\"layoutRootPath\";s:50:\"EXT:hive_cpt_nav_anchor/Resources/Private/Layouts/\";}s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:0:\"\";}}s:23:\"tx_hive_cpt_nav_anchor.\";a:1:{s:9:\"settings.\";a:2:{s:4:\"lib.\";a:1:{s:11:\"navigation.\";a:1:{s:10:\"bShowStart\";s:1:\"0\";}}s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:41:\"EXT:hive_cpt_nav_anchor/Resources/Public/\";s:7:\"private\";s:42:\"EXT:hive_cpt_nav_anchor/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:51:\"typo3conf/ext/hive_cpt_nav_anchor/Resources/Public/\";}}}}}s:43:\"tx_hivecptcntimg_hivecptcntimgshowfalimage.\";a:3:{s:5:\"view.\";a:3:{s:16:\"templateRootPath\";s:49:\"EXT:hive_cpt_cnt_img/Resources/Private/Templates/\";s:15:\"partialRootPath\";s:48:\"EXT:hive_cpt_cnt_img/Resources/Private/Partials/\";s:14:\"layoutRootPath\";s:47:\"EXT:hive_cpt_cnt_img/Resources/Private/Layouts/\";}s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:0:\"\";}s:9:\"settings.\";a:4:{s:6:\"bDebug\";s:1:\"0\";s:8:\"maxWidth\";s:5:\"1920c\";s:9:\"maxHeight\";s:5:\"1080c\";s:8:\"classes.\";a:4:{s:3:\"div\";s:17:\"caption container\";s:4:\"div1\";s:3:\"row\";s:4:\"div2\";s:3:\"col\";s:4:\"div3\";s:11:\"description\";}}}s:15:\"hive_cpt_brand.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:36:\"EXT:hive_cpt_brand/Resources/Public/\";s:7:\"private\";s:37:\"EXT:hive_cpt_brand/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:46:\"typo3conf/ext/hive_cpt_brand/Resources/Public/\";}}}}}s:15:\"tx_hive_thm_bs.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:33:\"EXT:hive_thm_bs/Resources/Public/\";s:7:\"private\";s:34:\"EXT:hive_thm_bs/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:43:\"typo3conf/ext/hive_thm_bs/Resources/Public/\";}}}}}s:18:\"tx_hive_thm_blazy.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:2:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:36:\"EXT:hive_thm_blazy/Resources/Public/\";s:7:\"private\";s:37:\"EXT:hive_thm_blazy/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:46:\"typo3conf/ext/hive_thm_blazy/Resources/Public/\";}}s:9:\"optional.\";a:1:{s:6:\"active\";s:1:\"1\";}}}}s:19:\"tx_hive_thm_custom.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:37:\"EXT:hive_thm_custom/Resources/Public/\";s:7:\"private\";s:38:\"EXT:hive_thm_custom/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:47:\"typo3conf/ext/hive_thm_custom/Resources/Public/\";}}}}}s:23:\"tx_hive_thm_bs_toolkit.\";a:1:{s:9:\"settings.\";a:1:{s:4:\"lib.\";a:1:{s:10:\"bsToolkit.\";a:1:{s:13:\"bUseBsToolkit\";s:1:\"1\";}}}}s:8:\"metaseo.\";a:3:{s:9:\"metaTags.\";a:10:{s:9:\"copyright\";s:49:\"<MUSTERFIRMA>, %YEAR%, <Alle Rechte vorbehalten.>\";s:5:\"email\";s:22:\"muster@musterfirma.tld\";s:6:\"author\";s:13:\"<MUSTERFIRMA>\";s:9:\"publisher\";s:13:\"<MUSTERFIRMA>\";s:8:\"language\";s:9:\"German=de\";s:19:\"geoPositionLatitude\";s:9:\"48.178605\";s:20:\"geoPositionLongitude\";s:8:\"8.616138\";s:9:\"geoRegion\";s:5:\"DE-BW\";s:12:\"geoPlacename\";s:13:\"<MUSTERSTADT>\";s:18:\"googleVerification\";s:0:\"\";}s:9:\"services.\";a:3:{s:15:\"googleAnalytics\";s:0:\"\";s:24:\"enableIfHeaderIsDisabled\";s:1:\"0\";s:16:\"googleAnalytics.\";a:4:{s:10:\"domainName\";s:0:\"\";s:11:\"anonymizeIp\";s:1:\"0\";s:14:\"trackDownloads\";s:1:\"0\";s:18:\"universalAnalytics\";s:1:\"0\";}}s:10:\"pageTitle.\";a:2:{s:13:\"sitetitleGlue\";s:2:\"::\";s:9:\"sitetitle\";s:22:\"<MUSTERWEBSEITE-TITEL>\";}}}s:18:\"applicationContext\";s:19:\"Production/Staging]\";}s:5:\"setup\";a:13:{s:7:\"config.\";a:33:{s:19:\"pageTitleProviders.\";a:3:{s:13:\"altPageTitle.\";a:2:{s:8:\"provider\";s:45:\"TYPO3\\CMS\\Core\\PageTitle\\AltPageTitleProvider\";s:6:\"before\";s:6:\"record\";}s:7:\"record.\";a:1:{s:8:\"provider\";s:48:\"TYPO3\\CMS\\Core\\PageTitle\\RecordPageTitleProvider\";}s:4:\"seo.\";a:3:{s:8:\"provider\";s:49:\"TYPO3\\CMS\\Seo\\PageTitle\\SeoTitlePageTitleProvider\";s:6:\"before\";s:6:\"record\";s:5:\"after\";s:12:\"altPageTitle\";}}s:7:\"doctype\";s:5:\"html5\";s:12:\"cache_period\";s:7:\"2592000\";s:16:\"sendCacheHeaders\";s:1:\"0\";s:8:\"no_cache\";s:1:\"0\";s:21:\"cache_clearAtMidnight\";s:1:\"0\";s:14:\"uniqueLinkVars\";s:1:\"1\";s:8:\"linkVars\";s:1:\"L\";s:21:\"typolinkCheckRootline\";s:1:\"1\";s:32:\"typolinkEnableLinksAcrossDomains\";s:1:\"1\";s:13:\"renderCharset\";s:5:\"utf-8\";s:11:\"metaCharset\";s:5:\"utf-8\";s:20:\"inlineStyle2TempFile\";s:1:\"1\";s:8:\"admPanel\";s:1:\"0\";s:9:\"intTarget\";s:0:\"\";s:9:\"extTarget\";s:0:\"\";s:12:\"index_enable\";s:1:\"1\";s:15:\"index_externals\";s:1:\"1\";s:14:\"index_metatags\";s:1:\"0\";s:16:\"sys_language_uid\";s:1:\"0\";s:8:\"language\";s:2:\"en\";s:10:\"locale_all\";s:11:\"en_US.UTF-8\";s:15:\"htmlTag_langKey\";s:2:\"en\";s:10:\"compressJs\";s:1:\"9\";s:11:\"compressCss\";s:1:\"9\";s:13:\"concatenateJs\";s:1:\"1\";s:14:\"concatenateCss\";s:1:\"1\";s:20:\"disablePrefixComment\";s:1:\"1\";s:24:\"pageRendererTemplateFile\";s:69:\"EXT:hive_cfg_typoscript/Resources/Private/Templates/PageRenderer.html\";s:7:\"baseURL\";s:29:\"http://development.localhost/\";s:17:\"sys_language_mode\";s:16:\"content_fallback\";s:20:\"sys_language_overlay\";s:1:\"1\";s:11:\"tx_extbase.\";a:3:{s:4:\"mvc.\";a:2:{s:16:\"requestHandlers.\";a:4:{s:48:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\FrontendRequestHandler\";s:48:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\FrontendRequestHandler\";s:47:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\BackendRequestHandler\";s:47:\"TYPO3\\CMS\\Extbase\\Mvc\\Web\\BackendRequestHandler\";s:40:\"TYPO3\\CMS\\Extbase\\Mvc\\Cli\\RequestHandler\";s:40:\"TYPO3\\CMS\\Extbase\\Mvc\\Cli\\RequestHandler\";s:48:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\WidgetRequestHandler\";s:48:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\WidgetRequestHandler\";}s:48:\"throwPageNotFoundExceptionIfActionCantBeResolved\";s:1:\"0\";}s:12:\"persistence.\";a:3:{s:28:\"enableAutomaticCacheClearing\";s:1:\"1\";s:20:\"updateReferenceIndex\";s:1:\"0\";s:8:\"classes.\";a:10:{s:41:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FileMount.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:14:\"sys_filemounts\";s:8:\"columns.\";a:3:{s:6:\"title.\";a:1:{s:13:\"mapOnProperty\";s:5:\"title\";}s:5:\"path.\";a:1:{s:13:\"mapOnProperty\";s:4:\"path\";}s:5:\"base.\";a:1:{s:13:\"mapOnProperty\";s:14:\"isAbsolutePath\";}}}}s:45:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FileReference.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:18:\"sys_file_reference\";}}s:36:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\File.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:8:\"sys_file\";}}s:43:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\BackendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"be_users\";s:8:\"columns.\";a:8:{s:9:\"username.\";a:1:{s:13:\"mapOnProperty\";s:8:\"userName\";}s:6:\"admin.\";a:1:{s:13:\"mapOnProperty\";s:15:\"isAdministrator\";}s:8:\"disable.\";a:1:{s:13:\"mapOnProperty\";s:10:\"isDisabled\";}s:9:\"realName.\";a:1:{s:13:\"mapOnProperty\";s:8:\"realName\";}s:10:\"starttime.\";a:1:{s:13:\"mapOnProperty\";s:16:\"startDateAndTime\";}s:8:\"endtime.\";a:1:{s:13:\"mapOnProperty\";s:14:\"endDateAndTime\";}s:14:\"disableIPlock.\";a:1:{s:13:\"mapOnProperty\";s:16:\"ipLockIsDisabled\";}s:10:\"lastlogin.\";a:1:{s:13:\"mapOnProperty\";s:20:\"lastLoginDateAndTime\";}}}}s:48:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\BackendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"be_groups\";s:8:\"columns.\";a:13:{s:9:\"subgroup.\";a:1:{s:13:\"mapOnProperty\";s:9:\"subGroups\";}s:10:\"groupMods.\";a:1:{s:13:\"mapOnProperty\";s:7:\"modules\";}s:14:\"tables_select.\";a:1:{s:13:\"mapOnProperty\";s:15:\"tablesListening\";}s:14:\"tables_modify.\";a:1:{s:13:\"mapOnProperty\";s:12:\"tablesModify\";}s:17:\"pagetypes_select.\";a:1:{s:13:\"mapOnProperty\";s:9:\"pageTypes\";}s:19:\"non_exclude_fields.\";a:1:{s:13:\"mapOnProperty\";s:20:\"allowedExcludeFields\";}s:19:\"explicit_allowdeny.\";a:1:{s:13:\"mapOnProperty\";s:22:\"explicitlyAllowAndDeny\";}s:18:\"allowed_languages.\";a:1:{s:13:\"mapOnProperty\";s:16:\"allowedLanguages\";}s:16:\"workspace_perms.\";a:1:{s:13:\"mapOnProperty\";s:19:\"workspacePermission\";}s:15:\"db_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:14:\"databaseMounts\";}s:17:\"file_permissions.\";a:1:{s:13:\"mapOnProperty\";s:24:\"fileOperationPermissions\";}s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}s:9:\"TSconfig.\";a:1:{s:13:\"mapOnProperty\";s:8:\"tsConfig\";}}}}s:44:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FrontendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"fe_users\";s:8:\"columns.\";a:1:{s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}}}}s:49:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\FrontendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"fe_groups\";s:8:\"columns.\";a:1:{s:13:\"lockToDomain.\";a:1:{s:13:\"mapOnProperty\";s:12:\"lockToDomain\";}}}}s:40:\"TYPO3\\CMS\\Extbase\\Domain\\Model\\Category.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:12:\"sys_category\";}}s:42:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUser.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:8:\"be_users\";s:8:\"columns.\";a:4:{s:18:\"allowed_languages.\";a:1:{s:13:\"mapOnProperty\";s:16:\"allowedLanguages\";}s:17:\"file_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:15:\"fileMountPoints\";}s:15:\"db_mountpoints.\";a:1:{s:13:\"mapOnProperty\";s:13:\"dbMountPoints\";}s:10:\"usergroup.\";a:1:{s:13:\"mapOnProperty\";s:17:\"backendUserGroups\";}}}}s:47:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUserGroup.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:9:\"be_groups\";s:8:\"columns.\";a:1:{s:9:\"subgroup.\";a:1:{s:13:\"mapOnProperty\";s:9:\"subGroups\";}}}}}}s:9:\"features.\";a:4:{s:20:\"skipDefaultArguments\";s:1:\"0\";s:25:\"ignoreAllEnableFieldsInBe\";s:1:\"0\";s:38:\"requireCHashArgumentForActionArguments\";s:1:\"1\";s:36:\"consistentTranslationOverlayHandling\";s:1:\"1\";}}}s:7:\"styles.\";a:1:{s:8:\"content.\";a:2:{s:3:\"get\";s:7:\"CONTENT\";s:4:\"get.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:2:{s:7:\"orderBy\";s:7:\"sorting\";s:5:\"where\";s:11:\"{#colPos}=0\";}}}}s:10:\"tt_content\";s:4:\"CASE\";s:11:\"tt_content.\";a:52:{s:4:\"key.\";a:1:{s:5:\"field\";s:5:\"CType\";}s:7:\"default\";s:4:\"TEXT\";s:8:\"default.\";a:4:{s:5:\"field\";s:5:\"CType\";s:16:\"htmlSpecialChars\";s:1:\"1\";s:4:\"wrap\";s:165:\"<p style=\"background-color: yellow; padding: 0.5em 1em;\"><strong>ERROR:</strong> Content Element with uid \"{field:uid}\" and type \"|\" has no rendering definition!</p>\";s:5:\"wrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editPanel\";s:1:\"1\";s:10:\"editPanel.\";a:5:{s:5:\"allow\";s:29:\"move, new, edit, hide, delete\";s:5:\"label\";s:2:\"%s\";s:14:\"onlyCurrentPid\";s:1:\"1\";s:13:\"previewBorder\";s:1:\"1\";s:5:\"edit.\";a:1:{s:13:\"displayRecord\";s:1:\"1\";}}}s:7:\"bullets\";s:20:\"< lib.contentElement\";s:8:\"bullets.\";a:3:{s:12:\"templateName\";s:7:\"Bullets\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\SplitProcessor\";s:3:\"10.\";a:4:{s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:11:\"isLessThan.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:9:\"fieldName\";s:8:\"bodytext\";s:18:\"removeEmptyEntries\";s:1:\"1\";s:2:\"as\";s:7:\"bullets\";}i:20;s:62:\"TYPO3\\CMS\\Frontend\\DataProcessing\\CommaSeparatedValueProcessor\";s:3:\"20.\";a:4:{s:9:\"fieldName\";s:8:\"bodytext\";s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:7:\"equals.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:14:\"fieldDelimiter\";s:1:\"|\";s:2:\"as\";s:7:\"bullets\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:59:\"tt_content: header [header_layout], bodytext [bullets_type]\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:92:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.bullets\";}}}}s:3:\"div\";s:20:\"< lib.contentElement\";s:4:\"div.\";a:1:{s:12:\"templateName\";s:3:\"Div\";}s:6:\"header\";s:20:\"< lib.contentElement\";s:7:\"header.\";a:2:{s:12:\"templateName\";s:6:\"Header\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:63:\"tt_content: header [header_layout|header_link], subheader, date\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:91:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.header\";}}}}s:4:\"html\";s:20:\"< lib.contentElement\";s:5:\"html.\";a:2:{s:12:\"templateName\";s:4:\"Html\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:20:\"tt_content: bodytext\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.html\";}}}}s:5:\"image\";s:20:\"< lib.contentElement\";s:6:\"image.\";a:3:{s:12:\"templateName\";s:5:\"Image\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}i:20;s:50:\"TYPO3\\CMS\\Frontend\\DataProcessing\\GalleryProcessor\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:92:\"tt_content : image [imageorient|imagewidth|imageheight], [imagecols|imageborder], image_zoom\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:90:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.image\";}}}}s:4:\"list\";s:20:\"< lib.contentElement\";s:5:\"list.\";a:3:{s:12:\"templateName\";s:4:\"List\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:72:\"tt_content: header [header_layout], list_type, layout, pages [recursive]\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.list\";}}}s:3:\"20.\";a:4:{s:58:\"hivecptnavanchor_hivecptnavanchornavigationrendernavanchor\";s:4:\"USER\";s:59:\"hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.\";a:4:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:16:\"HiveCptNavAnchor\";s:10:\"pluginName\";s:41:\"Hivecptnavanchornavigationrendernavanchor\";s:10:\"vendorName\";s:4:\"HIVE\";}s:39:\"hivecptcntimg_hivecptcntimgshowfalimage\";s:4:\"USER\";s:40:\"hivecptcntimg_hivecptcntimgshowfalimage.\";a:4:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:13:\"HiveCptCntImg\";s:10:\"pluginName\";s:25:\"Hivecptcntimgshowfalimage\";s:10:\"vendorName\";s:4:\"HIVE\";}}}s:8:\"shortcut\";s:20:\"< lib.contentElement\";s:9:\"shortcut.\";a:3:{s:12:\"templateName\";s:8:\"Shortcut\";s:10:\"variables.\";a:2:{s:9:\"shortcuts\";s:7:\"RECORDS\";s:10:\"shortcuts.\";a:2:{s:7:\"source.\";a:1:{s:5:\"field\";s:7:\"records\";}s:6:\"tables\";s:10:\"tt_content\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:43:\"tt_content: header [header_layout], records\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:93:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.shortcut\";}}}}s:5:\"table\";s:20:\"< lib.contentElement\";s:6:\"table.\";a:3:{s:12:\"templateName\";s:5:\"Table\";s:15:\"dataProcessing.\";a:2:{i:10;s:62:\"TYPO3\\CMS\\Frontend\\DataProcessing\\CommaSeparatedValueProcessor\";s:3:\"10.\";a:5:{s:9:\"fieldName\";s:8:\"bodytext\";s:15:\"fieldDelimiter.\";a:1:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_delimiter\";}}}s:15:\"fieldEnclosure.\";a:1:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_enclosure\";}}}s:15:\"maximumColumns.\";a:1:{s:5:\"field\";s:4:\"cols\";}s:2:\"as\";s:5:\"table\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:100:\"tt_content: header [header_layout], bodytext, [table_caption|cols|table_header_position|table_tfoot]\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:90:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.table\";}}}}s:4:\"text\";s:20:\"< lib.contentElement\";s:5:\"text.\";a:2:{s:12:\"templateName\";s:4:\"Text\";s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:20:\"tt_content: bodytext\";s:10:\"editIcons.\";a:2:{s:13:\"beforeLastTag\";s:1:\"1\";s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.html\";}}}}s:9:\"textmedia\";s:20:\"< lib.contentElement\";s:10:\"textmedia.\";a:3:{s:12:\"templateName\";s:9:\"Textmedia\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:6:\"assets\";}}i:20;s:50:\"TYPO3\\CMS\\Frontend\\DataProcessing\\GalleryProcessor\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:126:\"tt_content: header [header_layout], bodytext, assets [imageorient|imagewidth|imageheight], [imagecols|imageborder], image_zoom\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:94:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.textmedia\";}}}}s:7:\"textpic\";s:20:\"< lib.contentElement\";s:8:\"textpic.\";a:3:{s:12:\"templateName\";s:7:\"Textpic\";s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}i:20;s:50:\"TYPO3\\CMS\\Frontend\\DataProcessing\\GalleryProcessor\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:125:\"tt_content: header [header_layout], bodytext, image [imageorient|imagewidth|imageheight], [imagecols|imageborder], image_zoom\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:92:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.textpic\";}}}}s:7:\"uploads\";s:20:\"< lib.contentElement\";s:8:\"uploads.\";a:3:{s:12:\"templateName\";s:7:\"Uploads\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:3:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}s:12:\"collections.\";a:1:{s:5:\"field\";s:16:\"file_collections\";}s:8:\"sorting.\";a:2:{s:5:\"field\";s:16:\"filelink_sorting\";s:10:\"direction.\";a:1:{s:5:\"field\";s:26:\"filelink_sorting_direction\";}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:127:\"tt_content: header [header_layout], media, file_collections, filelink_sorting, [filelink_size|uploads_description|uploads_type]\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:92:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.uploads\";}}}}s:13:\"menu_abstract\";s:20:\"< lib.contentElement\";s:14:\"menu_abstract.\";a:3:{s:12:\"templateName\";s:12:\"MenuAbstract\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:24:\"menu_categorized_content\";s:20:\"< lib.contentElement\";s:25:\"menu_categorized_content.\";a:3:{s:12:\"templateName\";s:22:\"MenuCategorizedContent\";s:15:\"dataProcessing.\";a:2:{i:10;s:56:\"TYPO3\\CMS\\Frontend\\DataProcessing\\DatabaseQueryProcessor\";s:3:\"10.\";a:10:{s:5:\"table\";s:10:\"tt_content\";s:12:\"selectFields\";s:12:\"tt_content.*\";s:7:\"groupBy\";s:3:\"uid\";s:10:\"pidInList.\";a:1:{s:4:\"data\";s:12:\"leveluid : 0\";}s:9:\"recursive\";s:2:\"99\";s:5:\"join.\";a:2:{s:4:\"data\";s:25:\"field:selected_categories\";s:4:\"wrap\";s:109:\"sys_category_record_mm ON uid = sys_category_record_mm.uid_foreign AND sys_category_record_mm.uid_local IN(|)\";}s:6:\"where.\";a:2:{s:4:\"data\";s:20:\"field:category_field\";s:4:\"wrap\";s:41:\"tablenames=\'tt_content\' and fieldname=\'|\'\";}s:7:\"orderBy\";s:18:\"tt_content.sorting\";s:2:\"as\";s:7:\"content\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:71:\"tt_content: header [header_layout], selected_categories, category_field\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:22:\"menu_categorized_pages\";s:20:\"< lib.contentElement\";s:23:\"menu_categorized_pages.\";a:3:{s:12:\"templateName\";s:20:\"MenuCategorizedPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:10:\"categories\";s:8:\"special.\";a:4:{s:6:\"value.\";a:1:{s:5:\"field\";s:19:\"selected_categories\";}s:9:\"relation.\";a:1:{s:5:\"field\";s:14:\"category_field\";}s:7:\"sorting\";s:5:\"title\";s:5:\"order\";s:3:\"asc\";}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:71:\"tt_content: header [header_layout], selected_categories, category_field\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:10:\"menu_pages\";s:20:\"< lib.contentElement\";s:11:\"menu_pages.\";a:3:{s:12:\"templateName\";s:9:\"MenuPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:4:\"list\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:13:\"menu_subpages\";s:20:\"< lib.contentElement\";s:14:\"menu_subpages.\";a:3:{s:12:\"templateName\";s:12:\"MenuSubpages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:12:\"menu_section\";s:20:\"< lib.contentElement\";s:13:\"menu_section.\";a:3:{s:12:\"templateName\";s:11:\"MenuSection\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:4:\"list\";s:8:\"special.\";a:1:{s:6:\"value.\";a:2:{s:5:\"field\";s:5:\"pages\";s:9:\"override.\";a:2:{s:4:\"data\";s:8:\"page:uid\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:5:\"field\";s:5:\"pages\";}}}}}s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}i:20;s:56:\"TYPO3\\CMS\\Frontend\\DataProcessing\\DatabaseQueryProcessor\";s:3:\"20.\";a:6:{s:5:\"table\";s:10:\"tt_content\";s:10:\"pidInList.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:2:\"as\";s:7:\"content\";s:5:\"where\";s:16:\"sectionIndex = 1\";s:7:\"orderBy\";s:7:\"sorting\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:18:\"menu_section_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_section_pages.\";a:3:{s:12:\"templateName\";s:16:\"MenuSectionPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:4:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}i:20;s:56:\"TYPO3\\CMS\\Frontend\\DataProcessing\\DatabaseQueryProcessor\";s:3:\"20.\";a:5:{s:5:\"table\";s:10:\"tt_content\";s:10:\"pidInList.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:7:\"orderBy\";s:7:\"sorting\";s:2:\"as\";s:7:\"content\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:21:\"menu_recently_updated\";s:20:\"< lib.contentElement\";s:22:\"menu_recently_updated.\";a:3:{s:12:\"templateName\";s:19:\"MenuRecentlyUpdated\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:3:{s:7:\"special\";s:7:\"updated\";s:8:\"special.\";a:3:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}s:6:\"maxAge\";s:9:\"3600*24*7\";s:20:\"excludeNoSearchPages\";s:1:\"1\";}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:18:\"menu_related_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_related_pages.\";a:3:{s:12:\"templateName\";s:16:\"MenuRelatedPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:4:{s:7:\"special\";s:8:\"keywords\";s:8:\"special.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}s:20:\"excludeNoSearchPages\";s:1:\"1\";}s:23:\"alternativeSortingField\";s:5:\"title\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:12:\"menu_sitemap\";s:20:\"< lib.contentElement\";s:13:\"menu_sitemap.\";a:3:{s:12:\"templateName\";s:11:\"MenuSitemap\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:2:{s:6:\"levels\";s:1:\"7\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:18:\"menu_sitemap_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_sitemap_pages.\";a:3:{s:12:\"templateName\";s:16:\"MenuSitemapPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:4:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:6:\"levels\";s:1:\"7\";s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}s:8:\"stdWrap.\";a:2:{s:9:\"editIcons\";s:41:\"tt_content: header [header_layout], pages\";s:10:\"editIcons.\";a:1:{s:10:\"iconTitle.\";a:1:{s:4:\"data\";s:89:\"LLL:EXT:fluid_styled_content/Resources/Private/Language/FrontendEditing.xlf:editIcon.menu\";}}}}s:18:\"form_formframework\";s:20:\"< lib.contentElement\";s:19:\"form_formframework.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:4:\"USER\";s:3:\"20.\";a:4:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:4:\"Form\";s:10:\"pluginName\";s:13:\"Formframework\";s:10:\"vendorName\";s:9:\"TYPO3\\CMS\";}}}s:7:\"module.\";a:5:{s:8:\"tx_form.\";a:2:{s:9:\"settings.\";a:1:{s:19:\"yamlConfigurations.\";a:3:{i:10;s:42:\"EXT:form/Configuration/Yaml/BaseSetup.yaml\";i:20;s:48:\"EXT:form/Configuration/Yaml/FormEditorSetup.yaml\";i:30;s:48:\"EXT:form/Configuration/Yaml/FormEngineSetup.yaml\";}}s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:1:{i:10;s:45:\"EXT:form/Resources/Private/Backend/Templates/\";}s:17:\"partialRootPaths.\";a:1:{i:10;s:44:\"EXT:form/Resources/Private/Backend/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:10;s:43:\"EXT:form/Resources/Private/Backend/Layouts/\";}}}s:9:\"tx_belog.\";a:2:{s:12:\"persistence.\";a:1:{s:8:\"classes.\";a:2:{s:38:\"TYPO3\\CMS\\Belog\\Domain\\Model\\LogEntry.\";a:1:{s:8:\"mapping.\";a:2:{s:9:\"tableName\";s:7:\"sys_log\";s:8:\"columns.\";a:8:{s:7:\"userid.\";a:1:{s:13:\"mapOnProperty\";s:14:\"backendUserUid\";}s:7:\"recuid.\";a:1:{s:13:\"mapOnProperty\";s:9:\"recordUid\";}s:10:\"tablename.\";a:1:{s:13:\"mapOnProperty\";s:9:\"tableName\";}s:7:\"recpid.\";a:1:{s:13:\"mapOnProperty\";s:9:\"recordPid\";}s:11:\"details_nr.\";a:1:{s:13:\"mapOnProperty\";s:13:\"detailsNumber\";}s:3:\"IP.\";a:1:{s:13:\"mapOnProperty\";s:2:\"ip\";}s:10:\"workspace.\";a:1:{s:13:\"mapOnProperty\";s:12:\"workspaceUid\";}s:6:\"NEWid.\";a:1:{s:13:\"mapOnProperty\";s:5:\"newId\";}}}}s:39:\"TYPO3\\CMS\\Belog\\Domain\\Model\\Workspace.\";a:1:{s:8:\"mapping.\";a:1:{s:9:\"tableName\";s:13:\"sys_workspace\";}}}}s:9:\"settings.\";a:3:{s:29:\"selectableNumberOfLogEntries.\";a:7:{i:20;s:2:\"20\";i:50;s:2:\"50\";i:100;s:3:\"100\";i:200;s:3:\"200\";i:500;s:3:\"500\";i:1000;s:4:\"1000\";i:1000000;s:3:\"any\";}s:21:\"selectableTimeFrames.\";a:8:{i:0;s:8:\"thisWeek\";i:1;s:8:\"lastWeek\";i:2;s:9:\"last7Days\";i:10;s:9:\"thisMonth\";i:11;s:9:\"lastMonth\";i:12;s:10:\"last31Days\";i:20;s:7:\"noLimit\";i:30;s:11:\"userDefined\";}s:18:\"selectableActions.\";a:7:{i:0;s:3:\"any\";i:1;s:14:\"actionDatabase\";i:2;s:10:\"actionFile\";i:3;s:11:\"actionCache\";i:254;s:14:\"actionSettings\";i:255;s:11:\"actionLogin\";i:-1;s:12:\"actionErrors\";}}}s:10:\"tx_beuser.\";a:2:{s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:1:\"0\";}s:9:\"settings.\";a:1:{s:5:\"dummy\";s:3:\"foo\";}}s:20:\"tx_extensionmanager.\";a:1:{s:9:\"features.\";a:1:{s:20:\"skipDefaultArguments\";s:1:\"0\";}}s:18:\"extension_builder.\";a:1:{s:9:\"settings.\";a:2:{s:22:\"codeTemplateRootPaths.\";a:1:{i:0;s:62:\"EXT:extension_builder/Resources/Private/CodeTemplates/Extbase/\";}s:25:\"codeTemplatePartialPaths.\";a:1:{i:0;s:70:\"EXT:extension_builder/Resources/Private/CodeTemplates/Extbase/Partials\";}}}}s:4:\"lib.\";a:14:{s:14:\"contentElement\";s:13:\"FLUIDTEMPLATE\";s:15:\"contentElement.\";a:5:{s:12:\"templateName\";s:7:\"Default\";s:18:\"templateRootPaths.\";a:2:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:0:\"\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:0:\"\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:2:{s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:1:{s:9:\"no-cookie\";s:1:\"1\";}}}}s:10:\"parseFunc.\";a:8:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:4:{s:4:\"link\";s:4:\"TEXT\";s:5:\"link.\";a:3:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:2:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"parameters : allParams\";}s:9:\"extTarget\";s:6:\"_blank\";}s:10:\"parseFunc.\";a:1:{s:9:\"constants\";s:1:\"1\";}}s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:6:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}s:9:\"extTarget\";s:6:\"_blank\";s:10:\"extTarget.\";a:1:{s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:392:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:5:\"sword\";s:31:\"<span class=\"ce-sword\">|</span>\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}}}s:14:\"parseFunc_RTE.\";a:10:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:4:{s:4:\"link\";s:4:\"TEXT\";s:5:\"link.\";a:3:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:2:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"parameters : allParams\";}s:9:\"extTarget\";s:6:\"_blank\";}s:10:\"parseFunc.\";a:1:{s:9:\"constants\";s:1:\"1\";}}s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:6:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}s:9:\"extTarget\";s:6:\"_blank\";s:10:\"extTarget.\";a:1:{s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:392:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:5:\"sword\";s:31:\"<span class=\"ce-sword\">|</span>\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:3:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}s:12:\"encapsLines.\";a:4:{s:13:\"encapsTagList\";s:29:\"p,pre,h1,h2,h3,h4,h5,h6,hr,dt\";s:9:\"remapTag.\";a:1:{s:3:\"DIV\";s:1:\"P\";}s:13:\"nonWrappedTag\";s:1:\"P\";s:17:\"innerStdWrap_all.\";a:1:{s:7:\"ifBlank\";s:6:\"&nbsp;\";}}}s:14:\"externalBlocks\";s:89:\"article, aside, blockquote, div, dd, dl, footer, header, nav, ol, section, table, ul, pre\";s:15:\"externalBlocks.\";a:14:{s:3:\"ol.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:3:\"ul.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:4:\"pre.\";a:1:{s:8:\"stdWrap.\";a:1:{s:10:\"parseFunc.\";a:8:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:4:{s:4:\"link\";s:4:\"TEXT\";s:5:\"link.\";a:3:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:2:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:22:\"parameters : allParams\";}s:9:\"extTarget\";s:6:\"_blank\";}s:10:\"parseFunc.\";a:1:{s:9:\"constants\";s:1:\"1\";}}s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:6:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}s:9:\"extTarget\";s:6:\"_blank\";s:10:\"extTarget.\";a:1:{s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:392:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:5:\"sword\";s:31:\"<span class=\"ce-sword\">|</span>\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}}}}}s:6:\"table.\";a:4:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:5:\"tags.\";a:1:{s:6:\"table.\";a:1:{s:10:\"fixAttrib.\";a:1:{s:6:\"class.\";a:3:{s:7:\"default\";s:12:\"contenttable\";s:6:\"always\";s:1:\"1\";s:4:\"list\";s:12:\"contenttable\";}}}}s:18:\"keepNonMatchedTags\";s:1:\"1\";}}s:14:\"HTMLtableCells\";s:1:\"1\";s:15:\"HTMLtableCells.\";a:2:{s:8:\"default.\";a:1:{s:8:\"stdWrap.\";a:2:{s:9:\"parseFunc\";s:19:\"< lib.parseFunc_RTE\";s:10:\"parseFunc.\";a:1:{s:18:\"nonTypoTagStdWrap.\";a:1:{s:12:\"encapsLines.\";a:1:{s:13:\"nonWrappedTag\";s:0:\"\";}}}}}s:25:\"addChr10BetweenParagraphs\";s:1:\"1\";}}s:4:\"div.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"article.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:6:\"aside.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:11:\"blockquote.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"footer.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"header.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:4:\"nav.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"section.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dl.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dd.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}}}s:18:\"hivecfgtyposcript.\";a:1:{s:9:\"userfunc.\";a:2:{s:14:\"getbrowserinfo\";s:8:\"USER_INT\";s:15:\"getbrowserinfo.\";a:1:{s:8:\"userFunc\";s:62:\"HIVE\\HiveCfgTyposcript\\UserFunc\\GetBrowserInfo->getBrowserInfo\";}}}s:10:\"stdheader.\";a:2:{s:3:\"10.\";a:7:{s:8:\"default.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h1{register:headerClass}><span>|</span></h1>\";}s:2:\"1.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h1{register:headerClass}><span>|</span></h1>\";}s:2:\"2.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h2{register:headerClass}><span>|</span></h2>\";}s:2:\"3.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h3{register:headerClass}><span>|</span></h3>\";}s:2:\"4.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h4{register:headerClass}><span>|</span></h4>\";}s:2:\"5.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h5{register:headerClass}><span>|</span></h5>\";}s:2:\"6.\";a:2:{s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:6:\"header\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h6{register:headerClass}><span>|</span></h6>\";}}s:3:\"20.\";a:6:{s:8:\"default.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h2{register:headerClass}><span>|</span></h2>\";s:4:\"wrap\";s:1:\"|\";}s:2:\"1.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h2{register:headerClass}><span>|</span></h2>\";s:4:\"wrap\";s:1:\"|\";}s:2:\"2.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h3{register:headerClass}><span>|</span></h3>\";s:4:\"wrap\";s:1:\"|\";}s:2:\"3.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h4{register:headerClass}><span>|</span></h4>\";s:4:\"wrap\";s:1:\"|\";}s:2:\"4.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h5{register:headerClass}><span>|</span></h5>\";s:4:\"wrap\";s:1:\"|\";}s:2:\"5.\";a:5:{s:7:\"current\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"0\";s:11:\"setCurrent.\";a:2:{s:5:\"field\";s:9:\"subheader\";s:12:\"replacement.\";a:1:{s:3:\"20.\";a:3:{s:6:\"search\";s:14:\"/(<br\\ ?\\/?>)/\";s:9:\"useRegExp\";s:1:\"1\";s:7:\"replace\";s:19:\"</span><br /><span>\";}}}s:8:\"dataWrap\";s:45:\"<h6{register:headerClass}><span>|</span></h6>\";s:4:\"wrap\";s:1:\"|\";}}}s:11:\"navigation.\";a:2:{s:6:\"anchor\";s:3:\"COA\";s:7:\"anchor.\";a:5:{i:5;s:13:\"LOAD_REGISTER\";s:2:\"5.\";a:3:{s:7:\"colPos.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:6:\"colPos\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:1:\"0\";}}}}s:8:\"pageUid.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:7:\"pageUid\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:7:\"TSFE:id\";}}}s:11:\"onePageUid.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:10:\"onePageUid\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:7:\"TSFE:id\";}}}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:5:{s:5:\"table\";s:5:\"pages\";s:7:\"select.\";a:4:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:16:\"register:pageUid\";}s:5:\"where\";s:49:\"(NOT hidden) AND (NOT deleted) AND (NOT nav_hide)\";s:7:\"orderBy\";s:7:\"sorting\";}s:4:\"wrap\";s:10:\"<ul>|</ul>\";s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:2:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:2:{s:5:\"field\";s:18:\"nav_title // title\";s:9:\"typolink.\";a:3:{s:4:\"wrap\";s:10:\"<li>|</li>\";s:10:\"parameter.\";a:1:{s:4:\"data\";s:19:\"register:onePageUid\";}s:8:\"section.\";a:1:{s:8:\"dataWrap\";s:16:\"page-{field:uid}\";}}}}}i:30;s:16:\"RESTORE_REGISTER\";}}s:5:\"site.\";a:2:{s:6:\"anchor\";s:3:\"COA\";s:7:\"anchor.\";a:5:{i:5;s:13:\"LOAD_REGISTER\";s:2:\"5.\";a:73:{s:18:\"sectionClasses100.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"sectionClasses100\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:23:\"containerDivClasses100.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"containerDivClasses100\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:17:\"rowDivClasses100.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:16:\"rowDivClasses100\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:17:\"colDivClasses100.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:16:\"colDivClasses100\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"col\";}}}}s:17:\"offsetClasses100.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:16:\"offsetClasses100\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses5050.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses5050\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses5050.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses5050\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses5050.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses5050\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses5050a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses5050a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-6\";}}}}s:19:\"colDivClasses5050b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses5050b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-6\";}}}}s:18:\"offsetClasses5050.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses5050\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses3366.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses3366\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses3366.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses3366\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses3366.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses3366\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses3366a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses3366a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-4\";}}}}s:19:\"colDivClasses3366b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses3366b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-8\";}}}}s:18:\"offsetClasses3366.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses3366\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses6633.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses6633\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses6633.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses6633\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses6633.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses6633\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses6633a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses6633a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-8\";}}}}s:19:\"colDivClasses6633b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses6633b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-4\";}}}}s:18:\"offsetClasses6633.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses6633\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:21:\"sectionClasses333333.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:20:\"sectionClasses333333\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:26:\"containerDivClasses333333.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:25:\"containerDivClasses333333\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:20:\"rowDivClasses333333.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:19:\"rowDivClasses333333\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:21:\"colDivClasses333333a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:20:\"colDivClasses333333a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-4\";}}}}s:21:\"colDivClasses333333b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:20:\"colDivClasses333333b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-4\";}}}}s:21:\"colDivClasses333333c.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:20:\"colDivClasses333333c\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-4\";}}}}s:20:\"offsetClasses333333.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:19:\"offsetClasses333333\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:23:\"sectionClasses25252525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"sectionClasses25252525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:28:\"containerDivClasses25252525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:27:\"containerDivClasses25252525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:22:\"rowDivClasses25252525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:21:\"rowDivClasses25252525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:23:\"colDivClasses25252525a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"colDivClasses25252525a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:23:\"colDivClasses25252525b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"colDivClasses25252525b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:23:\"colDivClasses25252525c.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"colDivClasses25252525c\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:23:\"colDivClasses25252525d.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:22:\"colDivClasses25252525d\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:22:\"offsetClasses25252525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:21:\"offsetClasses25252525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses2575.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses2575\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses2575.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses2575\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses2575.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses2575\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses2575a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses2575a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:19:\"colDivClasses2575b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses2575b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-9\";}}}}s:18:\"offsetClasses2575.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses2575\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses7525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses2575\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses7525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses7525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses7525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses7525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses7525a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses7525a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-9\";}}}}s:19:\"colDivClasses7525b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses7525b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-3\";}}}}s:18:\"offsetClasses7525.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses7525\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses4060.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses4060\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses4060.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses4060\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses4060.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses4060\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses4060a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses4060a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-5\";}}}}s:19:\"colDivClasses4060b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses4060b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-7\";}}}}s:18:\"offsetClasses4060.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses4060\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:19:\"sectionClasses6040.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"sectionClasses6040\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:24:\"containerDivClasses6040.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"containerDivClasses6040\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:18:\"rowDivClasses6040.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"rowDivClasses6040\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:19:\"colDivClasses6040a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses6040a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-7\";}}}}s:19:\"colDivClasses6040b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:18:\"colDivClasses6040b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:8:\"col-md-5\";}}}}s:18:\"offsetClasses6040.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:17:\"offsetClasses6040\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:25:\"sectionClasses2020202020.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"sectionClasses2020202020\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:7:\"section\";}}}}s:30:\"containerDivClasses2020202020.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:29:\"containerDivClasses2020202020\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"container\";}}}}s:24:\"rowDivClasses2020202020.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"rowDivClasses2020202020\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:3:\"row\";}}}}s:25:\"colDivClasses2020202020a.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"colDivClasses2020202020a\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"col-md-20\";}}}}s:25:\"colDivClasses2020202020b.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"colDivClasses2020202020b\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"col-md-20\";}}}}s:25:\"colDivClasses2020202020c.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"colDivClasses2020202020c\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"col-md-20\";}}}}s:25:\"colDivClasses2020202020d.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"colDivClasses2020202020d\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"col-md-20\";}}}}s:25:\"colDivClasses2020202020e.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:24:\"colDivClasses2020202020e\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:9:\"col-md-20\";}}}}s:24:\"offsetClasses2020202020.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:23:\"offsetClasses2020202020\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:21:\"offset-md-1 col-md-10\";}}}}s:9:\"hClasses.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:8:\"hClasses\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:17:\"text-align_center\";}}}}s:8:\"pageUid.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:7:\"pageUid\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:7:\"TSFE:id\";}}}}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:4:{s:5:\"table\";s:5:\"pages\";s:7:\"select.\";a:3:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:16:\"register:pageUid\";}s:7:\"orderBy\";s:7:\"sorting\";}s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:4:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:1:{s:13:\"meinRegister.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:3:\"uid\";}}}i:15;s:3:\"COA\";s:3:\"15.\";a:7:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:332:\"<a id=\"page-{field:uid}\" name=\"page-{field:uid}\"></a><section id=\"section__uid--{field:uid}\" data-section=\"{field:uid}\" class=\"{register:sectionClasses100} section__bl--{field:backend_layout} section__doktype--{field:doktype} {field:tx_hivecptanchornav_bs4_class_section}\" data-sys_language_uid=\"{TSFE:sys_language_uid}\">|</section>\";}i:800;s:5:\"FILES\";s:4:\"800.\";a:3:{s:11:\"references.\";a:3:{s:5:\"table\";s:5:\"pages\";s:4:\"uid.\";a:1:{s:4:\"data\";s:11:\"field = uid\";}s:9:\"fieldName\";s:5:\"media\";}s:9:\"renderObj\";s:4:\"TEXT\";s:10:\"renderObj.\";a:3:{s:8:\"stdWrap.\";a:2:{s:4:\"wrap\";s:197:\"<div class=\"section__background\"><div class=\"section__background--inner b-lazy opacity_0\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-echo=\"|\"></div></div>\";s:8:\"required\";s:1:\"1\";}s:4:\"data\";s:22:\"file:current:publicUrl\";s:3:\"if.\";a:2:{s:5:\"value\";s:6:\"bg.png\";s:7:\"equals.\";a:1:{s:4:\"data\";s:17:\"file:current:name\";}}}}i:900;s:5:\"FILES\";s:4:\"900.\";a:3:{s:11:\"references.\";a:3:{s:5:\"table\";s:5:\"pages\";s:4:\"uid.\";a:1:{s:4:\"data\";s:11:\"field = uid\";}s:9:\"fieldName\";s:5:\"media\";}s:9:\"renderObj\";s:4:\"TEXT\";s:10:\"renderObj.\";a:3:{s:8:\"stdWrap.\";a:2:{s:4:\"wrap\";s:197:\"<div class=\"section__background\"><div class=\"section__background--inner b-lazy opacity_0\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-echo=\"|\"></div></div>\";s:8:\"required\";s:1:\"1\";}s:4:\"data\";s:22:\"file:current:publicUrl\";s:3:\"if.\";a:2:{s:5:\"value\";s:6:\"bg.jpg\";s:7:\"equals.\";a:1:{s:4:\"data\";s:17:\"file:current:name\";}}}}i:1000;s:7:\"CONTENT\";s:5:\"1000.\";a:4:{s:5:\"table\";s:5:\"pages\";s:7:\"select.\";a:3:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:21:\"register:meinRegister\";}s:7:\"orderBy\";s:7:\"sorting\";}s:9:\"renderObj\";s:3:\"COA\";s:10:\"renderObj.\";a:46:{i:10;s:13:\"LOAD_REGISTER\";s:3:\"10.\";a:1:{s:12:\"pidRegister.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:3:\"uid\";}}}i:20;s:3:\"COA\";s:3:\"20.\";a:4:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:285:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\"><div class=\"{register:rowDivClasses100} layout-{field:layout}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:154:\"<div class=\"{register:colDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_col0}\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:24:\"pagets__SectionAnchor100\";}}i:25;s:3:\"COA\";s:3:\"25.\";a:4:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:423:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses100} layout-{field:layout}\"><div class=\"{register:rowDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\"><div class=\"{register:offsetClasses100} layout-{field:layout}\"><div class=\"{register:rowDivClasses100} layout-{field:layout}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:154:\"<div class=\"{register:colDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_col0}\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:34:\"pagets__SectionAnchor100WithOffset\";}}i:30;s:3:\"COA\";s:3:\"30.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses5050} layout-{field:layout}\"><div class=\"{register:rowDivClasses5050} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:141:\"<div class=\"{register:colDivClasses5050a} {field:tx_hivecptanchornav_bs4_align_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:141:\"<div class=\"{register:colDivClasses5050b} {field:tx_hivecptanchornav_bs4_align_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor5050\";}}i:35;s:3:\"COA\";s:3:\"35.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses5050} layout-{field:layout}\"><div class=\"{register:rowDivClasses5050} layout-{field:layout}\"><div class=\"{register:offsetClasses5050}\"><div class=\"{register:rowDivClasses5050} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:141:\"<div class=\"{register:colDivClasses5050a} {field:tx_hivecptanchornav_bs4_align_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:141:\"<div class=\"{register:colDivClasses5050b} {field:tx_hivecptanchornav_bs4_align_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor5050WithOffset\";}}i:40;s:3:\"COA\";s:3:\"40.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses3366} layout-{field:layout}\"><div class=\"{register:rowDivClasses3366} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses3366a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses3366b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor3366\";}}i:45;s:3:\"COA\";s:3:\"45.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses3366} layout-{field:layout}\"><div class=\"{register:rowDivClasses3366} layout-{field:layout}\"><div class=\"{register:offsetClasses3366}\"><div class=\"{register:rowDivClasses3366} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses3366a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses3366b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor3366WithOffset\";}}i:50;s:3:\"COA\";s:3:\"50.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses6633} layout-{field:layout}\"><div class=\"{register:rowDivClasses6633} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6633a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6633b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor6633\";}}i:55;s:3:\"COA\";s:3:\"55.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses6633} layout-{field:layout}\"><div class=\"{register:rowDivClasses6633} layout-{field:layout}\"><div class=\"{register:offsetClasses6633}\"><div class=\"{register:rowDivClasses6633} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6633a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6633b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor6633WithOffset\";}}i:60;s:3:\"COA\";s:3:\"60.\";a:8:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:291:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses333333} layout-{field:layout}\"><div class=\"{register:rowDivClasses333333} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:27:\"pagets__SectionAnchor333333\";}}i:65;s:3:\"COA\";s:3:\"65.\";a:8:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:391:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses333333} layout-{field:layout}\"><div class=\"{register:rowDivClasses333333} layout-{field:layout}\"><div class=\"{register:offsetClasses333333}\"><div class=\"{register:rowDivClasses333333} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:190:\"<div class=\"{register:colDivClasses333333c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:37:\"pagets__SectionAnchor333333WithOffset\";}}i:70;s:3:\"COA\";s:3:\"70.\";a:10:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:295:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses25252525} layout-{field:layout}\"><div class=\"{register:rowDivClasses25252525} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:40;s:7:\"CONTENT\";s:3:\"40.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525d} {field:tx_hivecptanchornav_bs4_align_col3} {field:tx_hivecptanchornav_bs4_push_pull_col3} col--4\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=3\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:29:\"pagets__SectionAnchor25252525\";}}i:75;s:3:\"COA\";s:3:\"75.\";a:10:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:399:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses25252525} layout-{field:layout}\"><div class=\"{register:rowDivClasses25252525} layout-{field:layout}\"><div class=\"{register:offsetClasses25252525}\"><div class=\"{register:rowDivClasses25252525} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:40;s:7:\"CONTENT\";s:3:\"40.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:192:\"<div class=\"{register:colDivClasses25252525d} {field:tx_hivecptanchornav_bs4_align_col3} {field:tx_hivecptanchornav_bs4_push_pull_col3} col--4\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=3\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:39:\"pagets__SectionAnchor25252525WithOffset\";}}i:80;s:3:\"COA\";s:3:\"80.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses2575} layout-{field:layout}\"><div class=\"{register:rowDivClasses2575} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses2575a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses2575b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor2575\";}}i:85;s:3:\"COA\";s:3:\"85.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses2575} layout-{field:layout}\"><div class=\"{register:rowDivClasses2575} layout-{field:layout}\"><div class=\"{register:offsetClasses2575}\"><div class=\"{register:rowDivClasses2575} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses2575a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses2575b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor2575WithOffset\";}}i:90;s:3:\"COA\";s:3:\"90.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses7525} layout-{field:layout}\"><div class=\"{register:rowDivClasses7525} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses7525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses7525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor7525\";}}i:95;s:3:\"COA\";s:3:\"95.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses7525} layout-{field:layout}\"><div class=\"{register:rowDivClasses7525} layout-{field:layout}\"><div class=\"{register:offsetClasses7525}\"><div class=\"{register:rowDivClasses7525} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses7525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses7525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor7525WithOffset\";}}i:100;s:3:\"COA\";s:4:\"100.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses4060} layout-{field:layout}\"><div class=\"{register:rowDivClasses4060} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses4060a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses4060b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor4060\";}}i:105;s:3:\"COA\";s:4:\"105.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses4060} layout-{field:layout}\"><div class=\"{register:rowDivClasses4060} layout-{field:layout}\"><div class=\"{register:offsetClasses4060}\"><div class=\"{register:rowDivClasses4060} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses4060a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses4060b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor4060WithOffset\";}}i:110;s:3:\"COA\";s:4:\"110.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:287:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses6040} layout-{field:layout}\"><div class=\"{register:rowDivClasses6040} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6040a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6040b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:25:\"pagets__SectionAnchor6040\";}}i:115;s:3:\"COA\";s:4:\"115.\";a:6:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:383:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses6040} layout-{field:layout}\"><div class=\"{register:rowDivClasses6040} layout-{field:layout}\"><div class=\"{register:offsetClasses6040}\"><div class=\"{register:rowDivClasses6040} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6040a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:188:\"<div class=\"{register:colDivClasses6040b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:35:\"pagets__SectionAnchor6040WithOffset\";}}i:120;s:3:\"COA\";s:4:\"120.\";a:12:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:299:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses2020202020} layout-{field:layout}\"><div class=\"{register:rowDivClasses2020202020} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020a} {field:tx_hivecptanchornav_bs4_align_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020b} {field:tx_hivecptanchornav_bs4_align_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020c} {field:tx_hivecptanchornav_bs4_align_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:40;s:7:\"CONTENT\";s:3:\"40.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020d} {field:tx_hivecptanchornav_bs4_align_col3} col--4\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=3\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:50;s:7:\"CONTENT\";s:3:\"50.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020e} {field:tx_hivecptanchornav_bs4_align_col4} col--5\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=4\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:31:\"pagets__SectionAnchor2020202020\";}}i:125;s:3:\"COA\";s:4:\"125.\";a:12:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:407:\"<a id=\"row-{field:uid}\" name=\"row-{field:uid}\"></a><div class=\"{register:containerDivClasses2020202020} layout-{field:layout}\"><div class=\"{register:rowDivClasses2020202020} layout-{field:layout}\"><div class=\"{register:offsetClasses2020202020}\"><div class=\"{register:rowDivClasses2020202020} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}\">|</div></div></div></div>\";}i:10;s:7:\"CONTENT\";s:3:\"10.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020a} {field:tx_hivecptanchornav_bs4_align_col0} col--1\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=0\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020b} {field:tx_hivecptanchornav_bs4_align_col1} col--2\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=1\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:30;s:7:\"CONTENT\";s:3:\"30.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020c} {field:tx_hivecptanchornav_bs4_align_col2} col--3\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=2\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:40;s:7:\"CONTENT\";s:3:\"40.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020d} {field:tx_hivecptanchornav_bs4_align_col3} col--4\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=3\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}i:50;s:7:\"CONTENT\";s:3:\"50.\";a:3:{s:8:\"stdWrap.\";a:1:{s:8:\"dataWrap\";s:147:\"<div class=\"{register:colDivClasses2020202020e} {field:tx_hivecptanchornav_bs4_align_col4} col--5\"><div class=\"layout-{field:layout}\">|</div></div>\";}s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:5:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:20:\"register:pidRegister\";}s:5:\"where\";s:8:\"colPos=4\";s:9:\"andWhere.\";a:1:{s:8:\"dataWrap\";s:122:\"(NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})\";}s:7:\"orderBy\";s:7:\"sorting\";}}s:3:\"if.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"equals\";s:41:\"pagets__SectionAnchor2020202020WithOffset\";}}}}}}}i:30;s:16:\"RESTORE_REGISTER\";}}s:12:\"firstSubPage\";s:7:\"CONTENT\";s:13:\"firstSubPage.\";a:4:{s:5:\"table\";s:5:\"pages\";s:7:\"select.\";a:4:{s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:21:\"TSFE:content_from_pid\";}s:7:\"orderBy\";s:11:\"sorting ASC\";s:3:\"max\";s:1:\"1\";}s:9:\"renderObj\";s:4:\"TEXT\";s:10:\"renderObj.\";a:1:{s:4:\"data\";s:9:\"field:uid\";}}s:14:\"dynamicContent\";s:3:\"COA\";s:15:\"dynamicContent.\";a:5:{i:5;s:13:\"LOAD_REGISTER\";s:2:\"5.\";a:2:{s:7:\"colPos.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:6:\"colPos\";s:8:\"ifEmpty.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:6:\"value.\";a:1:{s:7:\"current\";s:1:\"1\";}s:7:\"ifEmpty\";s:1:\"0\";}}}}s:8:\"pageUid.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:7:\"pageUid\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:7:\"TSFE:id\";}}}}i:20;s:7:\"CONTENT\";s:3:\"20.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:4:{s:7:\"orderBy\";s:7:\"sorting\";s:5:\"where\";s:24:\"colPos={register:colPos}\";s:6:\"where.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:10:\"pidInList.\";a:1:{s:4:\"data\";s:16:\"register:pageUid\";}}}i:90;s:16:\"RESTORE_REGISTER\";}s:19:\"dynamicContentSlide\";s:20:\"< lib.dynamicContent\";s:20:\"dynamicContentSlide.\";a:1:{s:3:\"20.\";a:1:{s:5:\"slide\";s:2:\"-1\";}}}s:7:\"plugin.\";a:8:{s:12:\"tx_frontend.\";a:1:{s:18:\"_CSS_DEFAULT_STYLE\";s:3311:\"    .ce-align-left { text-align: left; }\n    .ce-align-center { text-align: center; }\n    .ce-align-right { text-align: right; }\n\n    .ce-table td, .ce-table th { vertical-align: top; }\n\n    .ce-textpic, .ce-image, .ce-nowrap .ce-bodytext, .ce-gallery, .ce-row, .ce-uploads li, .ce-uploads div { overflow: hidden; }\n\n    .ce-left .ce-gallery, .ce-column { float: left; }\n    .ce-center .ce-outer { position: relative; float: right; right: 50%; }\n    .ce-center .ce-inner { position: relative; float: right; right: -50%; }\n    .ce-right .ce-gallery { float: right; }\n\n    .ce-gallery figure { display: table; margin: 0; }\n    .ce-gallery figcaption { display: table-caption; caption-side: bottom; }\n    .ce-gallery img { display: block; }\n    .ce-gallery iframe { border-width: 0; }\n    .ce-border img,\n    .ce-border iframe {\n        border: 2px solid #000000;\n        padding: 0px;\n    }\n\n    .ce-intext.ce-right .ce-gallery, .ce-intext.ce-left .ce-gallery, .ce-above .ce-gallery {\n        margin-bottom: 10px;\n    }\n    .ce-intext.ce-right .ce-gallery { margin-left: 10px; }\n    .ce-intext.ce-left .ce-gallery { margin-right: 10px; }\n    .ce-below .ce-gallery { margin-top: 10px; }\n\n    .ce-column { margin-right: 10px; }\n    .ce-column:last-child { margin-right: 0; }\n\n    .ce-row { margin-bottom: 10px; }\n    .ce-row:last-child { margin-bottom: 0; }\n\n    .ce-above .ce-bodytext { clear: both; }\n\n    .ce-intext.ce-left ol, .ce-intext.ce-left ul { padding-left: 40px; overflow: auto; }\n\n    /* Headline */\n    .ce-headline-left { text-align: left; }\n    .ce-headline-center { text-align: center; }\n    .ce-headline-right { text-align: right; }\n\n    /* Uploads */\n    .ce-uploads { margin: 0; padding: 0; }\n    .ce-uploads li { list-style: none outside none; margin: 1em 0; }\n    .ce-uploads img { float: left; padding-right: 1em; vertical-align: top; }\n    .ce-uploads span { display: block; }\n\n    /* Table */\n    .ce-table { width: 100%; max-width: 100%; }\n    .ce-table th, .ce-table td { padding: 0.5em 0.75em; vertical-align: top; }\n    .ce-table thead th { border-bottom: 2px solid #dadada; }\n    .ce-table th, .ce-table td { border-top: 1px solid #dadada; }\n    .ce-table-striped tbody tr:nth-of-type(odd) { background-color: rgba(0,0,0,.05); }\n    .ce-table-bordered th, .ce-table-bordered td { border: 1px solid #dadada; }\n\n    /* Space */\n    .frame-space-before-extra-small { margin-top: 1em; }\n    .frame-space-before-small { margin-top: 2em; }\n    .frame-space-before-medium { margin-top: 3em; }\n    .frame-space-before-large { margin-top: 4em; }\n    .frame-space-before-extra-large { margin-top: 5em; }\n    .frame-space-after-extra-small { margin-bottom: 1em; }\n    .frame-space-after-small { margin-bottom: 2em; }\n    .frame-space-after-medium { margin-bottom: 3em; }\n    .frame-space-after-large { margin-bottom: 4em; }\n    .frame-space-after-extra-large { margin-bottom: 5em; }\n\n    /* Frame */\n    .frame-ruler-before:before { content: \'\'; display: block; border-top: 1px solid rgba(0,0,0,0.25); margin-bottom: 2em; }\n    .frame-ruler-after:after { content: \'\'; display: block; border-bottom: 1px solid rgba(0,0,0,0.25); margin-top: 2em; }\n    .frame-indent { margin-left: 15%; margin-right: 15%; }\n    .frame-indent-left { margin-left: 33%; }\n    .frame-indent-right { margin-right: 33%; }\";}s:62:\"tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.\";a:4:{s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:2:{i:0;s:52:\"EXT:hive_cpt_nav_anchor/Resources/Private/Templates/\";i:1;s:52:\"EXT:hive_cpt_nav_anchor/Resources/Private/Templates/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:51:\"EXT:hive_cpt_nav_anchor/Resources/Private/Partials/\";i:1;s:51:\"EXT:hive_cpt_nav_anchor/Resources/Private/Partials/\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:50:\"EXT:hive_cpt_nav_anchor/Resources/Private/Layouts/\";i:1;s:50:\"EXT:hive_cpt_nav_anchor/Resources/Private/Layouts/\";}}s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:0:\"\";}s:9:\"features.\";a:2:{s:25:\"ignoreAllEnableFieldsInBe\";s:1:\"0\";s:38:\"requireCHashArgumentForActionArguments\";s:1:\"1\";}s:4:\"mvc.\";a:0:{}}s:20:\"tx_hivecptnavanchor.\";a:1:{s:18:\"_CSS_DEFAULT_STYLE\";s:582:\"    textarea.f3-form-error {\n        background-color:#FF9F9F;\n        border: 1px #FF0000 solid;\n    }\n\n    input.f3-form-error {\n        background-color:#FF9F9F;\n        border: 1px #FF0000 solid;\n    }\n\n    .tx-hive-cpt-nav-anchor table {\n        border-collapse:separate;\n        border-spacing:10px;\n    }\n\n    .tx-hive-cpt-nav-anchor table th {\n        font-weight:bold;\n    }\n\n    .tx-hive-cpt-nav-anchor table td {\n        vertical-align:top;\n    }\n\n    .typo3-messages .message-error {\n        color:red;\n    }\n\n    .typo3-messages .message-ok {\n        color:green;\n    }\";}s:27:\"tx_hive_cpt_cnt_nav_anchor.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:41:\"EXT:hive_cpt_nav_anchor/Resources/Public/\";s:7:\"private\";s:42:\"EXT:hive_cpt_nav_anchor/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:51:\"typo3conf/ext/hive_cpt_nav_anchor/Resources/Public/\";}}}}}s:43:\"tx_hivecptcntimg_hivecptcntimgshowfalimage.\";a:3:{s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:1:{i:0;s:49:\"EXT:hive_cpt_cnt_img/Resources/Private/Templates/\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:48:\"EXT:hive_cpt_cnt_img/Resources/Private/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:0;s:47:\"EXT:hive_cpt_cnt_img/Resources/Private/Layouts/\";}}s:12:\"persistence.\";a:1:{s:10:\"storagePid\";s:0:\"\";}s:9:\"settings.\";a:4:{s:6:\"bDebug\";s:1:\"0\";s:8:\"maxWidth\";s:5:\"1920c\";s:9:\"maxHeight\";s:5:\"1080c\";s:8:\"classes.\";a:4:{s:3:\"div\";s:17:\"caption container\";s:4:\"div1\";s:3:\"row\";s:4:\"div2\";s:3:\"col\";s:4:\"div3\";s:11:\"description\";}}}s:17:\"tx_hivecptcntimg.\";a:1:{s:18:\"_CSS_DEFAULT_STYLE\";s:472:\"	textarea.f3-form-error {\n		background-color:#FF9F9F;\n		border: 1px #FF0000 solid;\n	}\n\n	input.f3-form-error {\n		background-color:#FF9F9F;\n		border: 1px #FF0000 solid;\n	}\n\n	.tx-hive-cpt-cnt-img table {\n		border-collapse:separate;\n		border-spacing:10px;\n	}\n\n	.tx-hive-cpt-cnt-img table th {\n		font-weight:bold;\n	}\n\n	.tx-hive-cpt-cnt-img table td {\n		vertical-align:top;\n	}\n\n	.typo3-messages .message-error {\n		color:red;\n	}\n\n	.typo3-messages .message-ok {\n		color:green;\n	}\n\";}s:24:\"hive_cpt_dynamiccontent.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:72:\"{$plugin.hive_cpt_dynamiccontent.settings.production.includePath.public}\";s:7:\"private\";s:73:\"{$plugin.hive_cpt_dynamiccontent.settings.production.includePath.private}\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:81:\"{$plugin.hive_cpt_dynamiccontent.settings.production.includePath.frontend.public}\";}}}}}s:15:\"tx_hive_thm_bs.\";a:1:{s:9:\"settings.\";a:1:{s:11:\"production.\";a:1:{s:12:\"includePath.\";a:3:{s:6:\"public\";s:33:\"EXT:hive_thm_bs/Resources/Public/\";s:7:\"private\";s:34:\"EXT:hive_thm_bs/Resources/Private/\";s:9:\"frontend.\";a:1:{s:6:\"public\";s:43:\"typo3conf/ext/hive_thm_bs/Resources/Public/\";}}}}}}s:4:\"page\";s:4:\"PAGE\";s:5:\"page.\";a:13:{s:7:\"typeNum\";s:1:\"0\";s:16:\"adminPanelStyles\";s:1:\"0\";s:7:\"headTag\";s:101:\"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=1\">\";s:11:\"headerData.\";a:12:{i:10;s:5:\"HMENU\";s:3:\"10.\";a:4:{s:7:\"special\";s:6:\"browse\";s:8:\"special.\";a:1:{s:5:\"items\";s:9:\"prev|next\";}i:1;s:5:\"TMENU\";s:2:\"1.\";a:1:{s:3:\"NO.\";a:3:{s:7:\"allWrap\";s:61:\"<link rel=\"prev\" href=\"|\" /> |*| <link rel=\"next\" href=\"|\" />\";s:11:\"doNotLinkIt\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"typolink.\";a:3:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:9:\"field:uid\";}s:12:\"useCacheHash\";s:1:\"1\";s:10:\"returnLast\";s:3:\"url\";}}}}}i:3330001;s:4:\"TEXT\";s:8:\"3330001.\";a:1:{s:5:\"value\";s:31:\"<script type=\"text/javascript\">\";}i:3330002;s:4:\"TEXT\";s:8:\"3330002.\";a:1:{s:8:\"dataWrap\";s:36:\"var hive_cfg_typoscript_bMobile = 0;\";}i:3330003;s:4:\"TEXT\";s:8:\"3330003.\";a:1:{s:8:\"dataWrap\";s:47:\"var hive_cfg_typoscript_sStage = \"development\";\";}i:3330004;s:4:\"TEXT\";s:8:\"3330004.\";a:1:{s:5:\"value\";s:9:\"</script>\";}i:6666666;s:4:\"TEXT\";s:8:\"6666666.\";a:2:{s:5:\"value\";s:7625:\"var bLazy;!function(){var e=setInterval(function(){if(\"undefined\"==typeof WebFont);else{if(clearInterval(e),\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"use_webfontloader initialize\");hive_thm_webfontloader__webFontConfig={google:{families:[\"Ubuntu:300,400,700\"]}},WebFont.load(hive_thm_webfontloader__webFontConfig)}},2e3)}(),function(e,t){if(\"function\"===typeof define&&define.amd)define(t);else if(\"object\"===typeof exports)module.exports=t();else e.Blazy=t()}(this,function(){\"use strict\";var p,i,d,u=\"src\",v=\"srcset\";return function(e){if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy :: constructor\");if(!document.querySelectorAll){var s=document.createStyleSheet();document.querySelectorAll=function(e,t,o,i,n){for(n=document.all,t=[],o=(e=e.replace(/\\[for\\b/gi,\"[htmlFor\").split(\",\")).length;o--;){for(s.addRule(e[o],\"k:v\"),i=n.length;i--;)n[i].currentStyle.k&&t.push(n[i]);s.removeRule(0)}return t}}var t=this,o=t._util={};o.elements=[],o.destroyed=!0,t.options=e||{},t.options.error=t.options.error||!1,t.options.offset=t.options.offset||100,t.options.success=t.options.success||!1,t.options.selector=t.options.selector||\".b-lazy\",t.options.separator=t.options.separator||\"|\",t.options.container=t.options.container?document.querySelectorAll(t.options.container):!1,t.options.errorClass=t.options.errorClass||\"b-error\",t.options.breakpoints=t.options.breakpoints||!1,t.options.loadInvisible=t.options.loadInvisible||!1,t.options.successClass=t.options.successClass||\"b-loaded\",t.options.validateDelay=t.options.validateDelay||25,t.options.saveViewportOffsetDelay=t.options.saveViewportOffsetDelay||50,t.options.srcset=t.options.srcset||\"data-srcset\",t.options.src=p=t.options.src||\"data-src\",d=window.devicePixelRatio>1,(i={}).top=0-t.options.offset,i.left=0-t.options.offset,t.revalidate=function(){n(this)},t.load=function(e,t){var o=this.options;if(void 0===e.length)a(e,t,o);else b(e,function(e){a(e,t,o)})},t.destroy=function(){var e=this,t=e._util;if(e.options.container)b(e.options.container,function(e){l(e,\"scroll\",t.validateT)});l(window,\"scroll\",t.validateT),l(window,\"resize\",t.validateT),l(window,\"resize\",t.saveViewportOffsetT),t.count=0,t.elements.length=0,t.destroyed=!0},o.validateT=f(function(){r(t)},t.options.validateDelay,t),o.saveViewportOffsetT=f(function(){c(t.options.offset)},t.options.saveViewportOffsetDelay,t),c(t.options.offset),b(t.options.breakpoints,function(e){if(e.width>=window.screen.width)return p=e.src,!1}),setTimeout(function(){n(t)})};function n(e){if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy :: function initialize(self)\");var t=e._util;if(t.elements=function(e){for(var t=[],o=document.querySelectorAll(e),i=o.length;i--;t.unshift(o[i]));return t}(e.options.selector),t.count=t.elements.length,\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy :: \"),console.info(t.elements),console.info(\"blazy :: \");if(t.destroyed){if(t.destroyed=!1,e.options.container)b(e.options.container,function(e){o(e,\"scroll\",t.validateT)});o(window,\"resize\",t.saveViewportOffsetT),o(window,\"resize\",t.validateT),o(window,\"scroll\",t.validateT)}r(e)}function r(e){for(var t=e._util,o=0;o<t.count;o++){var i=t.elements[o];if(s(i)||y(i,e.options.successClass))if(e.load(i),y(i,e.options.successClass))t.elements.splice(o,1),t.count--,o--}if(0===t.count)e.destroy()}function s(e){var t=e.getBoundingClientRect();if(y(document.getElementsByTagName(\"body\")[0],\"bIOS-1\")){var o=e.parentNode;if(y(o,\"focuspoint\")||y(o,\"focuhila\")||y(o,\"image\"))t=o.getBoundingClientRect()}return t.right>=i.left&&t.bottom>=i.top&&t.left<=i.right&&t.top<=i.bottom}function a(t,e,o){var i=document.getElementsByTagName(\"body\")[0],n=t;if(y(i,\"bIOS-1\")){var s=t.parentNode;if(y(s,\"focuspoint\")||y(s,\"focuhila\")||y(s,\"image\"))n=s}if(!y(t,o.successClass)&&(e||o.loadInvisible||n.offsetWidth>0&&n.offsetHeight>0)){var r=t.getAttribute(p)||t.getAttribute(o.src);if(r){var a=r.split(o.separator),c=a[d&&a.length>1?1:0],l=h(t,\"img\");if(l||void 0===t.src){var f=new Image;f.onerror=function(){if(o.error)o.error(t,\"invalid\");m(t,o.errorClass)},f.onload=function(){if(l){if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy :: [begin]\"),console.info(t);g(t,u,o.src),g(t,v,o.srcset);var e=t.parentNode;if(e&&h(e,\"picture\"))b(e.getElementsByTagName(\"source\"),function(e){g(e,v,o.srcset)});if(y(e,\"focuhila\")){if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"Parent height: \"+e.clientHeight);t.style.height=e.clientHeight+\"px\"}}else t.style.backgroundImage=\'url(\"\'+c+\'\")\';if(_(t,o),\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"[end] :: blazy\")},f.src=c}else g(t,u,o.src),_(t,o)}else if(h(t,\"video\"))b(t.getElementsByTagName(\"source\"),function(e){g(e,u,o.src)}),t.load(),_(t,o);else{if(o.error)o.error(t,\"missing\");m(t,o.errorClass)}}}function _(t,e){if(m(t,e.successClass),e.success)e.success(t);b(e.breakpoints,function(e){t.removeAttribute(e.src)})}function g(e,t,o){var i=e.getAttribute(o);if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy :: handleSource(ele, \"+t+\", \"+o+\")\");if(i)e[t]=i,e.removeAttribute(o)}function h(e,t){return e.nodeName.toLowerCase()===t}function y(e,t){return-1!==(\" \"+e.className+\" \").indexOf(\" \"+t+\" \")}function m(e,t){if(!y(e,t))e.className+=\" \"+t}function c(e){i.bottom=(window.innerHeight||document.documentElement.clientHeight)+e,i.right=(window.innerWidth||document.documentElement.clientWidth)+e}function o(e,t,o){if(e.attachEvent)e.attachEvent&&e.attachEvent(\"on\"+t,o);else e.addEventListener(t,o,!1)}function l(e,t,o){if(e.detachEvent)e.detachEvent&&e.detachEvent(\"on\"+t,o);else e.removeEventListener(t,o,!1)}function b(e,t){if(e&&t)for(var o=e.length,i=0;i<o&&!1!==t(e[i],i);i++);}function f(t,o,i){var n=0;return function(){var e=+new Date;if(!(e-n<o))n=e,t.apply(i,arguments)}}});var hive_thm_blazy__interval=setInterval(function(){if(\"undefined\"==typeof hive_cfg_typoscript__windowLoad);else{if(clearInterval(hive_thm_blazy__interval),\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy initialize\");bLazy=new Blazy({src:\"data-echo\",offset:0,successClass:\"fadeIn\"})}},250),addEvent=function(e,t,o,i){if(null!=e&&\"undefined\"!==typeof e)if(e.addEventListener)e.addEventListener(t,o,i?!0:!1);else if(e.attachEvent)e.attachEvent(\"on\"+t,o);else e[\"on\"+t]=o},debounce=function(o,i,n){var s;return function(){var e=n||this,t=arguments;clearTimeout(s),s=setTimeout(function(){s=null,o.apply(e,t)},i)}},hasClass=function(e,t){return-1!==(\" \"+e.className+\" \").indexOf(\" \"+t+\" \")},watch=function(e){var t=document.getElementsByClassName(\"b-lazy\");if(t.length>0)for(var o=0;o<t.length;o++){var i=t[o].parentNode;if(hasClass(i,\"focuhila\")){if(\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"Parent height: \"+i.clientHeight);t[o].style.height=i.clientHeight+\"px\"}}},hive_thm_blazy_addons__interval=setInterval(function(){if(\"undefined\"==typeof hive_cfg_typoscript__windowLoad);else{if(clearInterval(hive_thm_blazy_addons__interval),\"prototype\"==hive_cfg_typoscript_sStage||\"development\"==hive_cfg_typoscript_sStage)console.info(\"blazy addons initialize\");addEvent(window,\"resize\",debounce(watch,1e3),!0),addEvent(window,\"orientationchange\",debounce(watch,1e3),!0)}},250);\";s:4:\"wrap\";s:18:\"<script>|</script>\";}}s:14:\"bodyTagCObject\";s:3:\"COA\";s:15:\"bodyTagCObject.\";a:7:{s:4:\"wrap\";s:22:\"<body class=\"page-| \">\";i:10;s:4:\"TEXT\";s:3:\"10.\";a:1:{s:5:\"field\";s:3:\"uid\";}i:15;s:4:\"TEXT\";s:3:\"15.\";a:2:{s:5:\"value\";s:0:\"\";s:10:\"noTrimWrap\";s:5:\"| | |\";}i:20;s:8:\"USER_INT\";s:3:\"20.\";a:1:{s:8:\"userFunc\";s:62:\"HIVE\\HiveCfgTyposcript\\UserFunc\\GetBrowserInfo->getBrowserInfo\";}}s:3:\"10.\";a:5:{s:18:\"templateRootPaths.\";a:2:{i:300;s:56:\"EXT:hive_cpt_nav_anchor/Resources/Private/Templates/Page\";i:20;s:52:\"EXT:hive_thm_custom/Resources/Private/Templates/Page\";}s:12:\"templateName\";s:4:\"TEXT\";s:13:\"templateName.\";a:1:{s:8:\"stdWrap.\";a:3:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:3:{s:4:\"data\";s:45:\"levelfield:-2,backend_layout_next_level,slide\";s:9:\"override.\";a:1:{s:5:\"field\";s:14:\"backend_layout\";}s:6:\"split.\";a:2:{s:5:\"token\";s:8:\"pagets__\";s:2:\"1.\";a:2:{s:7:\"current\";s:1:\"1\";s:4:\"wrap\";s:1:\"|\";}}}s:7:\"ifEmpty\";s:7:\"Default\";}}s:16:\"layoutRootPaths.\";a:1:{i:20;s:45:\"EXT:hive_thm_custom/Resources/Private/Layouts\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:46:\"EXT:hive_thm_custom/Resources/Private/Partials\";}}i:10;s:13:\"FLUIDTEMPLATE\";s:12:\"shortcutIcon\";s:61:\"EXT:hive_thm_custom/Resources/Public/Assets/Icons/favicon.ico\";s:11:\"footerData.\";a:2:{i:9999999;s:4:\"TEXT\";s:8:\"9999999.\";a:2:{s:5:\"value\";s:39:\"var hive_cfg_typoscript__windowLoad=!0;\";s:4:\"wrap\";s:18:\"<script>|</script>\";}}s:11:\"includeCSS.\";a:2:{s:26:\"hive_thm_custom_includeCSS\";s:83:\"EXT:hive_thm_custom/Resources/Public/Assets/Gulp/Scss/includeCSS/includeCSS.min.css\";s:22:\"hive_thm_custom_theme.\";a:1:{s:5:\"media\";s:3:\"all\";}}s:14:\"includeJSLibs.\";a:1:{s:29:\"hive_thm_custom_includeJSLibs\";s:86:\"EXT:hive_thm_custom/Resources/Public/Assets/Gulp/Js/includeJSLibs/includeJSLibs.min.js\";}s:20:\"includeJSFooterlibs.\";a:1:{s:35:\"hive_thm_custom_includeJSFooterlibs\";s:98:\"EXT:hive_thm_custom/Resources/Public/Assets/Gulp/Js/includeJSFooterlibs/includeJSFooterlibs.min.js\";}}s:23:\"fluidAjaxWidgetResponse\";s:4:\"PAGE\";s:24:\"fluidAjaxWidgetResponse.\";a:4:{s:7:\"typeNum\";s:4:\"7076\";s:7:\"config.\";a:4:{s:8:\"no_cache\";s:1:\"1\";s:20:\"disableAllHeaderCode\";s:1:\"1\";s:18:\"additionalHeaders.\";a:1:{s:3:\"10.\";a:2:{s:6:\"header\";s:24:\"Content-Type: text/plain\";s:7:\"replace\";s:1:\"1\";}}s:5:\"debug\";s:1:\"0\";}i:10;s:8:\"USER_INT\";s:3:\"10.\";a:1:{s:8:\"userFunc\";s:42:\"TYPO3\\CMS\\Fluid\\Core\\Widget\\Bootstrap->run\";}}s:9:\"sitetitle\";s:0:\"\";s:6:\"types.\";a:2:{i:0;s:4:\"page\";i:7076;s:23:\"fluidAjaxWidgetResponse\";}}}'),(6,'da6fa01f471a95920dde52ebd699a510',2145909600,'a:19:{s:32:\"3c310046f75970e8784c9d09f0dbf455\";s:33:\"[globalString = _SERVER|HTTPS=on]\";s:32:\"93bcfb659962566317312256826343be\";s:68:\"[userFunc = HIVE\\HiveCfgTyposcript\\UserFunc\\GetBrowserInfo::bMobile]\";s:32:\"4d36dc1e659fdf4f6f6052e5ad3a8c0e\";s:34:\"[applicationContext = Development]\";s:32:\"b2b23aa2e4e7a531f975231772684e1a\";s:33:\"[applicationContext = Production]\";s:32:\"9bf651d4d8fd6a163975ad421e311bd0\";s:94:\"[globalString = ENV:HTTP_HOST=development.*] || [globalString = ENV:HTTP_HOST=*.development.*]\";s:32:\"557b89c655a7dabd67c7337a9efdb5d2\";s:86:\"[globalString = ENV:HTTP_HOST=staging.*] || [globalString = ENV:HTTP_HOST=*.staging.*]\";s:32:\"13b44bc9186e25195347f725ced2b22f\";s:36:\"[globalString = ENV:HTTP_HOST=api.*]\";s:32:\"6d8c32aa04180a41524612a200afca57\";s:48:\"[globalString = ENV:HTTP_HOST=api.development.*]\";s:32:\"aef7c87734e19c2563f85d8d4d0e8b4e\";s:44:\"[globalString = ENV:HTTP_HOST=api.staging.*]\";s:32:\"4c6f630ae5b70906f1ba125c08bb1ecd\";s:42:\"[globalString = ENV:HTTP_HOST=*.localhost]\";s:32:\"17b50dab0d7db16d89983525e409a241\";s:45:\"[globalString = ENV:HTTP_HOST=*.localhost.de]\";s:32:\"15557b18e3f34505057330121262b582\";s:45:\"[globalString = ENV:HTTP_HOST=*.localhost.ch]\";s:32:\"7107021e0be664fc828f2543a3e1a425\";s:45:\"[globalString = ENV:HTTP_HOST=*.localhost.fr]\";s:32:\"cbe7dfc77ad9d279a2a883f63eef52a7\";s:44:\"[globalString = ENV:HTTP_HOST=development.*]\";s:32:\"60028226b0989b53852c068c0a07714d\";s:23:\"[globalVar = LIT:1 > 1]\";s:32:\"3652062149e223f25ffa91556d2e2994\";s:23:\"[globalVar = LIT:1 = 0]\";s:32:\"e040ce30aefbc1b839e19defc802fa7b\";s:42:\"[globalVar = TSFE:content_from_pid = /^$/]\";s:32:\"148639764d4245682803e68c30b4dccf\";s:50:\"[globalVar = LIT:1 = 1] && [globalVar = LIT:1 > 1]\";s:32:\"d88857afbb43598769566b1dc6a3bfd8\";s:120:\"[globalVar = LIT:1 = {$plugin.tx_hive_thm_webfontloader.settings.production.optional.active}] && [globalVar = LIT:1 > 1]\";}'),(7,'fe5fdaa957379e5a9a4a0837af3fa5fa',1561276729,'a:0:{}');
/*!40000 ALTER TABLE `cf_cache_hash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_hash_tags`
--

DROP TABLE IF EXISTS `cf_cache_hash_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_hash_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_hash_tags`
--

LOCK TABLES `cf_cache_hash_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_hash_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_hash_tags` VALUES (1,'89dd425378b0a106e79d917e41fb6c3e','ident_PAGES_TSconfig'),(2,'eaa0ffa2db0b3f6820292e059110d999','ident_userTS_TSconfig'),(3,'8ca70ca2359d16a666e14c14677f779e','ident_PAGES_TSconfig'),(4,'c86790f9b1f60a6f5ef5e2a15eb88117','ident_userTS_TSconfig'),(5,'efa955de67f970b5d0a3b231c66f125a','ident_TS_TEMPLATE'),(6,'da6fa01f471a95920dde52ebd699a510','ident_TMPL_CONDITIONS_ALL'),(7,'fe5fdaa957379e5a9a4a0837af3fa5fa','ident_MENUDATA');
/*!40000 ALTER TABLE `cf_cache_hash_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_imagesizes`
--

DROP TABLE IF EXISTS `cf_cache_imagesizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_imagesizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_imagesizes`
--

LOCK TABLES `cf_cache_imagesizes` WRITE;
/*!40000 ALTER TABLE `cf_cache_imagesizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_cache_imagesizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_imagesizes_tags`
--

DROP TABLE IF EXISTS `cf_cache_imagesizes_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_imagesizes_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_imagesizes_tags`
--

LOCK TABLES `cf_cache_imagesizes_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_imagesizes_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_cache_imagesizes_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pages`
--

DROP TABLE IF EXISTS `cf_cache_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pages`
--

LOCK TABLES `cf_cache_pages` WRITE;
/*!40000 ALTER TABLE `cf_cache_pages` DISABLE KEYS */;
INSERT INTO `cf_cache_pages` VALUES (1,'redirects',1558688329,'x�K�2���\0O�'),(2,'e841c56680242d73df70ab5b302f0d61-titleTag-record',1561276729,'x�+�2�R���MU�\0$'),(3,'e841c56680242d73df70ab5b302f0d61',1561276729,'x��}�s۸����ۿ������u?��I�I��M7����N��%�V#�^IN�m��\0��p�tg����n,\0I\0@��������8({.bo��|�[e��i:���n���r���d��Ɲq�њ4��&���l�G�[>����Aİ�����_<;�����4�����^��ςi�̃2�y�J��^���w�咢�+�yn<���s�=�J^���\"���׬͡`����Ҝ}��Ռ3caā�2�lu��g[[����p�E�;>������B�񐻥�}	�~�.mA����`ZZp��y)���g�N0��J���X��e��R_��t����a�w\"��#��KN�Y,[��E�7��3������ꗁuo.�o��:�[M;bqz�Y\\j��w�Z��~IL��꥓�Н��6�\Z\0�3��{�0����4s\ZLD8g1�QP>�7^ʂ�/�����~!�uN����B��Y�K��Oze��%��9Q�Hh&�xe�{�\r�������Y�%\Z*+|I\n&��s�=����|�,�X̷���x�}����\"u�}\n����\"�:��\\B�m���y��P�����}x:<;��%�.<?e�ޞ�\\%5��?��N_\r�(X~z�?{�kAV��^�䨽�֤*\n��˷��.�w���O��j�g���w,\\�B�EQ���V�TB�Ӌ�hп>�\Z�g�;��L&�5���{.���.����=��T+��M�989�����h`��nu����x\nJ�?u�y���,\\(�\n �������l�d�iz<�c��k��¬����O\'�J���W�/����c��y���*�Q��o]�۵�/�����}��n��x�h�����4g�~\r\0�wM�,�\'ҋmT�N�\Z�N�Q9̙��bV>d;�v��L�)��;����g\Zz�ϯBq�\r�P}�����J�PMk������_ĕ����Ǿ�G���Ȣpe!A���%W!wD.rE�Q>Q�͢�_i�p\Z�;刋d�Y��6�Yx��2��\0x�&��j���.���A[��Q�\r/e	���\n\'Z��V�ѐ�(p��,H���T�Ĉh�ŭ�&����G����l�&0kx�.�8�����T��t񙦌���3��\\��q1t�@\'��Bt�\"����W�-�Đ�G���D1JF\'\0����na�`/��[C>_��|�s�W,�̈́}Č�,�J�@I�o���<��g�8�� :�B�0`~z�;�\Z�iJ�$��h���ɥ�f䬫h�4*k@�A��YŃ��A����ҹ!�R?��G��D�t��51]ӏ�Um�8Dk��.7X�2Amjq�^�������󹎖5̎4^פ<D��W�Z�� ��\r(�q&��[��\"�#�л�m�\r%�:2E�9���_K�?x$F�~�e6.גTQ�h23�F:� ny���pr��`w+���:�}n����o!\n��Y\'O���\'����[���|��uH}s�S�y�����ὂ���h�uh}�������ڇy(��Ϟ�@����V1�A���y!���6\'���X�tr���>�Wۿ�eXI���+�8�ִ�8�q�-aa\0!�Cn��.p�)�\\@DmL`n\0�S4VD1#��V��Y�_i�?�Ü�?�=�Q�)w?g�t\"Qj�u1v��چ��\0��#�	�/���l�M\0�ԡ\r4p�����c��+�ÂųGPv$��0�r=���h�Z�K�\nÀt��S��k��,�V7��@ڻ�����d+;�G[Q��}��I����k��T�Q��B�z�@yDD]	)i�H} 2��$�!�\"$L���BN�qf��(%:��2�G;����:�!�Gw�0��Gq)�@�c��G�;�2�&v2���j�W�o6舷8��4(�aQ��rh)�3�M�HγB?�N߄b�x�2�2N�H�m�-��4���H�7��b!Bg�G���B��ydB}�F��o�����y�4 �M������.���Q\\�l��bEIc8�D��t9��?�����GG�x�	�P���x�\\<6�V+A����%ͮi��w�u��9�(�T�h�`-珒ڷ0�\0��p�ER	��9Ѕ��Σ�pN��g͹r�J��[���bؑ(�PB;�I��6-^n�B��\r���t8�;�[�8�=e*vw�*��&�5Y�L?�0��4�o3y����?r0�{j0}0�SޯCq�Ԓ���U�Ҳ�{�Q^I�Bk�?�p�)���ޑ(4ѯ��Frd\Z2���\r6i��ˑQ��\'H�Op�ˣ��h�xrKhW��[�	[��Q8]�fCjo����4��$8��&��i�*�����m/���,�ij�E(W�Y�r�NnX��c<Y�d�/�-VS�ŕҐ�e�\\u��@1N�~�9�wY��6@e\Z\n��\"��;h����;�܅<Vm��29dE���#�@���»�*m�R�c���� ��ފ���=�7v;2Ъ��{��Ǭ�]M��2֍@��s]�g��Vخ��L�K����ww%w!���x!g����cɠn�ˇj�ő,yF��j��d�Ǿ(��dF�P��p$|Sަ>S�6�dE�c&�H2g�(F>�٣�>@�h���9�~���z��o�Dhav�|��6�٩�A]fup��PN/���5o�7�i�p�go�!�m�&��2pHM�=<=��ֻ���th�.?�W`���U(��Ϝ�^NS���ù�k�n�uy�I�4��T��\0\rAx�Iq�苞\0�@\r���g%U��<P�Zr�|��(v�0�t�l��vm�ʪ�OX.��&č�ntvq͙���H7���P�a�6��}�s���<`]��0d�3u�0��Ɔj_�V�f�l/���ZLz��Ѵ�s����+W��\Z ����\"B�F�Q��^nW=�0ae\'���M�P��k�rh8]��k��*G|�Eei�]/4C�y�7񐏿�&j�~�!1�_\'�Jp��jӯ�ZVMJ~�I]�{ԁ���|��J�b9W,�f�\0,G�H&Ky����)�\'�携�����M1Ua��F�5OE��5��_YQ���j��c��k��RڱLO��I�\'7����2G���ʷ�V���\"���͝P�>8�l�-�Y�ám�0�f:�m4�n�,1 ������� �<3p�%�E]��W0S[�Ņ�J\0+lwz\"���w��M�t۴n����wdE�\\�[����UE�� �Ή�#ˋ,r�g:�6vUM�ꀉ�A�l�BF����ϔ��3*�_�U�\"�hE������5��RU0�4�ܓV�E�*\'�B+y�����I���Cݡ�B]`���ȟE2\Z��<*��rd9���Q0�c��Ѱ@��������$<*ʺf���[���{ê.d�j��ّ?��*�{�Qgۙ��i���\n�z�w<�S�F��<T�T� ����-���O�_����W,aU�*��.7��V}�k�t��lcq�����*\n�\0ca�Ʌ!S�B*�Z��H�� �v���;��N�t]��1��,jӮ,ĥP\Z&Ja�d�WwH����N���T-�x=~+P�7�\\\Z��$�7SY�������]������B�T$��]Y�yw��U�Ez�+����Ǣ����套����E1֞�Z7�v�څA��5)w�bH\\(�a�D*��ڹc�����,V/�2�;�/4����ul�� ^�&�����p\'Q��z1e���V�:T���wh�����S0�Uf��U�,���!?�;|���[�U5E�\n��|�M�Z�5�oX�ܕt���o��w��|�sT<�{#_hpÓ\r	���V�\'�J��G�����!����_/��\'u���Q�#���\Z�^뛨�r{�h��\'�\Z�V�HW�}�t7$�^�>=�\\�ȓ#ґDh���ܐJTD�������dN�͒�m=3�>\\���Kr����ɝ��q1<Q�oo�E��O���ǃ�Iɤ�B��=?�xO�`�}����s�΃e�}Н�ˣc�ߛ��Pi�&�v&�{ͧxГ�\\�A\"�����M\rBA��8������h(���A�9��x}���8Er��̵t���74�M�9\0�Y�O� �7�g@fy�<�������	-�:�\rh�h��!��v�����:$����Gf��l���N�����k�d���8Lմ����	�\"�F��}Xٺ���_\"�)`�`�����P��aD���d�0y>9��`ks\nn2�Ҙ���X�^�Cő��M�� ]o2�DжaBC\r��>��8��/�i���:�K�1��F˱U@g\"��\'u51�LP��ݏ@hOF�1�bjӢ�x:*:��bzGP���w^�<�����8)�~����]x�Wx(h�^(\"�G|�<?��4�����\ZL}>������A�ɞU��G�0��G���>����ɆG;#J�E�I��n��u�(�\"�c�u� ���\ZY�;��v��xl�	��\'����7&B1���J\\�zGk�|�	M�N�F�|���@;�G��P\\��8��<veV������c�ܑ;�4���b(k��М\\�9�Xы�\r���\'\\dm�]�H>gARV�g2\n��J�gd3/���(��d���)i\Z��iA���vv�,N��>��嘲2J�3P�%%j%�4�ʉ���K�k��RO~lO�	Y�\"���9I@1n��m��|4��8��@��d��������8�]2�,Gl�i�B���~\"{���B�qO\Z�y��UH�������������ɱճ�ϲ��F�:E,y�X�QA�K�5��ƙ<P);�H�O��	��F}��@��]~ܻ&\\�J��O�s�߫K��7{����Fr�*�`[H+�2��QIO7	�,˔�2O��dC���m���/�҆L��0&A�0�y\ZB���\"���nl���!��h�����7Ej~R�[��Rk�$�9���s?�Ѿ{�O�D8m\Zd��߾�[�_��H+�������p��JKe>��\rz��R~���`���ף�p�vm�P/�@�ޅN�E������CSݥm�k��P\"wn�����t]�mՍ�{,㊳$�Ցwt�c��D|{���0#�/G�ŬC��O��ؔ�o��ΎM�vQ�ڇ\\��)�+�š]��~9��V���I8|k�pY��Y@G�M�It��e�H��Χzc3I�O������x(�v��\Z�0$�C+Xl��L���Ӊ���KK���L:_J�Ua����=��}���ΝQ��N�o��◱Q���T�vj;�\\���~E��{_3�-�E*Uw,Ǹ��dM[��du<Crv��Io\Z.��#}sm������z���^��JKVbG�Q�C�\ZBv��؄�\0&�u\0�K��%˪3��~�L}�\0�ꭒ��O�a��ސ����T�Sߓ�L��,��I��0&O�B_�r���ұ6ïDx*��u�䫥?eaR��C�>�e?)�鐹?�^� T�yUD������T��Y���dn��5�iAF�@�\r,�nt��۳x3b�+��4�;�\'0����j��~]��9&z�W�j��x���X�\'B�P���l�os�� &��M��)U�,`.�����o��h��i������ROQ~��Ӆ�\"�(YBb���v���E���� \\ר����4�u����T��9�.�E8M�o��&(� ���\\����\\Z,j�?�|1d���;��ْY��oUG�X|�ϵ��3ߤ�Z������@�ԅ�������t[��\\�\"{9]r��v����U�~�s�4fh	Ŭk1��.�P����~�N�������X�-*ѯ���?��G:�i�ñU��7��%���8�#���k\n�ex��.�\0�oB�Y-�z�U?y<Ɨ�\r~���a�ܪ<4��ԋ�5�D�\\�\nF��`��`)��?~K��a쇯\'��15\nժ95`��\'�c�Ry�1��Ϣ�h�t��{�;9ש0���4#]���/�?tsK=���ɭ��\"�Mi�S\'~���)#.����D�a)��4����|,�4�Z0�7�<���<���z�FBR	yy|��#l$t��7F���ZEl	%�0��	y	ݦ�DF���I�Y���2�8�T,� ��Z� �\'�B��F+{�j�f+uߙ�>�d�۩�$�t���8s�#��H�c|W�S\0���W�g�8�2S�%}�|�l�\\���njW&�=���7K��.�Vm�`o�Ou�|��M�Q6�̀-}��LyV��b�^�旉BS@o.�!����q,�}����bF�&f%�c�;���}J�W�!,���\'A������(o�1�l�r�Q��ԓ	2�d� &$O��z�\\����lt�0��6M��Y����J�XM�(5��,J�\'	W-$��Y�L���w���o�r��D�u�T\"эqi�\"T��Q��=�a��-��]��R�m۽OL���d�������݅�V�w)�\0٤T�?X?Iy�q�/h��)�o�D���dhh}O��WYp��1���vKY;a��#K���v�s�]r(w�6��pGm��f̿�m�j+�E5���U(|1��p�3��a����x��|�6��uū���S�VR*9JEm���?���	���A��6��~{K3oo�-���R<�_޲�TpU�h|.�`J�R�pL4��\0)[�Z>|�-�_���������Nr�T�`)��Ԡ�[�W�JoR)�0&�Yn����������!�#�@t��!��5�U8��{���~xH��k�U����u�T��F���/��Zͼ�x�zh�A\nf���sy�Q��T����L���!�>�P~?^���h�v࿽F���jUSC�#��Fԫ�����R���Z,��K`̚���ϟ˿u6w��gEq����=U��O!DUy�^W�����x��BǂU%P��|,��U����y5��앣�)�n�/�(�x�d$r�������NF��sIA���3W8t[K��%�t��|_*u�3\0����������=[V5��_�.T���Z���KM�*��������\0�q�=�j�h�_co��0+�Z�Z��<�ƳC��u(�Fu�� �\n� ~��on�՚�4����:���o�?��x�X���aT�[�D�Q])�ūr#��E/������/�CQ�a�����]���=k�⺠�����J��<E��<?<<kZ0b2��gd�\r�t=Z:��I/W�!)���E�Z��jI9���YU(6��`@���k5�R\0]=H��XC/�d�E�Б�=������U�WX�aZ���֋���{kʳ�l�v�.�Αes�ށ��0��쾷���ձ[c��\'�͖KR�,�\Z���NJ]���h]��n)S�F�-��p�����wu��+�3��1\0x٬U<�#�z,��VV�k^�s)��ajf�,S���LB���>稠9�額��[j����T�4Kʶ��ɶȺvEͨ���y{21%S�\r�U���W_+���������_��-�%����~�:�#\0�S�z�&ښ)^Q�m�V`�L����cW���ͧi8��.�H�Qq$^�YW\n�~-�y�>v�R+20��@V�X��A�kϚ�j\r�-3+i�D�9�E�������@�F~Rw/=�����{6CПJXr��M7G+lחA4�&qE�;�\Z�iM�t��iݟ�\Z�A���I����Q$�̴ !��I��m�@|��M�s�e�W��*�~���i�P�j�8/���?H��Y��8�U����}ų8b{�j�& -	�j�k���`0���5�mm� \\5����ʵT*֠\"��y�������t���W��ԉj����M���\n}G�\\�Ѐ�s|z9�jBp���˼��rIK�� �\r*ST���)�i�0Z�Zk�f��:-�_��#�Xv,�X̱|.`�x$H,�r|��	OX%�q�zJ�<��rx^�ey�yey%<z\n��=tN�����\nL�\r�e�&��x�\\��Ī����0��XnN����	4���Db�P-CD������L���6�3~o݆�L���L(i\":���D��N�\"�+������nU�%��Y =\'�$18L�������{���_r&�z8�>/k��5|���U|�QX���:.`��Q;М��zW����S���Y�)�m��ktE+�H%�����Mo��U�k]��t�����jE�����)��D����2�+�����r�8�j�_����/(|҂2)#� ���e�Vz[��~\rK�p�ٞ���tWU�<��&�[�D��i�DZ%�	�h�x�%�U��5t]������p���ѩZ�(��3��4����3��̀�_W\\�m�?�rUZ9��?Ŀ�Ћg�\'����M92^`��*bq�[�x�C���2���q���z�JYMb�\0��:��^N�2������>��6z�Ph�x�+{*n���Cr.><G�(�M��xM��4rS�!(��tLhŠ��1sf\'�������c�,�Y�r�5�\Z�e�B��b��j��vK��d=�[���Hcc#��qUǝ�,�a��8�^��ϟ���\n�uF�̓�0!4!ڞ�8�dw^�*�t����\r������Qg���q\r�7Z�A�&G�yM�c4�Զ��\0���H�ޣ��l`]��y�� uL@�!��y�X1y�wf�\\��A�f�L�^u�iP^��Q�ie������?�^X�|�l��eDV��kT������9��$���Y�KO�j.��u�M*��M�Z� ؔ�GF�I�z[|J�+�f�z�\n�TR?qƬ�G��S���m�o1��;;�Ծ�\\0/��`�����\ZY��\rk.˕��·���b��|z@����i^6H����4�(N���+($4�o��N�e�C6�� z���-�쪐6֚�]�=�,�A�\r��N�S��q��INv��#x��|ۼ�X��ڙ��_��en�>�6�]s����ڒl�Y��u�E� �G�rxA/^l�T�g���M5B��k(���9�G=\r��7�mH�\\\r�hj���橵��y��\0#����P.m�ԉ#��9��I0���c�V��~O]��_�y�;�������߭��t�M��v��}2	(:�mM5�_xg�}�ކ6�-�-�C�]�\r�`B�A�z��C����C�U�4�3����/����_�U�Y���wѕ�C?h���uD���hɵ��}�g�mكO��\'�R��DJ���c۲������Z�����Eoʽ�5���r�ޮ7ͧ�����/���\ZJ�T/�b���>�e[xx�0��m�)V]G�~�	R)��5�H^/��!ȢE�c�\rz�_��R�HA�d���P���e�*F�{|�t\'>S_=߱h��\'���|�I.1f�h�2��ݴ���\'�4�P�%^�*yN�A�F��[�I��Ȫ����r�r�kAU�c��@��:�[m���e_Z�<�{�l:vc��2��ʻ��}4����_<��?>\Z}������������%����;[�U�e�[E�J���(f�f[CV2m�k\'h¬v�H�Y��{B)#���I>x�2������k\'�&�i_f��Ko͞K(�W�s�I����N^�g��L���Ω�_ҟL��F⥛կӨ�a%_3�H�֒�;�;Y��\"q>��p-�fw]6�&�[��^A�N��n4�v����^���X~�m�k���ӓK@�k����+V�tfo]g���B������������E����fkow�����G�����۪��R#Zs�I��W\ZǞrL��e����j����!'),(4,'e841c56680242d73df70ab5b302f0d61-metatag-html5',1561276729,'x�+�21�R��M-IT�K�M�UJO�K-J,�/RRH��+I�+�U\n��7Vp�\rVRзS�\0�8g'),(5,'e841c56680242d73df70ab5b302f0d61-metatag-twitter',1561276729,'x�+�21�R��M-IT�K�M�U*)�,)I-�JN,JQRH��+I�+�U*.��M,�TRзS�\0��');
/*!40000 ALTER TABLE `cf_cache_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pages_tags`
--

DROP TABLE IF EXISTS `cf_cache_pages_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pages_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pages_tags`
--

LOCK TABLES `cf_cache_pages_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_pages_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_pages_tags` VALUES (1,'redirects','redirects'),(2,'e841c56680242d73df70ab5b302f0d61-titleTag-record','pageId_1'),(3,'e841c56680242d73df70ab5b302f0d61','pageId_1'),(4,'e841c56680242d73df70ab5b302f0d61-metatag-html5','pageId_1'),(5,'e841c56680242d73df70ab5b302f0d61-metatag-twitter','pageId_1');
/*!40000 ALTER TABLE `cf_cache_pages_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pagesection`
--

DROP TABLE IF EXISTS `cf_cache_pagesection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pagesection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pagesection`
--

LOCK TABLES `cf_cache_pagesection` WRITE;
/*!40000 ALTER TABLE `cf_cache_pagesection` DISABLE KEYS */;
INSERT INTO `cf_cache_pagesection` VALUES (1,'1_222419149',1561276729,'x�Ś{o�8���,��`�-�E��M}�[��	�H9BeK�#�l��~Cٲ%\'����@+ќ�����hT9���9�u&���c��\'�3\ZP���=�qY�i�H��q~���_�8Q2�Y��\r^���է�����E��RM�UA��<A�;�r���*c�	���~/(�x����Etc^���m��A��珫��0�oY�\rn�,�d<V��fm�i*t�\rX\ru�B\nĉ�RS���Iؔi\ZG�,�d�*Y����67&NҹYk��(B�$�Gr�C����!�e˵[�x�%����-*k��P��R3�\r�Xid�y쾟��\Z[�&�z�:|�e���`��göDMù�\\/�KGK��8�:�3�V\\;�]4y!g0ޓd3���T1����=N�:�F��IX�뤐i��)�P\"%b�E�aN��D�dH��y���a�I:��8�� �i�r�����Q�\n5ֹ�V�}�\0�\"i�r��D�����*�M�+ݤK��a�2�N�M8cGq���y�K9�!O�+��\0���rn�	����q �&���KW\\��;9v	RF.qC��v!#�p�a�V(��0p�=Mp�t]\nk`Lȉt�-�Á\"\".!B!���.\'��$r̪�$���If�����=�k5Tp���3�Px�0�x\\hb��=j^�Z�A��V��\\�A�j�D$���\n�������q`�ڢ��,��i�a|��_F��g�Mf�A2c6ͻ�\ZxN��APmb�v\0�/�_t�@���*�(�\\Gxp�(�!�\n�k�a����_Ҹ��6,�������ͨ�+N�6�07E��u�&��G�C	�7�n?����Y�$��ha�v�=\Z#� ���ߝ=�c�]���6��\nzc2?������7Fq=\nɣ0�0��2���.�yu��q��^��������׳W�0snt$C�vcy��Em�#�͢\0]@����k\"m��#����,)ӆv�਄��dR�T�L�С���mjj����0�fg��[q�S��?Zq��������ϣ���h�x��ƍU��1�\'�z�V�qQ���~~R2�\n���9��mh>0\0PU�@-��!���oI�W��Y�I֐t���n:0�IE�Y\nX�5\0��*�P(Mʹ�A��AH�јl\\�/[��e�w5̹�\r�l��줆^Ix��x<���a:����|�rz>��[:�G��ѱ̣aa���ѫ�2zU-b�U����o�z�l�8��t\\��Ka��47mRpr��Aj�и��ijYQ��o��\\7&gź��v\r[�Ti\rd��\r�B +�����\0��P��Do�p�@4	��&�Bf=��-� ��l�vZO�m�ۅ�GA��$[=4P��X��1��5�TӃ7H�)�O��փo@�k����j�Ĩe��،�?�=��m�{嚖�C�*�2/�y�l�t�`�v+5:�a�d�>��#�P;���z���\0�PP��\Z��Z����`��k��@-����s=�G����k�]�Պ���Tź��i��7~�5��D֚��������Im�0\r�C���E��6��#����(6W�G}&R�Q��/���뚷�fX��p��#���n���r`P?�1VU\"7���ud�GqQq��׃c\n{�W���:�n����3���P���������fKn2n\nH��*��ʌ��,�C%����K��d���0eh⼧�����6�(��-�a�SS%.;�<6U�d�)�a�`Z)��Ge��Ynl_����>G����86vu��Vv��\'�y�t��\'	�-�S5g:a�i�ޑu�bw����QH�̡n�Fa\'p��(��ɔwUGA���!�8�k4�*�F�	�h\n\Z�I�3�O�Ru�yN�v-O\Zu���cj��݇\0�ʽ���м3��ʋL���H�+�ѐ^��̒,���կ�p�R�S�]�~v7\Z;Ŷ�h=y��B�n�w#��et�B�!$����I�.����>\r��k���x�X�2\\ƷS\r��ɼS9��Ė���8:YdE��˴��,��D=Eza��M���L��ƻ2����`��4ֱ�����k�F6Y���GP�������_I?�C;۟�9	��p;^�߅��]�W��az�����!(�d��R��ќ�jb�\0���qE���f�h�:܁>�U�Y�{9�O����`ŪW)�!�Aqe����.KG��e���e����G�U��,d���F�Vɟ#L�b���܍�U$cQjr�V\";�,�����(�]�,��.��e����OX�=xw�?)uS');
/*!40000 ALTER TABLE `cf_cache_pagesection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_pagesection_tags`
--

DROP TABLE IF EXISTS `cf_cache_pagesection_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_pagesection_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_pagesection_tags`
--

LOCK TABLES `cf_cache_pagesection_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_pagesection_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_pagesection_tags` VALUES (1,'1_222419149','pageId_1'),(2,'1_222419149','mpvarHash_222419149');
/*!40000 ALTER TABLE `cf_cache_pagesection_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_rootline`
--

DROP TABLE IF EXISTS `cf_cache_rootline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_rootline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_rootline`
--

LOCK TABLES `cf_cache_rootline` WRITE;
/*!40000 ALTER TABLE `cf_cache_rootline` DISABLE KEYS */;
INSERT INTO `cf_cache_rootline` VALUES (4,'1__0_0',1561276716,'a:1:{i:0;a:29:{s:3:\"pid\";i:0;s:3:\"uid\";i:1;s:9:\"t3ver_oid\";i:0;s:10:\"t3ver_wsid\";i:0;s:11:\"t3ver_state\";i:0;s:5:\"title\";s:4:\"Home\";s:5:\"alias\";s:0:\"\";s:9:\"nav_title\";s:0:\"\";s:5:\"media\";s:0:\"\";s:6:\"layout\";i:0;s:6:\"hidden\";i:0;s:9:\"starttime\";i:0;s:7:\"endtime\";i:0;s:8:\"fe_group\";s:0:\"\";s:16:\"extendToSubpages\";i:0;s:7:\"doktype\";i:1;s:8:\"TSconfig\";N;s:17:\"tsconfig_includes\";N;s:11:\"is_siteroot\";i:1;s:9:\"mount_pid\";i:0;s:12:\"mount_pid_ol\";i:0;s:13:\"fe_login_mode\";i:0;s:25:\"backend_layout_next_level\";s:0:\"\";s:11:\"description\";N;s:8:\"keywords\";N;s:6:\"author\";s:0:\"\";s:8:\"nav_hide\";i:0;s:8:\"shortcut\";i:0;s:13:\"shortcut_mode\";i:0;}}');
/*!40000 ALTER TABLE `cf_cache_rootline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_cache_rootline_tags`
--

DROP TABLE IF EXISTS `cf_cache_rootline_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_cache_rootline_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_cache_rootline_tags`
--

LOCK TABLES `cf_cache_rootline_tags` WRITE;
/*!40000 ALTER TABLE `cf_cache_rootline_tags` DISABLE KEYS */;
INSERT INTO `cf_cache_rootline_tags` VALUES (4,'1__0_0','pageId_1');
/*!40000 ALTER TABLE `cf_cache_rootline_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_datamapfactory_datamap`
--

DROP TABLE IF EXISTS `cf_extbase_datamapfactory_datamap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_datamapfactory_datamap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_datamapfactory_datamap`
--

LOCK TABLES `cf_extbase_datamapfactory_datamap` WRITE;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap` DISABLE KEYS */;
INSERT INTO `cf_extbase_datamapfactory_datamap` VALUES (1,'TYPO3%CMS%Beuser%Domain%Model%BackendUser',1558688334,'O:52:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\DataMap\":20:{s:12:\"\0*\0className\";s:41:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUser\";s:12:\"\0*\0tableName\";s:8:\"be_users\";s:13:\"\0*\0recordType\";N;s:13:\"\0*\0subclasses\";a:0:{}s:13:\"\0*\0columnMaps\";a:25:{s:8:\"userName\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"userName\";s:13:\"\0*\0columnName\";s:8:\"username\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:11:\"description\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:11:\"description\";s:13:\"\0*\0columnName\";s:11:\"description\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:4:\"TEXT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:8:\"password\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"password\";s:13:\"\0*\0columnName\";s:8:\"password\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:17:\"backendUserGroups\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:17:\"backendUserGroups\";s:13:\"\0*\0columnName\";s:9:\"usergroup\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";s:9:\"be_groups\";s:27:\"\0*\0childTableWhereStatement\";s:24:\"ORDER BY be_groups.title\";s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:6:\"avatar\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:6:\"avatar\";s:13:\"\0*\0columnName\";s:6:\"avatar\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"INLINE\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:12:\"locktodomain\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:12:\"locktodomain\";s:13:\"\0*\0columnName\";s:12:\"lockToDomain\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:13:\"dbMountPoints\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:13:\"dbMountPoints\";s:13:\"\0*\0columnName\";s:14:\"db_mountpoints\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"GROUP\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:2:\"DB\";}}s:15:\"fileMountPoints\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"fileMountPoints\";s:13:\"\0*\0columnName\";s:16:\"file_mountpoints\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:5:\"email\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:5:\"email\";s:13:\"\0*\0columnName\";s:5:\"email\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:8:\"realName\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"realName\";s:13:\"\0*\0columnName\";s:8:\"realName\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:10:\"isDisabled\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:10:\"isDisabled\";s:13:\"\0*\0columnName\";s:7:\"disable\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:16:\"ipLockIsDisabled\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:16:\"ipLockIsDisabled\";s:13:\"\0*\0columnName\";s:13:\"disableIPlock\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:15:\"isAdministrator\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"isAdministrator\";s:13:\"\0*\0columnName\";s:5:\"admin\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:7:\"options\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:7:\"options\";s:13:\"\0*\0columnName\";s:7:\"options\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:15:\"filePermissions\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"filePermissions\";s:13:\"\0*\0columnName\";s:16:\"file_permissions\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:14:\"workspacePerms\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:14:\"workspacePerms\";s:13:\"\0*\0columnName\";s:15:\"workspace_perms\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:16:\"startDateAndTime\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:16:\"startDateAndTime\";s:13:\"\0*\0columnName\";s:9:\"starttime\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:14:\"endDateAndTime\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:14:\"endDateAndTime\";s:13:\"\0*\0columnName\";s:7:\"endtime\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:4:\"lang\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:4:\"lang\";s:13:\"\0*\0columnName\";s:4:\"lang\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:8:\"usermods\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"usermods\";s:13:\"\0*\0columnName\";s:8:\"userMods\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:16:\"allowedLanguages\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:16:\"allowedLanguages\";s:13:\"\0*\0columnName\";s:17:\"allowed_languages\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:8:\"tsconfig\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"tsconfig\";s:13:\"\0*\0columnName\";s:8:\"TSconfig\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:4:\"TEXT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:15:\"createdbyaction\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"createdbyaction\";s:13:\"\0*\0columnName\";s:15:\"createdByAction\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:11:\"PASSTHROUGH\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:20:\"lastLoginDateAndTime\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:20:\"lastLoginDateAndTime\";s:13:\"\0*\0columnName\";s:9:\"lastlogin\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:13:\"categoryPerms\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:13:\"categoryPerms\";s:13:\"\0*\0columnName\";s:14:\"category_perms\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}}s:19:\"\0*\0pageIdColumnName\";s:3:\"pid\";s:23:\"\0*\0languageIdColumnName\";N;s:30:\"\0*\0translationOriginColumnName\";N;s:34:\"\0*\0translationOriginDiffSourceName\";N;s:29:\"\0*\0modificationDateColumnName\";s:6:\"tstamp\";s:25:\"\0*\0creationDateColumnName\";s:6:\"crdate\";s:20:\"\0*\0creatorColumnName\";s:9:\"cruser_id\";s:24:\"\0*\0deletedFlagColumnName\";s:7:\"deleted\";s:25:\"\0*\0disabledFlagColumnName\";s:7:\"disable\";s:22:\"\0*\0startTimeColumnName\";s:9:\"starttime\";s:20:\"\0*\0endTimeColumnName\";s:7:\"endtime\";s:30:\"\0*\0frontendUserGroupColumnName\";N;s:23:\"\0*\0recordTypeColumnName\";s:5:\"admin\";s:11:\"\0*\0isStatic\";b:0;s:12:\"\0*\0rootLevel\";i:1;}'),(2,'TYPO3%CMS%Beuser%Domain%Model%BackendUserGroup',1558688334,'O:52:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\DataMap\":20:{s:12:\"\0*\0className\";s:46:\"TYPO3\\CMS\\Beuser\\Domain\\Model\\BackendUserGroup\";s:12:\"\0*\0tableName\";s:9:\"be_groups\";s:13:\"\0*\0recordType\";N;s:13:\"\0*\0subclasses\";a:0:{}s:13:\"\0*\0columnMaps\";a:19:{s:5:\"title\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:5:\"title\";s:13:\"\0*\0columnName\";s:5:\"title\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:13:\"dbMountpoints\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:13:\"dbMountpoints\";s:13:\"\0*\0columnName\";s:14:\"db_mountpoints\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"GROUP\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:2:\"DB\";}}s:15:\"fileMountpoints\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"fileMountpoints\";s:13:\"\0*\0columnName\";s:16:\"file_mountpoints\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:15:\"filePermissions\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"filePermissions\";s:13:\"\0*\0columnName\";s:16:\"file_permissions\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:14:\"workspacePerms\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:14:\"workspacePerms\";s:13:\"\0*\0columnName\";s:15:\"workspace_perms\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:15:\"pagetypesSelect\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:15:\"pagetypesSelect\";s:13:\"\0*\0columnName\";s:16:\"pagetypes_select\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:12:\"tablesModify\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:12:\"tablesModify\";s:13:\"\0*\0columnName\";s:13:\"tables_modify\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:12:\"tablesSelect\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:12:\"tablesSelect\";s:13:\"\0*\0columnName\";s:13:\"tables_select\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:16:\"nonExcludeFields\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:16:\"nonExcludeFields\";s:13:\"\0*\0columnName\";s:18:\"non_exclude_fields\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:17:\"explicitAllowdeny\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:17:\"explicitAllowdeny\";s:13:\"\0*\0columnName\";s:18:\"explicit_allowdeny\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:16:\"allowedLanguages\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:16:\"allowedLanguages\";s:13:\"\0*\0columnName\";s:17:\"allowed_languages\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:13:\"customOptions\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:13:\"customOptions\";s:13:\"\0*\0columnName\";s:14:\"custom_options\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:6:\"hidden\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:6:\"hidden\";s:13:\"\0*\0columnName\";s:6:\"hidden\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"CHECK\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:12:\"locktodomain\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:12:\"locktodomain\";s:13:\"\0*\0columnName\";s:12:\"lockToDomain\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:5:\"INPUT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:9:\"groupmods\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:9:\"groupmods\";s:13:\"\0*\0columnName\";s:9:\"groupMods\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:11:\"description\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:11:\"description\";s:13:\"\0*\0columnName\";s:11:\"description\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:4:\"TEXT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:8:\"tsconfig\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:8:\"tsconfig\";s:13:\"\0*\0columnName\";s:8:\"TSconfig\";s:17:\"\0*\0typeOfRelation\";s:13:\"RELATION_NONE\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:4:\"TEXT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:9:\"subGroups\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:9:\"subGroups\";s:13:\"\0*\0columnName\";s:8:\"subgroup\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";s:9:\"be_groups\";s:27:\"\0*\0childTableWhereStatement\";s:64:\"AND NOT(be_groups.uid = ###THIS_UID###) ORDER BY be_groups.title\";s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}s:13:\"categoryPerms\";O:54:\"TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Mapper\\ColumnMap\":18:{s:15:\"\0*\0propertyName\";s:13:\"categoryPerms\";s:13:\"\0*\0columnName\";s:14:\"category_perms\";s:17:\"\0*\0typeOfRelation\";s:17:\"RELATION_HAS_MANY\";s:17:\"\0*\0childClassName\";N;s:17:\"\0*\0childTableName\";N;s:27:\"\0*\0childTableWhereStatement\";N;s:23:\"\0*\0childSortByFieldName\";N;s:20:\"\0*\0relationTableName\";N;s:32:\"\0*\0relationTablePageIdColumnName\";N;s:27:\"\0*\0relationTableMatchFields\";N;s:28:\"\0*\0relationTableInsertFields\";N;s:30:\"\0*\0relationTableWhereStatement\";N;s:21:\"\0*\0parentKeyFieldName\";N;s:23:\"\0*\0parentTableFieldName\";N;s:20:\"\0*\0childKeyFieldName\";N;s:24:\"\0*\0dateTimeStorageFormat\";N;s:7:\"\0*\0type\";O:43:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnType\":1:{s:8:\"\0*\0value\";s:6:\"SELECT\";}s:15:\"\0*\0internalType\";O:46:\"TYPO3\\CMS\\Core\\DataHandling\\TableColumnSubType\":1:{s:8:\"\0*\0value\";s:0:\"\";}}}s:19:\"\0*\0pageIdColumnName\";s:3:\"pid\";s:23:\"\0*\0languageIdColumnName\";N;s:30:\"\0*\0translationOriginColumnName\";N;s:34:\"\0*\0translationOriginDiffSourceName\";N;s:29:\"\0*\0modificationDateColumnName\";s:6:\"tstamp\";s:25:\"\0*\0creationDateColumnName\";s:6:\"crdate\";s:20:\"\0*\0creatorColumnName\";s:9:\"cruser_id\";s:24:\"\0*\0deletedFlagColumnName\";s:7:\"deleted\";s:25:\"\0*\0disabledFlagColumnName\";s:6:\"hidden\";s:22:\"\0*\0startTimeColumnName\";N;s:20:\"\0*\0endTimeColumnName\";N;s:30:\"\0*\0frontendUserGroupColumnName\";N;s:23:\"\0*\0recordTypeColumnName\";N;s:11:\"\0*\0isStatic\";b:0;s:12:\"\0*\0rootLevel\";i:1;}');
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_extbase_datamapfactory_datamap_tags`
--

DROP TABLE IF EXISTS `cf_extbase_datamapfactory_datamap_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_extbase_datamapfactory_datamap_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_extbase_datamapfactory_datamap_tags`
--

LOCK TABLES `cf_extbase_datamapfactory_datamap_tags` WRITE;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_extbase_datamapfactory_datamap_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_vhs_main`
--

DROP TABLE IF EXISTS `cf_vhs_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_vhs_main` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_vhs_main`
--

LOCK TABLES `cf_vhs_main` WRITE;
/*!40000 ALTER TABLE `cf_vhs_main` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_vhs_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_vhs_main_tags`
--

DROP TABLE IF EXISTS `cf_vhs_main_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_vhs_main_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_vhs_main_tags`
--

LOCK TABLES `cf_vhs_main_tags` WRITE;
/*!40000 ALTER TABLE `cf_vhs_main_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_vhs_main_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_vhs_markdown`
--

DROP TABLE IF EXISTS `cf_vhs_markdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_vhs_markdown` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_vhs_markdown`
--

LOCK TABLES `cf_vhs_markdown` WRITE;
/*!40000 ALTER TABLE `cf_vhs_markdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_vhs_markdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cf_vhs_markdown_tags`
--

DROP TABLE IF EXISTS `cf_vhs_markdown_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cf_vhs_markdown_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cf_vhs_markdown_tags`
--

LOCK TABLES `cf_vhs_markdown_tags` WRITE;
/*!40000 ALTER TABLE `cf_vhs_markdown_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cf_vhs_markdown_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_groups`
--

DROP TABLE IF EXISTS `fe_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_groups` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `tx_extbase_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lockToDomain` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subgroup` tinytext COLLATE utf8_unicode_ci,
  `TSconfig` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_groups`
--

LOCK TABLES `fe_groups` WRITE;
/*!40000 ALTER TABLE `fe_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_sessions`
--

DROP TABLE IF EXISTS `fe_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_sessions` (
  `ses_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ses_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `ses_data` mediumblob,
  `ses_permanent` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ses_anonymous` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_sessions`
--

LOCK TABLES `fe_sessions` WRITE;
/*!40000 ALTER TABLE `fe_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_users`
--

DROP TABLE IF EXISTS `fe_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fe_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `disable` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `tx_extbase_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` tinytext COLLATE utf8_unicode_ci,
  `name` varchar(160) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lockToDomain` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uc` blob,
  `title` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `www` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` tinytext COLLATE utf8_unicode_ci,
  `TSconfig` text COLLATE utf8_unicode_ci,
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `is_online` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`username`(100)),
  KEY `username` (`username`(100)),
  KEY `is_online` (`is_online`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_users`
--

LOCK TABLES `fe_users` WRITE;
/*!40000 ALTER TABLE `fe_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `rowDescription` text COLLATE utf8_unicode_ci,
  `editlock` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_source` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `perms_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `perms_groupid` int(10) unsigned NOT NULL DEFAULT '0',
  `perms_user` smallint(5) unsigned NOT NULL DEFAULT '0',
  `perms_group` smallint(5) unsigned NOT NULL DEFAULT '0',
  `perms_everybody` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doktype` int(10) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text COLLATE utf8_unicode_ci,
  `is_siteroot` smallint(6) NOT NULL DEFAULT '0',
  `php_tree_stop` smallint(6) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `layout` int(10) unsigned NOT NULL DEFAULT '0',
  `target` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `media` int(10) unsigned NOT NULL DEFAULT '0',
  `lastUpdated` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` text COLLATE utf8_unicode_ci,
  `cache_timeout` int(10) unsigned NOT NULL DEFAULT '0',
  `cache_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `newUntil` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `no_search` smallint(5) unsigned NOT NULL DEFAULT '0',
  `SYS_LASTCHANGED` int(10) unsigned NOT NULL DEFAULT '0',
  `abstract` text COLLATE utf8_unicode_ci,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extendToSubpages` smallint(5) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nav_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nav_hide` smallint(6) NOT NULL DEFAULT '0',
  `content_from_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid_ol` smallint(6) NOT NULL DEFAULT '0',
  `alias` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `l18n_cfg` smallint(6) NOT NULL DEFAULT '0',
  `fe_login_mode` smallint(6) NOT NULL DEFAULT '0',
  `backend_layout` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backend_layout_next_level` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tsconfig_includes` text COLLATE utf8_unicode_ci,
  `legacy_overlay_uid` int(10) unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no_index` smallint(6) NOT NULL DEFAULT '0',
  `no_follow` smallint(6) NOT NULL DEFAULT '0',
  `og_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `og_description` text COLLATE utf8_unicode_ci,
  `og_image` int(10) unsigned NOT NULL DEFAULT '0',
  `twitter_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `twitter_description` text COLLATE utf8_unicode_ci,
  `twitter_image` int(10) unsigned NOT NULL DEFAULT '0',
  `canonical_link` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `categories` int(11) NOT NULL DEFAULT '0',
  `tx_hivecptanchornav_bs4_class_section` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_row` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_no_gutters_row` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_col0` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_col1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_col2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_col3` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_align_col4` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_push_pull_col0` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_push_pull_col1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_push_pull_col2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tx_hivecptanchornav_bs4_push_pull_col3` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `alias` (`alias`),
  KEY `determineSiteRoot` (`is_siteroot`),
  KEY `language_identifier` (`l10n_parent`,`sys_language_uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,1558684712,1558684331,1,0,0,0,0,'',256,NULL,0,0,0,0,NULL,0,'a:54:{s:7:\"doktype\";N;s:5:\"title\";N;s:4:\"slug\";N;s:9:\"nav_title\";N;s:8:\"subtitle\";N;s:9:\"seo_title\";N;s:8:\"no_index\";N;s:9:\"no_follow\";N;s:14:\"canonical_link\";N;s:8:\"og_title\";N;s:14:\"og_description\";N;s:8:\"og_image\";N;s:13:\"twitter_title\";N;s:19:\"twitter_description\";N;s:13:\"twitter_image\";N;s:8:\"abstract\";N;s:8:\"keywords\";N;s:11:\"description\";N;s:6:\"author\";N;s:12:\"author_email\";N;s:11:\"lastUpdated\";N;s:6:\"layout\";N;s:8:\"newUntil\";N;s:14:\"backend_layout\";N;s:25:\"backend_layout_next_level\";N;s:37:\"tx_hivecptanchornav_bs4_class_section\";N;s:33:\"tx_hivecptanchornav_bs4_align_row\";N;s:38:\"tx_hivecptanchornav_bs4_no_gutters_row\";N;s:34:\"tx_hivecptanchornav_bs4_align_col0\";N;s:34:\"tx_hivecptanchornav_bs4_align_col1\";N;s:34:\"tx_hivecptanchornav_bs4_align_col2\";N;s:34:\"tx_hivecptanchornav_bs4_align_col3\";N;s:16:\"content_from_pid\";N;s:6:\"target\";N;s:13:\"cache_timeout\";N;s:10:\"cache_tags\";N;s:11:\"is_siteroot\";N;s:9:\"no_search\";N;s:13:\"php_tree_stop\";N;s:6:\"module\";N;s:5:\"media\";N;s:17:\"tsconfig_includes\";N;s:8:\"TSconfig\";N;s:8:\"l18n_cfg\";N;s:6:\"hidden\";N;s:8:\"nav_hide\";N;s:9:\"starttime\";N;s:7:\"endtime\";N;s:16:\"extendToSubpages\";N;s:8:\"fe_group\";N;s:13:\"fe_login_mode\";N;s:8:\"editlock\";N;s:10:\"categories\";N;s:14:\"rowDescription\";N;}',0,0,'',0,0,0,0,0,0,1,0,31,27,0,'Home','/',1,NULL,1,0,'',0,0,'',0,'',0,0,NULL,0,'',0,NULL,0,1558684712,NULL,'',0,'','','',0,0,0,0,'',0,0,'pagets__CustomAnchor','',NULL,0,0,'',0,0,'',NULL,0,'',NULL,0,'',0,'','','','','','','0','','','','',''),(2,0,1558684414,1558684387,1,0,0,0,0,'0',512,NULL,0,0,0,0,NULL,0,'a:1:{s:6:\"hidden\";N;}',0,0,'',0,0,0,0,0,0,1,0,31,27,0,'.:: ROOT TEMPLATE ::.','/',254,NULL,0,0,'',0,0,'',0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0,0,'',0,0,'',NULL,0,'',NULL,0,'',0,'','','','','','','','','','','',''),(3,0,1558684456,1558684432,1,0,0,0,0,'0',768,NULL,0,0,0,0,NULL,0,'a:1:{s:6:\"hidden\";N;}',0,0,'',0,0,0,0,0,0,1,0,31,27,0,'.:: GLOBAL CONTENT ::.','/',254,NULL,0,0,'',0,0,'',0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0,0,'',0,0,'',NULL,0,'',NULL,0,'',0,'','','','','','','','','','','','');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_be_shortcuts`
--

DROP TABLE IF EXISTS `sys_be_shortcuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_be_shortcuts` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` text COLLATE utf8_unicode_ci,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `sc_group` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_be_shortcuts`
--

LOCK TABLES `sys_be_shortcuts` WRITE;
/*!40000 ALTER TABLE `sys_be_shortcuts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_be_shortcuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category`
--

DROP TABLE IF EXISTS `sys_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_category` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `items` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `category_parent` (`parent`),
  KEY `category_list` (`pid`,`deleted`,`sys_language_uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category`
--

LOCK TABLES `sys_category` WRITE;
/*!40000 ALTER TABLE `sys_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category_record_mm`
--

DROP TABLE IF EXISTS `sys_category_record_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_category_record_mm` (
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fieldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `sorting_foreign` int(11) NOT NULL DEFAULT '0',
  KEY `uid_local_foreign` (`uid_local`,`uid_foreign`),
  KEY `uid_foreign_tablefield` (`uid_foreign`,`tablenames`(40),`fieldname`(3),`sorting_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category_record_mm`
--

LOCK TABLES `sys_category_record_mm` WRITE;
/*!40000 ALTER TABLE `sys_category_record_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category_record_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_collection`
--

DROP TABLE IF EXISTS `sys_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_collection` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8_unicode_ci,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'static',
  `table_name` tinytext COLLATE utf8_unicode_ci,
  `items` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_collection`
--

LOCK TABLES `sys_collection` WRITE;
/*!40000 ALTER TABLE `sys_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_collection_entries`
--

DROP TABLE IF EXISTS `sys_collection_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_collection_entries` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_collection_entries`
--

LOCK TABLES `sys_collection_entries` WRITE;
/*!40000 ALTER TABLE `sys_collection_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_collection_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_domain`
--

DROP TABLE IF EXISTS `sys_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_domain` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `domainName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `getSysDomain` (`hidden`),
  KEY `getDomainStartPage` (`pid`,`hidden`,`domainName`(100)),
  KEY `parent` (`pid`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_domain`
--

LOCK TABLES `sys_domain` WRITE;
/*!40000 ALTER TABLE `sys_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file`
--

DROP TABLE IF EXISTS `sys_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `last_indexed` int(11) NOT NULL DEFAULT '0',
  `missing` smallint(6) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `metadata` int(11) NOT NULL DEFAULT '0',
  `identifier` text COLLATE utf8_unicode_ci,
  `identifier_hash` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `folder_hash` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mime_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8_unicode_ci,
  `sha1` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `creation_date` int(11) NOT NULL DEFAULT '0',
  `modification_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `sel01` (`storage`,`identifier_hash`),
  KEY `folder` (`storage`,`folder_hash`),
  KEY `tstamp` (`tstamp`),
  KEY `lastindex` (`last_indexed`),
  KEY `sha1` (`sha1`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file`
--

LOCK TABLES `sys_file` WRITE;
/*!40000 ALTER TABLE `sys_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_collection`
--

DROP TABLE IF EXISTS `sys_file_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_collection` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8_unicode_ci,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'static',
  `files` int(11) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `folder` text COLLATE utf8_unicode_ci,
  `recursive` smallint(6) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_collection`
--

LOCK TABLES `sys_file_collection` WRITE;
/*!40000 ALTER TABLE `sys_file_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_metadata`
--

DROP TABLE IF EXISTS `sys_file_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_metadata` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `file` int(11) NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8_unicode_ci,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `alternative` text COLLATE utf8_unicode_ci,
  `categories` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `file` (`file`),
  KEY `fal_filelist` (`l10n_parent`,`sys_language_uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_metadata`
--

LOCK TABLES `sys_file_metadata` WRITE;
/*!40000 ALTER TABLE `sys_file_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_processedfile`
--

DROP TABLE IF EXISTS `sys_file_processedfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_processedfile` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `storage` int(11) NOT NULL DEFAULT '0',
  `original` int(11) NOT NULL DEFAULT '0',
  `identifier` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8_unicode_ci,
  `configuration` text COLLATE utf8_unicode_ci,
  `configurationsha1` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `originalfilesha1` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `task_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `checksum` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `combined_1` (`original`,`task_type`(100),`configurationsha1`),
  KEY `identifier` (`storage`,`identifier`(180))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_processedfile`
--

LOCK TABLES `sys_file_processedfile` WRITE;
/*!40000 ALTER TABLE `sys_file_processedfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_processedfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_reference`
--

DROP TABLE IF EXISTS `sys_file_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_reference` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fieldname` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sorting_foreign` int(11) NOT NULL DEFAULT '0',
  `table_local` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` tinytext COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `alternative` text COLLATE utf8_unicode_ci,
  `link` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `crop` varchar(4000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `autoplay` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `tablenames_fieldname` (`tablenames`(32),`fieldname`(12)),
  KEY `deleted` (`deleted`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`),
  KEY `combined_1` (`l10n_parent`,`t3ver_oid`,`t3ver_wsid`,`t3ver_state`,`deleted`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_reference`
--

LOCK TABLES `sys_file_reference` WRITE;
/*!40000 ALTER TABLE `sys_file_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_storage`
--

DROP TABLE IF EXISTS `sys_file_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_file_storage` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `driver` tinytext COLLATE utf8_unicode_ci,
  `configuration` text COLLATE utf8_unicode_ci,
  `is_default` smallint(6) NOT NULL DEFAULT '0',
  `is_browsable` smallint(6) NOT NULL DEFAULT '0',
  `is_public` smallint(6) NOT NULL DEFAULT '0',
  `is_writable` smallint(6) NOT NULL DEFAULT '0',
  `is_online` smallint(6) NOT NULL DEFAULT '1',
  `auto_extract_metadata` smallint(6) NOT NULL DEFAULT '1',
  `processingfolder` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_storage`
--

LOCK TABLES `sys_file_storage` WRITE;
/*!40000 ALTER TABLE `sys_file_storage` DISABLE KEYS */;
INSERT INTO `sys_file_storage` VALUES (1,0,1558684341,1558684341,0,0,'This is the local fileadmin/ directory. This storage mount has been created automatically by TYPO3.','fileadmin/ (auto-created)','Local','<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n<T3FlexForms>\n    <data>\n        <sheet index=\"sDEF\">\n            <language index=\"lDEF\">\n                <field index=\"basePath\">\n                    <value index=\"vDEF\">fileadmin/</value>\n                </field>\n                <field index=\"pathType\">\n                    <value index=\"vDEF\">relative</value>\n                </field>\n                <field index=\"caseSensitive\">\n                    <value index=\"vDEF\">1</value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>',1,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `sys_file_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_filemounts`
--

DROP TABLE IF EXISTS `sys_filemounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_filemounts` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `base` int(10) unsigned NOT NULL DEFAULT '0',
  `read_only` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_filemounts`
--

LOCK TABLES `sys_filemounts` WRITE;
/*!40000 ALTER TABLE `sys_filemounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filemounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_history`
--

DROP TABLE IF EXISTS `sys_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_history` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `actiontype` smallint(6) NOT NULL DEFAULT '0',
  `usertype` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'BE',
  `userid` int(10) unsigned DEFAULT NULL,
  `originaluserid` int(10) unsigned DEFAULT NULL,
  `recuid` int(11) NOT NULL DEFAULT '0',
  `tablename` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `history_data` mediumtext COLLATE utf8_unicode_ci,
  `workspace` int(11) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `recordident_1` (`tablename`(100),`recuid`),
  KEY `recordident_2` (`tablename`(100),`tstamp`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_history`
--

LOCK TABLES `sys_history` WRITE;
/*!40000 ALTER TABLE `sys_history` DISABLE KEYS */;
INSERT INTO `sys_history` VALUES (1,0,1558684331,1,'BE',1,0,1,'pages','{\"uid\":1,\"pid\":0,\"tstamp\":1558684331,\"crdate\":1558684331,\"cruser_id\":1,\"deleted\":0,\"hidden\":1,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":256,\"rowDescription\":null,\"editlock\":0,\"sys_language_uid\":0,\"l10n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l10n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_id\":0,\"t3ver_label\":\"\",\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"t3ver_count\":0,\"t3ver_tstamp\":0,\"t3ver_move_id\":0,\"perms_userid\":1,\"perms_groupid\":0,\"perms_user\":31,\"perms_group\":27,\"perms_everybody\":0,\"title\":\"Home\",\"slug\":\"\\/\",\"doktype\":1,\"TSconfig\":null,\"is_siteroot\":0,\"php_tree_stop\":0,\"url\":\"\",\"shortcut\":0,\"shortcut_mode\":0,\"subtitle\":\"\",\"layout\":0,\"target\":\"\",\"media\":0,\"lastUpdated\":0,\"keywords\":null,\"cache_timeout\":0,\"cache_tags\":\"\",\"newUntil\":0,\"description\":null,\"no_search\":0,\"SYS_LASTCHANGED\":0,\"abstract\":null,\"module\":\"\",\"extendToSubpages\":0,\"author\":\"\",\"author_email\":\"\",\"nav_title\":\"\",\"nav_hide\":0,\"content_from_pid\":0,\"mount_pid\":0,\"mount_pid_ol\":0,\"alias\":\"\",\"l18n_cfg\":0,\"fe_login_mode\":0,\"backend_layout\":\"\",\"backend_layout_next_level\":\"\",\"tsconfig_includes\":null,\"legacy_overlay_uid\":0,\"tx_impexp_origuid\":0,\"seo_title\":\"\",\"no_index\":0,\"no_follow\":0,\"og_title\":\"\",\"og_description\":null,\"og_image\":0,\"twitter_title\":\"\",\"twitter_description\":null,\"twitter_image\":0,\"canonical_link\":\"\",\"categories\":0,\"tx_hivecptanchornav_bs4_class_section\":\"\",\"tx_hivecptanchornav_bs4_align_row\":\"\",\"tx_hivecptanchornav_bs4_no_gutters_row\":\"\",\"tx_hivecptanchornav_bs4_align_col0\":\"\",\"tx_hivecptanchornav_bs4_align_col1\":\"\",\"tx_hivecptanchornav_bs4_align_col2\":\"\",\"tx_hivecptanchornav_bs4_align_col3\":\"\",\"tx_hivecptanchornav_bs4_align_col4\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col0\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col1\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col2\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col3\":\"\"}',0),(2,0,1558684357,2,'BE',1,0,1,'pages','{\"oldRecord\":{\"tx_hivecptanchornav_bs4_align_col3\":\"\",\"is_siteroot\":0,\"fe_group\":\"0\",\"l10n_diffsource\":\"\"},\"newRecord\":{\"tx_hivecptanchornav_bs4_align_col3\":\"0\",\"is_siteroot\":\"1\",\"fe_group\":\"\",\"l10n_diffsource\":\"a:54:{s:7:\\\"doktype\\\";N;s:5:\\\"title\\\";N;s:4:\\\"slug\\\";N;s:9:\\\"nav_title\\\";N;s:8:\\\"subtitle\\\";N;s:9:\\\"seo_title\\\";N;s:8:\\\"no_index\\\";N;s:9:\\\"no_follow\\\";N;s:14:\\\"canonical_link\\\";N;s:8:\\\"og_title\\\";N;s:14:\\\"og_description\\\";N;s:8:\\\"og_image\\\";N;s:13:\\\"twitter_title\\\";N;s:19:\\\"twitter_description\\\";N;s:13:\\\"twitter_image\\\";N;s:8:\\\"abstract\\\";N;s:8:\\\"keywords\\\";N;s:11:\\\"description\\\";N;s:6:\\\"author\\\";N;s:12:\\\"author_email\\\";N;s:11:\\\"lastUpdated\\\";N;s:6:\\\"layout\\\";N;s:8:\\\"newUntil\\\";N;s:14:\\\"backend_layout\\\";N;s:25:\\\"backend_layout_next_level\\\";N;s:37:\\\"tx_hivecptanchornav_bs4_class_section\\\";N;s:33:\\\"tx_hivecptanchornav_bs4_align_row\\\";N;s:38:\\\"tx_hivecptanchornav_bs4_no_gutters_row\\\";N;s:34:\\\"tx_hivecptanchornav_bs4_align_col0\\\";N;s:34:\\\"tx_hivecptanchornav_bs4_align_col1\\\";N;s:34:\\\"tx_hivecptanchornav_bs4_align_col2\\\";N;s:34:\\\"tx_hivecptanchornav_bs4_align_col3\\\";N;s:16:\\\"content_from_pid\\\";N;s:6:\\\"target\\\";N;s:13:\\\"cache_timeout\\\";N;s:10:\\\"cache_tags\\\";N;s:11:\\\"is_siteroot\\\";N;s:9:\\\"no_search\\\";N;s:13:\\\"php_tree_stop\\\";N;s:6:\\\"module\\\";N;s:5:\\\"media\\\";N;s:17:\\\"tsconfig_includes\\\";N;s:8:\\\"TSconfig\\\";N;s:8:\\\"l18n_cfg\\\";N;s:6:\\\"hidden\\\";N;s:8:\\\"nav_hide\\\";N;s:9:\\\"starttime\\\";N;s:7:\\\"endtime\\\";N;s:16:\\\"extendToSubpages\\\";N;s:8:\\\"fe_group\\\";N;s:13:\\\"fe_login_mode\\\";N;s:8:\\\"editlock\\\";N;s:10:\\\"categories\\\";N;s:14:\\\"rowDescription\\\";N;}\"}}',0),(3,0,1558684367,2,'BE',1,0,1,'pages','{\"oldRecord\":{\"hidden\":1},\"newRecord\":{\"hidden\":\"0\"}}',0),(4,0,1558684387,1,'BE',1,0,2,'pages','{\"uid\":2,\"pid\":0,\"tstamp\":1558684387,\"crdate\":1558684387,\"cruser_id\":1,\"deleted\":0,\"hidden\":1,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":128,\"rowDescription\":null,\"editlock\":0,\"sys_language_uid\":0,\"l10n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l10n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_id\":0,\"t3ver_label\":\"\",\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"t3ver_count\":0,\"t3ver_tstamp\":0,\"t3ver_move_id\":0,\"perms_userid\":1,\"perms_groupid\":0,\"perms_user\":31,\"perms_group\":27,\"perms_everybody\":0,\"title\":\"Root Template\",\"slug\":\"\\/\",\"doktype\":254,\"TSconfig\":null,\"is_siteroot\":0,\"php_tree_stop\":0,\"url\":\"\",\"shortcut\":0,\"shortcut_mode\":0,\"subtitle\":\"\",\"layout\":0,\"target\":\"\",\"media\":0,\"lastUpdated\":0,\"keywords\":null,\"cache_timeout\":0,\"cache_tags\":\"\",\"newUntil\":0,\"description\":null,\"no_search\":0,\"SYS_LASTCHANGED\":0,\"abstract\":null,\"module\":\"\",\"extendToSubpages\":0,\"author\":\"\",\"author_email\":\"\",\"nav_title\":\"\",\"nav_hide\":0,\"content_from_pid\":0,\"mount_pid\":0,\"mount_pid_ol\":0,\"alias\":\"\",\"l18n_cfg\":0,\"fe_login_mode\":0,\"backend_layout\":\"\",\"backend_layout_next_level\":\"\",\"tsconfig_includes\":null,\"legacy_overlay_uid\":0,\"tx_impexp_origuid\":0,\"seo_title\":\"\",\"no_index\":0,\"no_follow\":0,\"og_title\":\"\",\"og_description\":null,\"og_image\":0,\"twitter_title\":\"\",\"twitter_description\":null,\"twitter_image\":0,\"canonical_link\":\"\",\"categories\":0,\"tx_hivecptanchornav_bs4_class_section\":\"\",\"tx_hivecptanchornav_bs4_align_row\":\"\",\"tx_hivecptanchornav_bs4_no_gutters_row\":\"\",\"tx_hivecptanchornav_bs4_align_col0\":\"\",\"tx_hivecptanchornav_bs4_align_col1\":\"\",\"tx_hivecptanchornav_bs4_align_col2\":\"\",\"tx_hivecptanchornav_bs4_align_col3\":\"\",\"tx_hivecptanchornav_bs4_align_col4\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col0\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col1\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col2\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col3\":\"\"}',0),(5,0,1558684401,2,'BE',1,0,2,'pages','{\"oldRecord\":{\"title\":\"Root Template\",\"l10n_diffsource\":\"\"},\"newRecord\":{\"title\":\".:: ROOT TEMPLATE ::.\",\"l10n_diffsource\":\"a:1:{s:5:\\\"title\\\";N;}\"}}',0),(6,0,1558684407,3,'BE',1,0,2,'pages','{\"oldPageId\":0,\"newPageId\":0,\"oldData\":{\"header\":\".:: ROOT TEMPLATE ::.\",\"pid\":0,\"event_pid\":2,\"t3ver_state\":0,\"_ORIG_pid\":null},\"newData\":{\"tstamp\":1558684407,\"pid\":0,\"sorting\":512}}',0),(7,0,1558684414,2,'BE',1,0,2,'pages','{\"oldRecord\":{\"hidden\":1,\"l10n_diffsource\":\"a:1:{s:5:\\\"title\\\";N;}\"},\"newRecord\":{\"hidden\":\"0\",\"l10n_diffsource\":\"a:1:{s:6:\\\"hidden\\\";N;}\"}}',0),(8,0,1558684432,1,'BE',1,0,3,'pages','{\"uid\":3,\"pid\":0,\"tstamp\":1558684432,\"crdate\":1558684432,\"cruser_id\":1,\"deleted\":0,\"hidden\":1,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":128,\"rowDescription\":null,\"editlock\":0,\"sys_language_uid\":0,\"l10n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l10n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_id\":0,\"t3ver_label\":\"\",\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"t3ver_count\":0,\"t3ver_tstamp\":0,\"t3ver_move_id\":0,\"perms_userid\":1,\"perms_groupid\":0,\"perms_user\":31,\"perms_group\":27,\"perms_everybody\":0,\"title\":\".:: GLOBAL CONTENT ::.\",\"slug\":\"\\/\",\"doktype\":254,\"TSconfig\":null,\"is_siteroot\":0,\"php_tree_stop\":0,\"url\":\"\",\"shortcut\":0,\"shortcut_mode\":0,\"subtitle\":\"\",\"layout\":0,\"target\":\"\",\"media\":0,\"lastUpdated\":0,\"keywords\":null,\"cache_timeout\":0,\"cache_tags\":\"\",\"newUntil\":0,\"description\":null,\"no_search\":0,\"SYS_LASTCHANGED\":0,\"abstract\":null,\"module\":\"\",\"extendToSubpages\":0,\"author\":\"\",\"author_email\":\"\",\"nav_title\":\"\",\"nav_hide\":0,\"content_from_pid\":0,\"mount_pid\":0,\"mount_pid_ol\":0,\"alias\":\"\",\"l18n_cfg\":0,\"fe_login_mode\":0,\"backend_layout\":\"\",\"backend_layout_next_level\":\"\",\"tsconfig_includes\":null,\"legacy_overlay_uid\":0,\"tx_impexp_origuid\":0,\"seo_title\":\"\",\"no_index\":0,\"no_follow\":0,\"og_title\":\"\",\"og_description\":null,\"og_image\":0,\"twitter_title\":\"\",\"twitter_description\":null,\"twitter_image\":0,\"canonical_link\":\"\",\"categories\":0,\"tx_hivecptanchornav_bs4_class_section\":\"\",\"tx_hivecptanchornav_bs4_align_row\":\"\",\"tx_hivecptanchornav_bs4_no_gutters_row\":\"\",\"tx_hivecptanchornav_bs4_align_col0\":\"\",\"tx_hivecptanchornav_bs4_align_col1\":\"\",\"tx_hivecptanchornav_bs4_align_col2\":\"\",\"tx_hivecptanchornav_bs4_align_col3\":\"\",\"tx_hivecptanchornav_bs4_align_col4\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col0\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col1\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col2\":\"\",\"tx_hivecptanchornav_bs4_push_pull_col3\":\"\"}',0),(9,0,1558684442,2,'BE',1,0,3,'pages','{\"oldRecord\":{\"hidden\":1,\"l10n_diffsource\":\"\"},\"newRecord\":{\"hidden\":\"0\",\"l10n_diffsource\":\"a:1:{s:6:\\\"hidden\\\";N;}\"}}',0),(10,0,1558684456,3,'BE',1,0,3,'pages','{\"oldPageId\":0,\"newPageId\":0,\"oldData\":{\"header\":\".:: GLOBAL CONTENT ::.\",\"pid\":0,\"event_pid\":3,\"t3ver_state\":0,\"_ORIG_pid\":null},\"newData\":{\"tstamp\":1558684456,\"pid\":0,\"sorting\":768}}',0),(11,0,1558684498,1,'BE',1,0,1,'sys_template','{\"uid\":1,\"pid\":2,\"tstamp\":1558684498,\"crdate\":1558684498,\"cruser_id\":1,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"sorting\":256,\"description\":null,\"t3_origuid\":0,\"t3ver_oid\":0,\"t3ver_id\":0,\"t3ver_label\":\"\",\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"t3ver_count\":0,\"t3ver_tstamp\":0,\"t3ver_move_id\":0,\"title\":\"NEW SITE\",\"sitetitle\":\"\",\"root\":1,\"clear\":3,\"include_static_file\":null,\"constants\":null,\"config\":\"\\n# Default PAGE object:\\npage = PAGE\\npage.10 = TEXT\\npage.10.value = HELLO WORLD!\\n\",\"nextLevel\":\"\",\"basedOn\":\"\",\"includeStaticAfterBasedOn\":0,\"static_file_mode\":0,\"tx_impexp_origuid\":0}',0),(12,0,1558684562,2,'BE',1,0,1,'sys_template','{\"oldRecord\":{\"title\":\"NEW SITE\",\"config\":\"\\n# Default PAGE object:\\npage = PAGE\\npage.10 = TEXT\\npage.10.value = HELLO WORLD!\\n\",\"include_static_file\":null},\"newRecord\":{\"title\":\".:: ROOT TEMPLATE ::.\",\"config\":\"\",\"include_static_file\":\"EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/,EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/Styling\\/,EXT:hive_cfg_typoscript\\/Configuration\\/TypoScript,EXT:hive_cpt_nav_anchor\\/Configuration\\/TypoScript,EXT:hive_thm_backendlayout\\/Configuration\\/TypoScript,EXT:hive_thm_bs\\/Configuration\\/TypoScript,EXT:hive_cpt_cnt_img\\/Configuration\\/TypoScript,EXT:hive_thm_blazy\\/Configuration\\/TypoScript,EXT:hive_cpt_dynamiccontent\\/Configuration\\/TypoScript,EXT:hive_thm_custom\\/Configuration\\/TypoScript\"}}',0),(13,0,1558684592,2,'BE',1,0,1,'sys_template','{\"oldRecord\":{\"include_static_file\":\"EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/,EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/Styling\\/,EXT:hive_cfg_typoscript\\/Configuration\\/TypoScript,EXT:hive_cpt_nav_anchor\\/Configuration\\/TypoScript,EXT:hive_thm_backendlayout\\/Configuration\\/TypoScript,EXT:hive_thm_bs\\/Configuration\\/TypoScript,EXT:hive_cpt_cnt_img\\/Configuration\\/TypoScript,EXT:hive_thm_blazy\\/Configuration\\/TypoScript,EXT:hive_cpt_dynamiccontent\\/Configuration\\/TypoScript,EXT:hive_thm_custom\\/Configuration\\/TypoScript\"},\"newRecord\":{\"include_static_file\":\"EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/,EXT:fluid_styled_content\\/Configuration\\/TypoScript\\/Styling\\/,EXT:hive_cfg_typoscript\\/Configuration\\/TypoScript,EXT:hive_cpt_nav_anchor\\/Configuration\\/TypoScript,EXT:hive_cpt_cnt_img\\/Configuration\\/TypoScript,EXT:hive_cpt_dynamiccontent\\/Configuration\\/TypoScript,EXT:hive_thm_backendlayout\\/Configuration\\/TypoScript,EXT:hive_thm_bs\\/Configuration\\/TypoScript,EXT:hive_thm_blazy\\/Configuration\\/TypoScript,EXT:hive_thm_custom\\/Configuration\\/TypoScript\"}}',0),(14,0,1558684611,1,'BE',1,0,2,'sys_template','{\"uid\":2,\"pid\":1,\"tstamp\":1558684611,\"crdate\":1558684611,\"cruser_id\":1,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"sorting\":256,\"description\":null,\"t3_origuid\":0,\"t3ver_oid\":0,\"t3ver_id\":0,\"t3ver_label\":\"\",\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"t3ver_count\":0,\"t3ver_tstamp\":0,\"t3ver_move_id\":0,\"title\":\"+ext\",\"sitetitle\":\"\",\"root\":0,\"clear\":0,\"include_static_file\":null,\"constants\":null,\"config\":null,\"nextLevel\":\"\",\"basedOn\":\"\",\"includeStaticAfterBasedOn\":0,\"static_file_mode\":0,\"tx_impexp_origuid\":0}',0),(15,0,1558684626,2,'BE',1,0,2,'sys_template','{\"oldRecord\":{\"title\":\"+ext\"},\"newRecord\":{\"title\":\"+ext [1]\"}}',0),(16,0,1558684663,2,'BE',1,0,2,'sys_template','{\"oldRecord\":{\"basedOn\":\"\"},\"newRecord\":{\"basedOn\":\"1\"}}',0),(17,0,1558684708,2,'BE',1,0,1,'pages','{\"oldRecord\":{\"backend_layout\":\"\"},\"newRecord\":{\"backend_layout\":\"pagets__CustomAnchor\"}}',0);
/*!40000 ALTER TABLE `sys_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_language`
--

DROP TABLE IF EXISTS `sys_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_language` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `title` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `flag` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `language_isocode` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `static_lang_isocode` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_language`
--

LOCK TABLES `sys_language` WRITE;
/*!40000 ALTER TABLE `sys_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_lockedrecords`
--

DROP TABLE IF EXISTS `sys_lockedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_lockedrecords` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `record_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `record_uid` int(11) NOT NULL DEFAULT '0',
  `record_pid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `feuserid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`tstamp`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_lockedrecords`
--

LOCK TABLES `sys_lockedrecords` WRITE;
/*!40000 ALTER TABLE `sys_lockedrecords` DISABLE KEYS */;
INSERT INTO `sys_lockedrecords` VALUES (15,1,1558684712,'pages',1,0,'sudo',0);
/*!40000 ALTER TABLE `sys_lockedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_log` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `action` smallint(5) unsigned NOT NULL DEFAULT '0',
  `recuid` int(10) unsigned NOT NULL DEFAULT '0',
  `tablename` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `recpid` int(11) NOT NULL DEFAULT '0',
  `error` smallint(5) unsigned NOT NULL DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `details_nr` smallint(6) NOT NULL DEFAULT '0',
  `IP` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `log_data` text COLLATE utf8_unicode_ci,
  `event_pid` int(11) NOT NULL DEFAULT '-1',
  `workspace` int(11) NOT NULL DEFAULT '0',
  `NEWid` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `request_id` varchar(13) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_micro` double NOT NULL DEFAULT '0',
  `component` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `level` smallint(5) unsigned NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`event_pid`),
  KEY `recuidIdx` (`recuid`),
  KEY `user_auth` (`type`,`action`,`tstamp`),
  KEY `request` (`request_id`),
  KEY `combined_1` (`tstamp`,`type`,`userid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (1,0,1558684146,0,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1301648975: No pages are found on the rootlevel! | TYPO3\\CMS\\Core\\Error\\Http\\ServiceUnavailableException thrown in file /app/web/typo3/sysext/frontend/Classes/Controller/TypoScriptFrontendController.php in line 1431. Requested URL: http://development.localhost/',5,0,'172.25.0.1','',-1,0,'','',0,'',0,NULL,NULL),(2,0,1558684160,0,3,0,'',0,3,'Login-attempt from ###IP###, username \'%s\', password not accepted!',255,1,'172.25.0.1','a:1:{i:0;s:4:\"sudo\";}',-1,-99,'','',0,'',0,NULL,NULL),(3,0,1558684281,1,1,0,'',0,0,'User %s logged in from ###IP###',255,1,'172.25.0.1','a:1:{i:0;s:4:\"sudo\";}',-1,-99,'','',0,'',0,NULL,NULL),(4,0,1558684331,1,1,1,'pages',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1,10,'172.25.0.1','a:4:{i:0;s:4:\"Home\";i:1;s:7:\"pages:1\";i:2;s:12:\"[root-level]\";i:3;i:0;}',0,0,'NEW_1','',0,'',0,NULL,NULL),(5,0,1558684357,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:4:\"Home\";i:1;s:7:\"pages:1\";s:7:\"history\";s:1:\"2\";}',1,0,'','',0,'',0,NULL,NULL),(6,0,1558684367,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:4:\"Home\";i:1;s:7:\"pages:1\";s:7:\"history\";s:1:\"3\";}',1,0,'','',0,'',0,NULL,NULL),(7,0,1558684387,1,1,2,'pages',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1,10,'172.25.0.1','a:4:{i:0;s:13:\"Root Template\";i:1;s:7:\"pages:2\";i:2;s:12:\"[root-level]\";i:3;i:0;}',0,0,'NEW_1','',0,'',0,NULL,NULL),(8,0,1558684401,1,2,2,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:21:\".:: ROOT TEMPLATE ::.\";i:1;s:7:\"pages:2\";s:7:\"history\";s:1:\"5\";}',2,0,'','',0,'',0,NULL,NULL),(9,0,1558684407,1,4,2,'pages',0,0,'Moved record \'%s\' (%s) on page \'%s\' (%s)',1,4,'172.25.0.1','a:4:{i:0;s:21:\".:: ROOT TEMPLATE ::.\";i:1;s:7:\"pages:2\";i:2;s:12:\"[root-level]\";i:3;i:0;}',0,0,'','',0,'',0,NULL,NULL),(10,0,1558684414,1,2,2,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:21:\".:: ROOT TEMPLATE ::.\";i:1;s:7:\"pages:2\";s:7:\"history\";s:1:\"7\";}',2,0,'','',0,'',0,NULL,NULL),(11,0,1558684432,1,1,3,'pages',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1,10,'172.25.0.1','a:4:{i:0;s:22:\".:: GLOBAL CONTENT ::.\";i:1;s:7:\"pages:3\";i:2;s:12:\"[root-level]\";i:3;i:0;}',0,0,'NEW_1','',0,'',0,NULL,NULL),(12,0,1558684442,1,2,3,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:22:\".:: GLOBAL CONTENT ::.\";i:1;s:7:\"pages:3\";s:7:\"history\";s:1:\"9\";}',3,0,'','',0,'',0,NULL,NULL),(13,0,1558684456,1,4,3,'pages',0,0,'Moved record \'%s\' (%s) on page \'%s\' (%s)',1,4,'172.25.0.1','a:4:{i:0;s:22:\".:: GLOBAL CONTENT ::.\";i:1;s:7:\"pages:3\";i:2;s:12:\"[root-level]\";i:3;i:0;}',0,0,'','',0,'',0,NULL,NULL),(14,0,1558684498,1,1,1,'sys_template',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1,10,'172.25.0.1','a:4:{i:0;s:8:\"NEW SITE\";i:1;s:14:\"sys_template:1\";i:2;s:21:\".:: ROOT TEMPLATE ::.\";i:3;i:2;}',2,0,'NEW','',0,'',0,NULL,NULL),(15,0,1558684498,1,1,0,'',0,0,'User %s has cleared the cache (cacheCmd=%s)',3,0,'172.25.0.1','a:2:{i:0;s:4:\"sudo\";i:1;s:3:\"all\";}',-1,0,'','',0,'',0,NULL,NULL),(16,0,1558684562,1,2,1,'sys_template',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:21:\".:: ROOT TEMPLATE ::.\";i:1;s:14:\"sys_template:1\";s:7:\"history\";s:2:\"12\";}',2,0,'','',0,'',0,NULL,NULL),(17,0,1558684592,1,2,1,'sys_template',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:21:\".:: ROOT TEMPLATE ::.\";i:1;s:14:\"sys_template:1\";s:7:\"history\";s:2:\"13\";}',2,0,'','',0,'',0,NULL,NULL),(18,0,1558684611,1,1,2,'sys_template',0,0,'Record \'%s\' (%s) was inserted on page \'%s\' (%s)',1,10,'172.25.0.1','a:4:{i:0;s:4:\"+ext\";i:1;s:14:\"sys_template:2\";i:2;s:4:\"Home\";i:3;i:1;}',1,0,'NEW','',0,'',0,NULL,NULL),(19,0,1558684626,1,2,2,'sys_template',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:8:\"+ext [1]\";i:1;s:14:\"sys_template:2\";s:7:\"history\";s:2:\"15\";}',1,0,'','',0,'',0,NULL,NULL),(20,0,1558684663,1,2,2,'sys_template',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:8:\"+ext [1]\";i:1;s:14:\"sys_template:2\";s:7:\"history\";s:2:\"16\";}',1,0,'','',0,'',0,NULL,NULL),(21,0,1558684708,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:4:\"Home\";i:1;s:7:\"pages:1\";s:7:\"history\";s:2:\"17\";}',1,0,'','',0,'',0,NULL,NULL),(22,0,1558684712,1,2,1,'pages',0,0,'Record \'%s\' (%s) was updated. (Online).',1,10,'172.25.0.1','a:3:{i:0;s:4:\"Home\";i:1;s:7:\"pages:1\";s:7:\"history\";i:0;}',1,0,'','',0,'',0,NULL,NULL),(23,0,1558684729,1,0,0,'',0,1,'Core: Error handler (FE): PHP Warning: file_get_contents(/app/web/typo3conf/ext/hive_thm_custom/Resources/Public/Assets/Gulp/Js/includeJSLibs/includeJSLibs.min.js): failed to open stream: No such file or directory in /app/web/typo3/sysext/core/Classes/Resource/ResourceCompressor.php line 296',5,0,'172.25.0.1','',-1,0,'','',0,'',0,NULL,NULL);
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_news`
--

DROP TABLE IF EXISTS `sys_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_news` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_news`
--

LOCK TABLES `sys_news` WRITE;
/*!40000 ALTER TABLE `sys_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_redirect`
--

DROP TABLE IF EXISTS `sys_redirect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_redirect` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `updatedon` int(10) unsigned NOT NULL DEFAULT '0',
  `createdon` int(10) unsigned NOT NULL DEFAULT '0',
  `createdby` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `source_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `source_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_regexp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `force_https` smallint(5) unsigned NOT NULL DEFAULT '0',
  `respect_query_parameters` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keep_query_parameters` smallint(5) unsigned NOT NULL DEFAULT '0',
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `target_statuscode` int(11) NOT NULL DEFAULT '307',
  `hitcount` int(11) NOT NULL DEFAULT '0',
  `lasthiton` int(11) NOT NULL DEFAULT '0',
  `disable_hitcount` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index_source` (`source_host`(80),`source_path`(80)),
  KEY `parent` (`pid`,`deleted`,`disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_redirect`
--

LOCK TABLES `sys_redirect` WRITE;
/*!40000 ALTER TABLE `sys_redirect` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_redirect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_refindex`
--

DROP TABLE IF EXISTS `sys_refindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_refindex` (
  `hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tablename` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `recuid` int(11) NOT NULL DEFAULT '0',
  `field` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `flexpointer` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `softref_key` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `softref_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `deleted` smallint(6) NOT NULL DEFAULT '0',
  `workspace` int(11) NOT NULL DEFAULT '0',
  `ref_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ref_uid` int(11) NOT NULL DEFAULT '0',
  `ref_string` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`hash`),
  KEY `lookup_rec` (`tablename`(100),`recuid`),
  KEY `lookup_uid` (`ref_table`(100),`ref_uid`),
  KEY `lookup_string` (`ref_string`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_refindex`
--

LOCK TABLES `sys_refindex` WRITE;
/*!40000 ALTER TABLE `sys_refindex` DISABLE KEYS */;
INSERT INTO `sys_refindex` VALUES ('9c5c89c6d5272643ae7b055f55121b18','sys_template',2,'basedOn','','','',0,0,0,'sys_template',1,'');
/*!40000 ALTER TABLE `sys_refindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_registry`
--

DROP TABLE IF EXISTS `sys_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_registry` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_namespace` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `entry_key` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `entry_value` mediumblob,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `entry_identifier` (`entry_namespace`,`entry_key`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_registry`
--

LOCK TABLES `sys_registry` WRITE;
/*!40000 ALTER TABLE `sys_registry` DISABLE KEYS */;
INSERT INTO `sys_registry` VALUES (1,'installUpdate','TYPO3\\CMS\\Form\\Hooks\\FormFileExtensionUpdate','i:1;'),(2,'installUpdate','TYPO3\\CMS\\Install\\Updates\\ExtensionManagerTables','i:1;'),(3,'installUpdate','TYPO3\\CMS\\Install\\Updates\\WizardDoneToRegistry','i:1;'),(4,'installUpdate','TYPO3\\CMS\\Install\\Updates\\StartModuleUpdate','i:1;'),(5,'installUpdate','TYPO3\\CMS\\Install\\Updates\\FrontendUserImageUpdateWizard','i:1;'),(6,'installUpdate','TYPO3\\CMS\\Install\\Updates\\DatabaseRowsUpdateWizard','i:1;'),(7,'installUpdate','TYPO3\\CMS\\Install\\Updates\\CommandLineBackendUserRemovalUpdate','i:1;'),(8,'installUpdate','TYPO3\\CMS\\Install\\Updates\\FillTranslationSourceField','i:1;'),(9,'installUpdate','TYPO3\\CMS\\Install\\Updates\\SectionFrameToFrameClassUpdate','i:1;'),(10,'installUpdate','TYPO3\\CMS\\Install\\Updates\\SplitMenusUpdate','i:1;'),(11,'installUpdate','TYPO3\\CMS\\Install\\Updates\\BulletContentElementUpdate','i:1;'),(12,'installUpdate','TYPO3\\CMS\\Install\\Updates\\UploadContentElementUpdate','i:1;'),(13,'installUpdate','TYPO3\\CMS\\Install\\Updates\\MigrateFscStaticTemplateUpdate','i:1;'),(14,'installUpdate','TYPO3\\CMS\\Install\\Updates\\FileReferenceUpdate','i:1;'),(15,'installUpdate','TYPO3\\CMS\\Install\\Updates\\MigrateFeSessionDataUpdate','i:1;'),(16,'installUpdate','TYPO3\\CMS\\Install\\Updates\\Compatibility7ExtractionUpdate','i:1;'),(17,'installUpdate','TYPO3\\CMS\\Install\\Updates\\FormLegacyExtractionUpdate','i:1;'),(18,'installUpdate','TYPO3\\CMS\\Install\\Updates\\RteHtmlAreaExtractionUpdate','i:1;'),(19,'installUpdate','TYPO3\\CMS\\Install\\Updates\\LanguageSortingUpdate','i:1;'),(20,'installUpdate','TYPO3\\CMS\\Install\\Updates\\Typo3DbExtractionUpdate','i:1;'),(21,'installUpdate','TYPO3\\CMS\\Install\\Updates\\FuncExtractionUpdate','i:1;'),(22,'installUpdate','TYPO3\\CMS\\Install\\Updates\\MigrateUrlTypesInPagesUpdate','i:1;'),(23,'installUpdate','TYPO3\\CMS\\Install\\Updates\\SeparateSysHistoryFromSysLogUpdate','i:1;'),(24,'installUpdate','TYPO3\\CMS\\Install\\Updates\\RedirectExtractionUpdate','i:1;'),(25,'installUpdate','TYPO3\\CMS\\Install\\Updates\\BackendUserStartModuleUpdate','i:1;'),(26,'installUpdate','TYPO3\\CMS\\Install\\Updates\\MigratePagesLanguageOverlayUpdate','i:1;'),(27,'installUpdate','TYPO3\\CMS\\Install\\Updates\\MigratePagesLanguageOverlayBeGroupsAccessRights','i:1;'),(28,'installUpdate','TYPO3\\CMS\\Install\\Updates\\BackendLayoutIconUpdateWizard','i:1;'),(29,'installUpdate','TYPO3\\CMS\\Install\\Updates\\RedirectsExtensionUpdate','i:1;'),(30,'installUpdate','TYPO3\\CMS\\Install\\Updates\\AdminPanelInstall','i:1;'),(31,'installUpdate','TYPO3\\CMS\\Install\\Updates\\PopulatePageSlugs','i:1;'),(32,'installUpdate','TYPO3\\CMS\\Install\\Updates\\Argon2iPasswordHashes','i:1;'),(33,'core','formProtectionSessionToken:1','s:64:\"e03b6c1d7cd08af94cec838c89d92c15b83ba9309064004986c601630f65ebda\";');
/*!40000 ALTER TABLE `sys_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_template`
--

DROP TABLE IF EXISTS `sys_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_template` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sitetitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `root` smallint(5) unsigned NOT NULL DEFAULT '0',
  `clear` smallint(5) unsigned NOT NULL DEFAULT '0',
  `include_static_file` text COLLATE utf8_unicode_ci,
  `constants` text COLLATE utf8_unicode_ci,
  `config` text COLLATE utf8_unicode_ci,
  `nextLevel` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `basedOn` tinytext COLLATE utf8_unicode_ci,
  `includeStaticAfterBasedOn` smallint(5) unsigned NOT NULL DEFAULT '0',
  `static_file_mode` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `roottemplate` (`deleted`,`hidden`,`root`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_template`
--

LOCK TABLES `sys_template` WRITE;
/*!40000 ALTER TABLE `sys_template` DISABLE KEYS */;
INSERT INTO `sys_template` VALUES (1,2,1558684592,1558684498,1,0,0,0,0,256,NULL,0,0,0,'',0,0,0,0,0,0,'.:: ROOT TEMPLATE ::.','',1,3,'EXT:fluid_styled_content/Configuration/TypoScript/,EXT:fluid_styled_content/Configuration/TypoScript/Styling/,EXT:hive_cfg_typoscript/Configuration/TypoScript,EXT:hive_cpt_nav_anchor/Configuration/TypoScript,EXT:hive_cpt_cnt_img/Configuration/TypoScript,EXT:hive_cpt_dynamiccontent/Configuration/TypoScript,EXT:hive_thm_backendlayout/Configuration/TypoScript,EXT:hive_thm_bs/Configuration/TypoScript,EXT:hive_thm_blazy/Configuration/TypoScript,EXT:hive_thm_custom/Configuration/TypoScript',NULL,'','','',0,0,0),(2,1,1558684663,1558684611,1,0,0,0,0,256,NULL,0,0,0,'',0,0,0,0,0,0,'+ext [1]','',0,0,NULL,NULL,NULL,'','1',0,0,0);
/*!40000 ALTER TABLE `sys_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tt_content`
--

DROP TABLE IF EXISTS `tt_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tt_content` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rowDescription` text COLLATE utf8_unicode_ci,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `editlock` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l18n_parent` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_source` int(10) unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8_unicode_ci,
  `t3_origuid` int(10) unsigned NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  `t3ver_oid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_wsid` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_move_id` int(10) unsigned NOT NULL DEFAULT '0',
  `CType` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `header_position` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bodytext` mediumtext COLLATE utf8_unicode_ci,
  `bullets_type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uploads_description` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uploads_type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `assets` int(10) unsigned NOT NULL DEFAULT '0',
  `image` int(10) unsigned NOT NULL DEFAULT '0',
  `imagewidth` int(10) unsigned NOT NULL DEFAULT '0',
  `imageorient` smallint(5) unsigned NOT NULL DEFAULT '0',
  `imagecols` smallint(5) unsigned NOT NULL DEFAULT '0',
  `imageborder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `media` int(10) unsigned NOT NULL DEFAULT '0',
  `layout` int(10) unsigned NOT NULL DEFAULT '0',
  `frame_class` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `cols` int(10) unsigned NOT NULL DEFAULT '0',
  `spaceBefore` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spaceAfter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `space_before_class` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `space_after_class` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `records` text COLLATE utf8_unicode_ci,
  `pages` text COLLATE utf8_unicode_ci,
  `colPos` int(10) unsigned NOT NULL DEFAULT '0',
  `subheader` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `header_link` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_zoom` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header_layout` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `list_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sectionIndex` smallint(5) unsigned NOT NULL DEFAULT '0',
  `linkToTop` smallint(5) unsigned NOT NULL DEFAULT '0',
  `file_collections` text COLLATE utf8_unicode_ci,
  `filelink_size` smallint(5) unsigned NOT NULL DEFAULT '0',
  `filelink_sorting` varchar(17) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filelink_sorting_direction` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `target` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `recursive` smallint(5) unsigned NOT NULL DEFAULT '0',
  `imageheight` int(10) unsigned NOT NULL DEFAULT '0',
  `pi_flexform` mediumtext COLLATE utf8_unicode_ci,
  `accessibility_title` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accessibility_bypass` smallint(5) unsigned NOT NULL DEFAULT '0',
  `accessibility_bypass_text` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `selected_categories` text COLLATE utf8_unicode_ci,
  `category_field` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `table_class` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `table_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `table_delimiter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `table_enclosure` smallint(5) unsigned NOT NULL DEFAULT '0',
  `table_header_position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `table_tfoot` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `categories` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`sorting`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l18n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tt_content`
--

LOCK TABLES `tt_content` WRITE;
/*!40000 ALTER TABLE `tt_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `tt_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_extensionmanager_domain_model_extension`
--

DROP TABLE IF EXISTS `tx_extensionmanager_domain_model_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_extensionmanager_domain_model_extension` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `extension_key` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `repository` int(10) unsigned NOT NULL DEFAULT '1',
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alldownloadcounter` int(10) unsigned NOT NULL DEFAULT '0',
  `downloadcounter` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci,
  `state` int(11) NOT NULL DEFAULT '0',
  `review_state` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0',
  `serialized_dependencies` mediumtext COLLATE utf8_unicode_ci,
  `author_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ownerusername` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `md5hash` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `update_comment` mediumtext COLLATE utf8_unicode_ci,
  `authorcompany` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `integer_version` int(11) NOT NULL DEFAULT '0',
  `current_version` int(11) NOT NULL DEFAULT '0',
  `lastreviewedversion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `versionextrepo` (`extension_key`,`version`,`repository`),
  KEY `index_extrepo` (`extension_key`,`repository`),
  KEY `index_versionrepo` (`integer_version`,`repository`,`extension_key`),
  KEY `index_currentversions` (`current_version`,`review_state`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_extensionmanager_domain_model_extension`
--

LOCK TABLES `tx_extensionmanager_domain_model_extension` WRITE;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_extensionmanager_domain_model_repository`
--

DROP TABLE IF EXISTS `tx_extensionmanager_domain_model_repository`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_extensionmanager_domain_model_repository` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci,
  `wsdl_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mirror_list_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_update` int(10) unsigned NOT NULL DEFAULT '0',
  `extension_count` int(11) NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_extensionmanager_domain_model_repository`
--

LOCK TABLES `tx_extensionmanager_domain_model_repository` WRITE;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_repository` DISABLE KEYS */;
INSERT INTO `tx_extensionmanager_domain_model_repository` VALUES (1,'TYPO3.org Main Repository','Main repository on typo3.org. This repository has some mirrors configured which are available with the mirror url.','https://typo3.org/wsdl/tx_ter_wsdl.php','https://repositories.typo3.org/mirrors.xml.gz',1346191200,0,0);
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_repository` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_hivecptcntimg_domain_model_falimage`
--

DROP TABLE IF EXISTS `tx_hivecptcntimg_domain_model_falimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_hivecptcntimg_domain_model_falimage` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `l10n_state` text COLLATE utf8_unicode_ci,
  `fal_image` int(10) unsigned NOT NULL DEFAULT '0',
  `focus_x` double NOT NULL DEFAULT '0',
  `focus_y` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_hivecptcntimg_domain_model_falimage`
--

LOCK TABLES `tx_hivecptcntimg_domain_model_falimage` WRITE;
/*!40000 ALTER TABLE `tx_hivecptcntimg_domain_model_falimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_hivecptcntimg_domain_model_falimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_hivecptnavanchor_domain_model_navigation`
--

DROP TABLE IF EXISTS `tx_hivecptnavanchor_domain_model_navigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_hivecptnavanchor_domain_model_navigation` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_state` smallint(6) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `l10n_state` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_hivecptnavanchor_domain_model_navigation`
--

LOCK TABLES `tx_hivecptnavanchor_domain_model_navigation` WRITE;
/*!40000 ALTER TABLE `tx_hivecptnavanchor_domain_model_navigation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_hivecptnavanchor_domain_model_navigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_impexp_presets`
--

DROP TABLE IF EXISTS `tx_impexp_presets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_impexp_presets` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_uid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `public` smallint(6) NOT NULL DEFAULT '0',
  `item_uid` int(11) NOT NULL DEFAULT '0',
  `preset_data` blob,
  PRIMARY KEY (`uid`),
  KEY `lookup` (`item_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_impexp_presets`
--

LOCK TABLES `tx_impexp_presets` WRITE;
/*!40000 ALTER TABLE `tx_impexp_presets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_impexp_presets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_scheduler_task`
--

DROP TABLE IF EXISTS `tx_scheduler_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_scheduler_task` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `disable` smallint(5) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `nextexecution` int(10) unsigned NOT NULL DEFAULT '0',
  `lastexecution_time` int(10) unsigned NOT NULL DEFAULT '0',
  `lastexecution_failure` text COLLATE utf8_unicode_ci,
  `lastexecution_context` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `serialized_task_object` mediumblob,
  `serialized_executions` mediumblob,
  `task_group` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index_nextexecution` (`nextexecution`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_scheduler_task`
--

LOCK TABLES `tx_scheduler_task` WRITE;
/*!40000 ALTER TABLE `tx_scheduler_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_scheduler_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_scheduler_task_group`
--

DROP TABLE IF EXISTS `tx_scheduler_task_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tx_scheduler_task_group` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(10) unsigned NOT NULL DEFAULT '0',
  `crdate` int(10) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `groupName` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_scheduler_task_group`
--

LOCK TABLES `tx_scheduler_task_group` WRITE;
/*!40000 ALTER TABLE `tx_scheduler_task_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_scheduler_task_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-24  8:05:49
